/*!
  _   _  ___  ____  ___ ________  _   _   _   _ ___   
 | | | |/ _ \|  _ \|_ _|__  / _ \| \ | | | | | |_ _| 
 | |_| | | | | |_) || |  / / | | |  \| | | | | || | 
 |  _  | |_| |  _ < | | / /| |_| | |\  | | |_| || |
 |_| |_|\___/|_| \_\___/____\___/|_| \_|  \___/|___|
                                                                                                                                                                                                                                                                                                                                       
=========================================================
* Horizon UI - v1.1.0
=========================================================

* Product Page: https://www.horizon-ui.com/
* Copyright 2023 Horizon UI (https://www.horizon-ui.com/)

* Designed and Coded by Simmmple

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/

// Chakra imports
import { Box, SimpleGrid } from "@chakra-ui/react";
import ComplexTable from "./components/ComplexTable";
import {
  columnsCustomerTable,
} from "./variables/columnsData";
// import tableDataComplex from "./variables/tableDataComplex.json";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { authorizeAdmin, fetchCustomer } from "../../../action/table.action";

export default function CustomerTable() {
  const dispatch = useDispatch()
  const { customer,limit,currentPage,updateTable } = useSelector((reduxData) => reduxData.tableReducers)
  useEffect(()=>{
    dispatch(fetchCustomer(limit,currentPage))
  },[updateTable===true])
  // Chakra Color Mode
  return (
    <Box pt={{ base: "130px", md: "80px", xl: "80px" }}>
      <SimpleGrid
        mb='20px'
        columns={{ sm: 1, md: 2 }}
        spacing={{ base: "20px", xl: "20px" }}>
        {customer?<ComplexTable
          columnsData={columnsCustomerTable}
          tableData={customer}
        />:""}
      </SimpleGrid>
    </Box>
  );
}
