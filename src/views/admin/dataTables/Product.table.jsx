/*!
  _   _  ___  ____  ___ ________  _   _   _   _ ___   
 | | | |/ _ \|  _ \|_ _|__  / _ \| \ | | | | | |_ _| 
 | |_| | | | | |_) || |  / / | | |  \| | | | | || | 
 |  _  | |_| |  _ < | | / /| |_| | |\  | | |_| || |
 |_| |_|\___/|_| \_\___/____\___/|_| \_|  \___/|___|
                                                                                                                                                                                                                                                                                                                                       
=========================================================
* Horizon UI - v1.1.0
=========================================================

* Product Page: https://www.horizon-ui.com/
* Copyright 2023 Horizon UI (https://www.horizon-ui.com/)

* Designed and Coded by Simmmple

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/

// Chakra imports
import { Box, SimpleGrid } from "@chakra-ui/react";
import ProductDataTable from "./components/ProductTable";

import {
  columnsProductTable,
} from "./variables/columnsData";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {  fetchProduct, fetchProductType } from "../../../action/table.action";

export default function ProductTable() {
  const dispatch = useDispatch()
  const { products,limit,currentPage,updateTable } = useSelector((reduxData) => reduxData.tableReducers)
  useEffect(()=>{
    dispatch(fetchProduct(5,currentPage),dispatch(fetchProductType()))
  },[updateTable===true])
  // Chakra Color Mode
  return (
    <Box pt={{ base: "130px", md: "80px", xl: "80px" }}>
      <SimpleGrid
        mb='20px'
        columns={{ sm: 1, md: 2 }}
        spacing={{ base: "20px", xl: "20px" }}>
        {products?<ProductDataTable
          columnsData={columnsProductTable}
          tableData={products}
        />:""}
      </SimpleGrid>
    </Box>
  );
}
