export const columnsCustomerTable = [
  {
    Header: "FULLNAME",
    accessor: "fullName",
  },
  {
    Header: "EMAIL",
    accessor: "email",
  },
  {
    Header: "PHONE",
    accessor: "phone",
  },
  {
    Header: "ADDRESS",
    accessor: "address",
  },
  {
    Header: "CITY",
    accessor: "city",
  },
  {
    Header: "ACTION",
    // accessor: "address",
  },
];
export const columsOrderTable = [
  {
    Header: "ORDER CODE",
    accessor: "_id",
  },
  {
    Header: "ITEMS NUMBER",
    accessor: "orderDetails",
  },
  {
    Header: "SHIPPED DATE",
    accessor: "shippedDate",
  },
  {
    Header: "COST",
    accessor: "cost",
  },
  {
    Header: "NOTE",
    accessor: "note",
  },
  {
    Header: "ACTION",
    // accessor: "address",
  },
];
export const columnsProductTable = [
  {
    Header: "NAME",
    accessor: "name",
  },
  {
    Header: "IMAGE",
    accessor: "imageUrl",
  },
  {
    Header: "PROMOTION PRICE",
    accessor: "promotionPrice",
  },
  {
    Header: "BUY PRICE",
    accessor: "buyPrice",
  },
  {
    Header: "AMOUNT",
    accessor: "amount",
  },
  {
    Header: "ACTION",
    // accessor: "address",
  },
];
