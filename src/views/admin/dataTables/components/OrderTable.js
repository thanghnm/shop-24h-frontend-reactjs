import {
  Flex,
  // Table,
  Progress,
  Icon,
  Tbody,
  Td,
  Text,
  Th,
  Thead,
  Tr,
  useColorModeValue,
} from "@chakra-ui/react";
import React, { useMemo } from "react";
import {
  useGlobalFilter,
  usePagination,
  useSortBy,
  useTable,
} from "react-table";
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import AddIcon from '@mui/icons-material/Add';
// Custom components
import Card from "../../../../components/card/Card";
import Menu from "../../../../components/menu/MainMenu";
// Assets
// import { MdCheckCircle, MdCancel, MdOutlineError } from "react-icons/md";
import { useDispatch, useSelector } from "react-redux";
import { createMoreFilter, filtOrder, inputFilterOrder, openAdminCartToEdit, openDeleteOrderModal, openProductList, selectFilterOrder } from "../../../../action/table.action";
import { validateProduct } from "../../../../action/cart.action";
import { Table } from "reactstrap";
import { columsOrderTable } from "../variables/columnsData";
import FilterListIcon from '@mui/icons-material/FilterList';
import productNotFound from "../../../../assets/images/shopping-cart/product-not-found.png"

// import { MenuItem, Select } from "@mui/material";
export default function ColumnsTable(props) {
  const { columnsData, tableData } = props;
  const { inputFilterOrderValue, selectFilterOrderValue, limit, currentPage, filterOrderArray,refProduct
   } = useSelector((reduxData) => reduxData.tableReducers)
  // console.log(inputFilterCustomerValue)
  const columns = useMemo(() => columnsData, [columnsData]);
  const data = useMemo(() => tableData, [tableData]);
  // console.log(columns,data)
  const tableInstance = useTable(
    {
      columns,
      data,
    },
    useGlobalFilter,
    useSortBy,
    usePagination
  );
  const {
    initialState,
  } = tableInstance;
  initialState.pageSize = 5;
  const textColor = useColorModeValue("secondaryGray.900", "white");
  const borderColor = useColorModeValue("gray.200", "whiteAlpha.100");
  const dispatch = useDispatch()
  const onBtnAddClick = () => {
    dispatch(openProductList())
    window.scrollTo(0,500)
  }
  const onBtnEditClick = (element) => {
    dispatch(validateProduct(), dispatch(openAdminCartToEdit(element)))
    window.scrollTo(0,500)

  }
  const onBtnDeleteClick = (element) => {
    dispatch(openDeleteOrderModal(element))
  }
  const onBtnMoreFilterClick = () => {
    dispatch(createMoreFilter())
  }
  const onSelectFilterChange=(event)=>{
    dispatch(selectFilterOrder(event))
  }
  const onInputFilterChange=(event)=>{
    dispatch(inputFilterOrder(event))
  }
  const onBtnFilterClick=()=>{
    dispatch(filtOrder(selectFilterOrderValue,inputFilterOrderValue,limit,currentPage))
  }
  return (
    <div>
      <button onClick={onBtnAddClick} className="btn btn-success mb-3 w-20 "><AddIcon /></button>
      <Flex className="mb-3 mt-3 " px='25px' mb='20px'  >
        <div className="flex-col flex w-full">
        {filterOrderArray ? filterOrderArray.map((element, index) => {
          return (
            <div key={index} className=" flex">
              <select data-key={index} value={selectFilterOrderValue[index]} onChange={onSelectFilterChange}  className="w-1/5 form-control" style={{
                borderTopRightRadius: 0, borderBottomRightRadius: 0
              }}>
                <option value={""}>Type</option>
                <option value={"code"}>Code</option>
                <option value={"cost"}>Cost</option>
              </select>
              <input data-key={index} onChange={onInputFilterChange} value={inputFilterOrderValue[index]} style={{ borderRadius: 0 }} className="form-control " placeholder="Input data  to filt" />
            </div>
          )
      }) : ""}
      </div>
            <div className="w-1/5">
              < button onClick={onBtnFilterClick} style = {{ borderTopLeftRadius: 0, borderBottomLeftRadius: 0,height:"100%"}}
               className="btn btn-warning w-full  ">Filter</button>
              </div>
    </Flex>
      <button onClick={onBtnMoreFilterClick} className="btn btn-warning ml-5 mb-3"><FilterListIcon /></button>
      <Table dark striped>
        <thead>
          <tr>
            {columsOrderTable.map((element, index) => {
              return (<th key={index}>{element.Header}</th>)
            })}
          </tr>
        </thead>
        <tbody>
          {props.tableData.map((element, index) => {
            return (
              <tr key={index}>
                <td>{element.orderCode}</td>
                <td>{element.orderDetails.length}</td>
                <td>{new Date(element.shippedDate).toString()}</td>
                <td>{element.cost}</td>
                <td>{element.note}</td>
                <td>
                  <Flex align='center' style={{ justifyContent: "start" }}>
                    <button onClick={() => onBtnEditClick(element)} className="btn btn-primary text-white"><EditIcon /></button>
                    <button onClick={() => onBtnDeleteClick(element)} style={{ background: "red" }} className="btn text-white"><DeleteIcon /></button>
                  </Flex>
                </td>
              </tr>
            )
          })}
        </tbody>
      </Table>
    {props.tableData.length===0?<img src={productNotFound}></img>:""}
    </div >
  );
}
