import {
  Flex,
  Progress,
  Icon,
  Tbody,
  Td,
  Text,
  Th,
  Thead,
  Tr,
  useColorModeValue,
} from "@chakra-ui/react";
import React, { useMemo } from "react";
import {
  useGlobalFilter,
  usePagination,
  useSortBy,
  useTable,
} from "react-table";
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import AddIcon from '@mui/icons-material/Add';
// Custom components
import Card from "../../../../components/card/Card";
import Menu from "../../../../components/menu/MainMenu";
// Assets
// import { MdCheckCircle, MdCancel, MdOutlineError } from "react-icons/md";
import { useDispatch, useSelector } from "react-redux";
import { createMoreFilterCustomer, filtCustomer, inputFilterCustomer, openAddCustomerModal, openDeleteCustomerModal, openEditCustomerModal, selectFilterCustomer } from "../../../../action/table.action";
// import { MenuItem, Select } from "@mui/material";
import { columnsCustomerTable } from "../variables/columnsData";
import { Table } from "react-bootstrap";
import FilterListIcon from '@mui/icons-material/FilterList';
import productNotFound from "../../../../assets/images/shopping-cart/product-not-found.png"

export default function ColumnsTable(props) {
  const { columnsData, tableData } = props;
  const { inputFilterCustomerValue, selectFilterCustomerValue,limit,currentPage,filterCustomerArray } = useSelector((reduxData) => reduxData.tableReducers)
  // console.log(inputFilterCustomerValue)
  const columns = useMemo(() => columnsData, [columnsData]);
  const data = useMemo(() => tableData, [tableData]);

  const tableInstance = useTable(
    {
      columns,
      data,
    },
    useGlobalFilter,
    useSortBy,
    usePagination
  );
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    page,
    prepareRow,
    initialState,
  } = tableInstance;
  initialState.pageSize = 8;
  const textColor = useColorModeValue("secondaryGray.900", "white");
  const borderColor = useColorModeValue("gray.200", "whiteAlpha.100");
  const dispatch = useDispatch()
  const onBtnAddClick = () => {
    dispatch(openAddCustomerModal())
  }
  const onBtnEditClick = (element) => {
    dispatch(openEditCustomerModal(element))
  }
  const onBtnDeleteClick = (element) => {
    dispatch(openDeleteCustomerModal(element))
  }
  const onSelectFilterChange = (event) => {
    dispatch(selectFilterCustomer(event))
  }
  const onTextFieldFilterChange = (event) => {
    dispatch(inputFilterCustomer(event))
  }
  const onBtnMoreFilterClick =()=>{
    dispatch(createMoreFilterCustomer())
  }
  const onBtnFilterClick = () => {
    dispatch(filtCustomer(selectFilterCustomerValue,inputFilterCustomerValue,limit,currentPage))
  }
  return (
    <div>
    <button onClick={onBtnAddClick} className="btn btn-success mb-3 w-20 "><AddIcon /></button>
    <Flex className="mb-3 mt-3 " px='25px' mb='20px'  >
      <div className="flex-col flex w-full">
      {filterCustomerArray ? filterCustomerArray.map((element, index) => {
        return (
          <div key={index} className=" flex">
            <select data-key={index} value={selectFilterCustomerValue[index]} onChange={onSelectFilterChange}  className="w-1/5 form-control" style={{
              borderTopRightRadius: 0, borderBottomRightRadius: 0
            }}>
              <option value={""}>Type</option>
              <option value={"fullName"}>Name</option>
              <option value={"email"}>Email</option>
              <option value={"phone"}>Phone</option>
            </select>
            <input data-key={index} onChange={onTextFieldFilterChange} value={inputFilterCustomerValue[index]} style={{ borderRadius: 0 }} className="form-control " placeholder="Input data  to filt" />
          </div>
        )
    }) : ""}
    </div>
          <div className="w-1/5">
            < button onClick={onBtnFilterClick} style = {{ borderTopLeftRadius: 0, borderBottomLeftRadius: 0,height:"100%"}}
             className="btn btn-warning w-full  ">Filter</button>
            </div>
  </Flex>
    <button onClick={onBtnMoreFilterClick} className="btn btn-warning ml-5 mb-3"><FilterListIcon /></button>
    <Table className="table-dark" striped>
      <thead>
        <tr>
          {columnsCustomerTable.map((element, index) => {
            return (<th key={index}>{element.Header}</th>)
          })}
        </tr>
      </thead>
      <tbody>
        {props.tableData.map((element, index) => {
          return (
            <tr key={index}>
              <td>{element.fullName}</td>
              <td>{element.email}</td>
              <td>{element.phone}</td>
              <td>{element.address}</td>
              <td>{element.city}</td>
              <td>
                <Flex align='center' style={{ justifyContent: "start" }}>
                  <button onClick={() => onBtnEditClick(element)} className="btn btn-primary text-white"><EditIcon /></button>
                  <button onClick={() => onBtnDeleteClick(element)} style={{ background: "red" }} className="btn text-white"><DeleteIcon /></button>
                </Flex>
              </td>
            </tr>
          )
        })}
      </tbody>
    </Table>
    {props.tableData.length===0?<img src={productNotFound}></img>:""}
  </div >
  );
}
