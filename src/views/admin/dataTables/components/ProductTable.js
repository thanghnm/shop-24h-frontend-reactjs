import {
  Flex,
  Progress,
  Icon,
  Tbody,
  Td,
  Text,
  Th,
  Thead,
  Tr,
  useColorModeValue,
} from "@chakra-ui/react";
import React, { useMemo } from "react";
import {
  useGlobalFilter,
  usePagination,
  useSortBy,
  useTable,
} from "react-table";
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import AddIcon from '@mui/icons-material/Add';
// Custom components
import Card from "../../../../components/card/Card";
import Menu from "../../../../components/menu/MainMenu";
// Assets
// import { MdCheckCircle, MdCancel, MdOutlineError } from "react-icons/md";
import { useDispatch, useSelector } from "react-redux";
import { createMoreFilterProduct, filtCustomer, filtProduct, inputFilterCustomer, inputFilterProduct, openAddCustomerModal, openAddProductModal, openDeleteCustomerModal, openDeleteProductModal, openEditCustomerModal, openEditProductModal, selectFilterCustomer, selectFilterProduct } from "../../../../action/table.action";
// import { MenuItem, Select } from "@mui/material";
import { columnsProductTable } from "../variables/columnsData";
import { Table } from "react-bootstrap";
import FilterListIcon from '@mui/icons-material/FilterList';
import productNotFound from "../../../../assets/images/shopping-cart/product-not-found.png"

export default function ColumnsTable(props) {
  const { columnsData, tableData } = props;
  const { inputFilterProductValue, selectFilterProductValue, limit, currentPage, filterProductArray,
  } = useSelector((reduxData) => reduxData.tableReducers)
  const columns = useMemo(() => columnsData, [columnsData]);
  const data = useMemo(() => tableData, [tableData]);

  const tableInstance = useTable(
    {
      columns,
      data,
    },
    useGlobalFilter,
    useSortBy,
    usePagination
  );
  const {
    initialState,
  } = tableInstance;
  initialState.pageSize = 8;
  const dispatch = useDispatch()
  const onBtnAddClick = () => {
    dispatch(openAddProductModal())
  }
  const onBtnEditClick = (element) => {
    dispatch(openEditProductModal(element))
  }
  const onBtnDeleteClick = (element) => {
    dispatch(openDeleteProductModal(element))
  }
  const onSelectFilterChange = (event) => {
    dispatch(selectFilterProduct(event))
  }
  const onTextFieldFilterChange = (event) => {
    dispatch(inputFilterProduct(event))
  }
  const onBtnFilterClick = () => {
    dispatch(filtProduct(selectFilterProductValue, inputFilterProductValue, 5, currentPage))
  }
  const onBtnMoreFilterClick = () => {
    dispatch(createMoreFilterProduct())
  }
  return (
    <div>
      <button onClick={onBtnAddClick} className="btn btn-success mb-3 w-20 mt-3"><AddIcon /></button>
      <Flex className="mb-3 mt-3" px='25px' mb='20px' >
        <div className="flex-col flex w-full">
          {filterProductArray ? filterProductArray.map((element, index) => {
            return (
              <div key={index} className=" flex">
                <select data-key={index} value={selectFilterProductValue[index]} onChange={onSelectFilterChange} className="w-1/5 form-control" style={{
                  borderTopRightRadius: 0, borderBottomRightRadius: 0
                }}>
                  <option value={""}>Type</option>
                  <option value={"name"}>Name</option>
                  <option value={"amount"}>Amount</option>
                  <option value={"promotionPrice"}>Promotion price</option>
                  <option value={"buyPrice"}>Buy price</option>
                </select>
                <input data-key={index} onChange={onTextFieldFilterChange} value={inputFilterProductValue[index]} style={{ borderRadius: 0 }} className="form-control " placeholder="Input data  to filt" />
              </div>
            )
          }) : ""}
        </div>
        <div className="w-1/5">
          < button onClick={onBtnFilterClick} style={{ borderTopLeftRadius: 0, borderBottomLeftRadius: 0, height: "100%" }}
            className="btn btn-warning w-full  ">Filter</button>
        </div>
      </Flex>
      <button onClick={onBtnMoreFilterClick} className="btn btn-warning ml-5 mb-3"><FilterListIcon /></button>

      <Table className="table-dark" striped>
        <thead>
          <tr>
            {columnsProductTable.map((element, index) => {
              return (<th key={index}>{element.Header}</th>)
            })}
          </tr>
        </thead>
        <tbody>
          {props.tableData.map((element, index) => {
            return (
              <tr key={index}>
                <td>{element.name}</td>
                <td style={{width:"300px"}}><img className="admin-product-img" src={element.imageUrl}></img></td>
                <td>{element.promotionPrice}</td>
                <td>{element.buyPrice}</td>
                <td>{element.amount}</td>
                <td>
                  <Flex align='center' style={{ justifyContent: "start" }}>
                    <button onClick={() => onBtnEditClick(element)} className="btn btn-primary text-white"><EditIcon /></button>
                    <button onClick={() => onBtnDeleteClick(element)} style={{ background: "red" }} className="btn text-white"><DeleteIcon /></button>
                  </Flex>
                </td>
              </tr>
            )
          })}
        </tbody>
      </Table>
    {props.tableData.length===0?<img src={productNotFound}></img>:""}

    </div>
  );
}
