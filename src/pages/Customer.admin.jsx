
// Chakra imports
import { Box, useDisclosure } from '@chakra-ui/react';
import Footer from '../components/footerAdmin/FooterAdmin';
// Layout components
import Sidebar from '../components/sidebar/Sidebar.js';
import { SidebarContext } from '../contexts/SidebarContext';
import React, { useEffect, useState } from 'react';
import { Route } from 'react-router-dom';
import routes from '../routes.js';
import CustomerTable from '../views/admin/dataTables/Customer.table.jsx';
import Breadcrumbs from '../components/Content/Breadcrumbs/Breadcrumbs.jsx';
import { ModalAddCustomer, ModalSuccess, ModalDeleteCustomer, ModalEditCustomer } from '../components/Content/Modal/Modal.component.jsx';
import { useDispatch, useSelector } from 'react-redux';
import { Pagination } from '@mui/material';
import { authorizeAdmin, pageChangePagination } from '../action/table.action.js';
import LogIn from './LogIn.jsx';

// Custom Chakra theme
const Dashboard = (props) => {
	const { openCreateCustomerSuccessModal, openModalAddCustomer, openModalEditCustomer,noPage,authorize,
		openModalDeleteCustomer, openUpdateCustomerSuccessModal, openDeleteCustomerSuccessModal, currentPage } =
		useSelector((reduxData) => reduxData.tableReducers)
		const { pending } =useSelector((reduxData) => reduxData.logInReducers)
		useEffect(() => {
			dispatch(authorizeAdmin())
		}, [pending === false])
	const { ...rest } = props;
	// states and functions
	const [fixed] = useState(false);
	const [toggleSidebar, setToggleSidebar] = useState(false);
	// functions for changing the states from components
	const getRoute = () => {
		return window.location.pathname !== '/admin/full-screen-maps';
	};
	const getActiveRoute = (routes) => {
		let activeRoute = 'Default Brand Text';
		for (let i = 0; i < routes.length; i++) {
			if (routes[i].collapse) {
				let collapseActiveRoute = getActiveRoute(routes[i].items);
				if (collapseActiveRoute !== activeRoute) {
					return collapseActiveRoute;
				}
			} else if (routes[i].category) {
				let categoryActiveRoute = getActiveRoute(routes[i].items);
				if (categoryActiveRoute !== activeRoute) {
					return categoryActiveRoute;
				}
			} else {
				if (window.location.href.indexOf(routes[i].layout + routes[i].path) !== -1) {
					return routes[i].name;
				}
			}
		}
		return activeRoute;
	};
	const getActiveNavbar = (routes) => {
		let activeNavbar = false;
		for (let i = 0; i < routes.length; i++) {
			if (routes[i].collapse) {
				let collapseActiveNavbar = getActiveNavbar(routes[i].items);
				if (collapseActiveNavbar !== activeNavbar) {
					return collapseActiveNavbar;
				}
			} else if (routes[i].category) {
				let categoryActiveNavbar = getActiveNavbar(routes[i].items);
				if (categoryActiveNavbar !== activeNavbar) {
					return categoryActiveNavbar;
				}
			} else {
				if (window.location.href.indexOf(routes[i].layout + routes[i].path) !== -1) {
					return routes[i].secondary;
				}
			}
		}
		return activeNavbar;
	};
	const getActiveNavbarText = (routes) => {
		let activeNavbar = false;
		for (let i = 0; i < routes.length; i++) {
			if (routes[i].collapse) {
				let collapseActiveNavbar = getActiveNavbarText(routes[i].items);
				if (collapseActiveNavbar !== activeNavbar) {
					return collapseActiveNavbar;
				}
			} else if (routes[i].category) {
				let categoryActiveNavbar = getActiveNavbarText(routes[i].items);
				if (categoryActiveNavbar !== activeNavbar) {
					return categoryActiveNavbar;
				}
			} else {
				if (window.location.href.indexOf(routes[i].layout + routes[i].path) !== -1) {
					return routes[i].messageNavbar;
				}
			}
		}
		return activeNavbar;
	};
	const getRoutes = (routes) => {
		return routes.map((prop, key) => {
			if (prop.layout === '/admin') {
				return <Route path={prop.layout + prop.path} component={prop.component} key={key} />;
			}
			if (prop.collapse) {
				return getRoutes(prop.items);
			}
			if (prop.category) {
				return getRoutes(prop.items);
			} else {
				return null;
			}
		});
	};
	document.documentElement.dir = 'ltr';
	const { onOpen } = useDisclosure();
	document.documentElement.dir = 'ltr';
	const dispatch=useDispatch()
	const onChangePagination = (event, value) => {
        dispatch(pageChangePagination(value));
    }

	return (
		<Box>
			{authorize ?<Box>
				<SidebarContext.Provider
					value={{
						toggleSidebar,
						setToggleSidebar
					}}
				>
					<Sidebar routes={routes} display='none' {...rest} />
					<Box
						className='pt-5'
						float='right'
						minHeight='100vh'
						height='100%'
						overflow='auto'
						position='relative'
						maxHeight='100%'
						w={{ base: '100%', xl: 'calc( 100% - 290px )' }}
						maxWidth={{ base: '100%', xl: 'calc( 100% - 290px )' }}
						transition='all 0.33s cubic-bezier(0.685, 0.0473, 0.346, 1)'
						transitionDuration='.2s, .2s, .35s'
						transitionProperty='top, bottom, width'
						transitionTimingFunction='linear, linear, ease'>
						<Breadcrumbs marginTop="50px" parentPage="Admin" thisPage="Customer" color="blue" />
						<h2 style={{ color: "blue" }}>Admin Customer</h2>
						{getRoute() ? (
							<Box mx='auto' p={{ base: '20px', md: '30px' }} pe='20px' minH='100vh' pt='50px'>
								{/* <Routes>
									{getRoutes(routes)}
									<Navigate from='/' to='/admin/default' />
								</Routes> */}
								<CustomerTable/>
								<Pagination className="text-center" color="primary"
									count={noPage} page={currentPage} onChange={onChangePagination} variant="outlined" shape="rounded" />
							</Box>
						) : null}
						<Box>
							<Footer />
						</Box>
					</Box>
				</SidebarContext.Provider>
				<ModalAddCustomer />
				<ModalEditCustomer />
				<ModalDeleteCustomer />
				{openModalAddCustomer === true ? <ModalSuccess content="Create customer successfully !!!" openModal={openCreateCustomerSuccessModal} /> : ""}
				{openModalEditCustomer === true ? <ModalSuccess content="Update customer successfully !!!" openModal={openUpdateCustomerSuccessModal} /> : ""}
				{openModalDeleteCustomer === true ? <ModalSuccess content="Delete customer successfully !!!" openModal={openDeleteCustomerSuccessModal} /> : ""}
			</Box>
			:<LogIn/>}
		</Box>
	);
}
export default Dashboard