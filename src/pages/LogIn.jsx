import LogInForm from "../components/Content/LogInForm/LogInForm"
import CopyRight from "../components/Footer/coppyright.footer"
import TopHeader from "../components/Header/TopHeader/TopHeader.header"

const LogIn=()=>{
    return(
        <div >
        <TopHeader></TopHeader>
        <LogInForm></LogInForm>
        <CopyRight />
        </div>
    )
}
 export default LogIn