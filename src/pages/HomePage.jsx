import Header from '../components/Header/Header.component';
import Footer from '../components/Footer/footer.components';
import Content from '../components/Content/Content.component';
const HomePage = () => {
// localStorage.setItem("cartItems", JSON.stringify([]))

    return (
        <>
            <Header></Header>
            <Content></Content>
            <Footer></Footer>
        </>
    )
}
export default HomePage