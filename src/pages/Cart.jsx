import Breadcrumbs from "../components/Content/Breadcrumbs/Breadcrumbs"
import CartContent from "../components/Content/CartContent/Cart.content"
import Footer from "../components/Footer/footer.components"
import Header from "../components/Header/Header.component"

const Cart =()=>{
    return(
        <>
        <Header/>
        <Breadcrumbs thisPage={"Cart"} />
        <CartContent/>
        <Footer/>
        </>
    )
}
export default Cart