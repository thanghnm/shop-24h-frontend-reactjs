import Header from '../components/Header/Header.component';
import Footer from '../components/Footer/footer.components';
import ListComponent from '../components/Content/List.component';
const ProductList = () => {
    return (
        <>
            <Header></Header>
            <ListComponent/>
            <Footer></Footer>
        </>
    )
}
export default ProductList