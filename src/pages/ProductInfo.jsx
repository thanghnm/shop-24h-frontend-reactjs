import Breadcrumbs from "../components/Content/Breadcrumbs/Breadcrumbs"
import Policy from "../components/Content/ProductInfo/Policy.product"
import Footer from "../components/Footer/footer.components"
import Header from "../components/Header/Header.component"
import ProductDetail from "../components/productDetail/ProductDetail"


const ProductInfo=()=>{
    return(
        <>
        <Header/>
        <ProductDetail/>
        <Policy/>
        <Footer/>
        </>
    )
}
 export default ProductInfo