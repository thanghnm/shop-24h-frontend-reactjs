import SignUpForm from "../components/Content/SignUpForm/SignUpForm"
import CopyRight from "../components/Footer/coppyright.footer"
import TopHeader from "../components/Header/TopHeader/TopHeader.header"

const SignUp = () => {
    return (
        <>
            <TopHeader></TopHeader>
            <SignUpForm/>
            <CopyRight />
        </>

    )
}
export default SignUp