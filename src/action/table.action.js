import { ADMIN_LOGIN_SUCCESS, ADMIN_LOGIN_UNAUTHORIZE, ADMIN_PRODUCT_ADD_BUYPRICE_INVALID, ADMIN_PRODUCT_ADD_IMAGE_INVALID, ADMIN_PRODUCT_ADD_MODAL_AMOUNT_CHANGE, ADMIN_PRODUCT_ADD_MODAL_BUYPRICE_CHANGE, ADMIN_PRODUCT_ADD_MODAL_DESC_CHANGE, ADMIN_PRODUCT_ADD_MODAL_NAME_CHANGE, ADMIN_PRODUCT_ADD_MODAL_PROMOTIONPRICE_CHANGE, ADMIN_PRODUCT_ADD_MODAL_TYPE_CHANGE, ADMIN_PRODUCT_ADD_MODAL_URL_CHANGE, ADMIN_PRODUCT_ADD_NAME_INVALID, ADMIN_PRODUCT_ADD_PROMOTIONPRICE_INVALID, ADMIN_PRODUCT_ADD_TYPE_INVALID, ADMIN_PRODUCT_CREATE_ERROR, ADMIN_PRODUCT_CREATE_PENDING, ADMIN_PRODUCT_CREATE_SUCCESS, ADMIN_PRODUCT_DELETE_ERROR, ADMIN_PRODUCT_DELETE_FAIL, ADMIN_PRODUCT_DELETE_PENDING, ADMIN_PRODUCT_DELETE_SUCCESS, ADMIN_PRODUCT_EDIT_BUYPRICE_INVALID, ADMIN_PRODUCT_EDIT_IMAGE_INVALID, ADMIN_PRODUCT_EDIT_MODAL_AMOUNT_CHANGE, ADMIN_PRODUCT_EDIT_MODAL_BUYPRICE_CHANGE, ADMIN_PRODUCT_EDIT_MODAL_DESC_CHANGE, ADMIN_PRODUCT_EDIT_MODAL_NAME_CHANGE, ADMIN_PRODUCT_EDIT_MODAL_PROMOTIONPRICE_CHANGE, ADMIN_PRODUCT_EDIT_MODAL_TYPE_CHANGE, ADMIN_PRODUCT_EDIT_MODAL_URL_CHANGE, ADMIN_PRODUCT_EDIT_NAME_INVALID, ADMIN_PRODUCT_EDIT_PROMOTIONPRICE_INVALID, ADMIN_PRODUCT_EDIT_TYPE_INVALID, ADMIN_PRODUCT_FETCH_ERROR, ADMIN_PRODUCT_FETCH_PENDING, ADMIN_PRODUCT_FETCH_SUCCESS, ADMIN_PRODUCT_FILT_ERROR, ADMIN_PRODUCT_FILT_MORE, ADMIN_PRODUCT_FILT_SUCCESS, ADMIN_PRODUCT_INPUT_FILTER_CHANGE, ADMIN_PRODUCT_OPEN_ADD_MODAL, ADMIN_PRODUCT_OPEN_DELETE_MODAL, ADMIN_PRODUCT_OPEN_EDIT_MODAL, ADMIN_PRODUCT_SELECT_FILTER_CHANGE, ADMIN_PRODUCT_UPDATE_ERROR, ADMIN_PRODUCT_UPDATE_PENDING, ADMIN_PRODUCT_UPDATE_SUCCESS, ADMIN_TYPE_FETCH_ERROR, ADMIN_TYPE_FETCH_PENDING, ADMIN_TYPE_FETCH_SUCCESS, CUSTOMER_ADD_EMAIL_INVALID, CUSTOMER_ADD_FULLNAME_INVALID, CUSTOMER_ADD_MODAL_ADDRESS_CHANGE, CUSTOMER_ADD_MODAL_CITY_CHANGE, CUSTOMER_ADD_MODAL_COUNTRY_CHANGE, CUSTOMER_ADD_MODAL_EMAIL_CHANGE, CUSTOMER_ADD_MODAL_FULLNAME_CHANGE, CUSTOMER_ADD_MODAL_PHONE_CHANGE, CUSTOMER_ADD_PHONE_INVALID, CUSTOMER_CREATE_ERROR, CUSTOMER_CREATE_PENDING, CUSTOMER_CREATE_SUCCESS, CUSTOMER_DELETE_ERROR, CUSTOMER_DELETE_PENDING, CUSTOMER_DELETE_SUCCESS, CUSTOMER_EDIT_MODAL_ADDRESS_CHANGE, CUSTOMER_EDIT_MODAL_CITY_CHANGE, CUSTOMER_EDIT_MODAL_COUNTRY_CHANGE, CUSTOMER_EDIT_MODAL_EMAIL_CHANGE, CUSTOMER_EDIT_MODAL_FULLNAME_CHANGE, CUSTOMER_EDIT_MODAL_PHONE_CHANGE, CUSTOMER_FETCH_ERROR, CUSTOMER_FETCH_PENDING, CUSTOMER_FETCH_SUCCESS, CUSTOMER_FILT_ERROR, CUSTOMER_FILT_MORE, CUSTOMER_FILT_SUCCESS, CUSTOMER_INPUT_FILTER_CHANGE, CUSTOMER_OPEN_ADD_MODAL, CUSTOMER_OPEN_DELETE_MODAL, CUSTOMER_OPEN_EDIT_MODAL, CUSTOMER_PAGE_CHANGE, CUSTOMER_SELECT_FILTER_CHANGE, CUSTOMER_UPDATE_ERROR, CUSTOMER_UPDATE_PENDING, CUSTOMER_UPDATE_SUCCESS, LOG_IN_ERROR, ORDER_CART_FETCH_ERROR, ORDER_CART_FETCH_PENDING, ORDER_DELETE_ERROR, ORDER_DELETE_PENDING, ORDER_DELETE_SUCCESS, ORDER_EDIT_MODAL_NOTE_CHANGE, ORDER_EDIT_SHIPDATE_CHANGE, ORDER_FETCH_ERROR, ORDER_FETCH_PENDING, ORDER_FETCH_SUCCESS, ORDER_FILT_ERROR, ORDER_FILT_MORE, ORDER_FILT_SUCCESS, ORDER_INPUT_FILTER_CHANGE, ORDER_OPEN_ADD_CART, ORDER_OPEN_ADD_PRODUCTLIST, ORDER_OPEN_DELETE_MODAL, ORDER_OPEN_EDIT_CART, ORDER_OPEN_EDIT_MODAL, ORDER_OPEN_EDIT_PRODUCTLIST, ORDER_SELECT_FILTER_CHANGE, ORDER_UPDATE_ERROR, ORDER_UPDATE_SUCCESS } from "../constants/constants";
import { isEmailValidate, isPhoneValidate } from "./signup.action";
// Hàm fetch dữ liệu customer
export const fetchCustomer = (limit, page) => {
    return async (dispatch) => {
        try {
            var requestOptions = {
                method: 'GET',
                redirect: 'follow'
            };
            await dispatch({
                type: CUSTOMER_FETCH_PENDING
            })
            const responseTotal = await fetch("http://localhost:8000/api/v1/customers", requestOptions);
            const dataTotal = await responseTotal.json();
            // searchParam để phân trang
            const params = new URLSearchParams({
                limit: limit,
                page: page
            });
            const response = await fetch("http://localhost:8000/api/v1/customers?" + params.toString(), requestOptions);
            const data = await response.json();
            return dispatch({
                type: CUSTOMER_FETCH_SUCCESS,
                total: dataTotal.result.length,
                data: data,
            })
        } catch (error) {
            return dispatch({
                type: CUSTOMER_FETCH_ERROR,
                error: error
            })

        }
    }
}
// hàm xử lý khi click button +
export const openAddCustomerModal = () => {
    return {
        type: CUSTOMER_OPEN_ADD_MODAL
    }
}
// hàm xử lý khi click button edit

export const openEditCustomerModal = (element) => {
    return {
        type: CUSTOMER_OPEN_EDIT_MODAL,
        customerId: element._id,
        payload: {
            fullName: element.fullName,
            email: element.email,
            phone: element.phone,
            address: element.address,
            city: element.city,
            country: element.country,
        }
    }
}
// hàm xử lý khi click button delete
export const openDeleteCustomerModal = (element) => {
    return {
        type: CUSTOMER_OPEN_DELETE_MODAL,
        customerId: element._id
    }
}
// Hàm xử lý sự kiện onchange của input Fullname modal Add
export const changeFullNameAddCustomer = (event) => {
    return {
        type: CUSTOMER_ADD_MODAL_FULLNAME_CHANGE,
        payload: event.target.value
    }
}
// Hàm xử lý sự kiện onchange của input Email modal Add
export const changeEmailAddCustomer = (event) => {
    return {
        type: CUSTOMER_ADD_MODAL_EMAIL_CHANGE,
        payload: event.target.value
    }
}
// Hàm xử lý sự kiện onchange của input Phone modal Add
export const changePhoneAddCustomer = (event) => {
    return {
        type: CUSTOMER_ADD_MODAL_PHONE_CHANGE,
        payload: event.target.value
    }
}
// Hàm xử lý sự kiện onchange của input Address modal Add
export const changeAddressAddCustomer = (event) => {
    return {
        type: CUSTOMER_ADD_MODAL_ADDRESS_CHANGE,
        payload: event.target.value
    }
}
// Hàm xử lý sự kiện onchange của input City modal Add
export const changeCityAddCustomer = (event) => {
    return {
        type: CUSTOMER_ADD_MODAL_CITY_CHANGE,
        payload: event.target.value
    }
}
// Hàm xử lý sự kiện onchange của input Country modal Add
export const changeCountryAddCustomer = (event) => {
    return {
        type: CUSTOMER_ADD_MODAL_COUNTRY_CHANGE,
        payload: event.target.value
    }
}
// Hàm xử lý sự kiện onclick của button create customer modal Add
export const createCustomer = (stateAddCustomer) => {
    return async (dispatch) => {
        try {
            const vCustomer = {
                fullName: stateAddCustomer.fullName,
                phone: stateAddCustomer.phone,
                email: stateAddCustomer.email,
                address: stateAddCustomer.address,
                city: stateAddCustomer.city,
                country: stateAddCustomer.country
            }
            if (!vCustomer.fullName) {
                return dispatch({
                    type: CUSTOMER_ADD_FULLNAME_INVALID,
                    message: "Fullname is required"
                })
            }
            if (!vCustomer.email) {
                return dispatch({
                    type: CUSTOMER_ADD_EMAIL_INVALID,
                    message: "Email is required"
                })
            }
            if (isEmailValidate(vCustomer.email) === false) {
                return dispatch({
                    type: CUSTOMER_ADD_EMAIL_INVALID,
                    message: "Email is invalid"
                })
            }
            if (!vCustomer.phone) {
                return dispatch({
                    type: CUSTOMER_ADD_PHONE_INVALID,
                    message: "Phone is required"
                })
            }

            if (isPhoneValidate(vCustomer.phone) === false) {
                return dispatch({
                    type: CUSTOMER_ADD_PHONE_INVALID,
                    message: "Phone is invalid"
                })
            }


            var requestOptionsPostCustomer = {
                method: 'POST',
                redirect: 'follow',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(vCustomer)
            };
            await dispatch({
                type: CUSTOMER_CREATE_PENDING
            })
            const paramsPhone = new URLSearchParams({
                phone: vCustomer.phone,
            });
            const paramsEmail = new URLSearchParams({
                email: vCustomer.email,
            });
            var requestOptions = {
                method: 'GET',
                redirect: 'follow'
            };
            // Kiểm tra số điện thoại đã sử dụng hay chưa
            const checkDublicatePhone = await fetch("http://localhost:8000/api/v1/customers?" + paramsPhone.toString(), requestOptions);
            const dataPhone = await checkDublicatePhone.json()
            if (dataPhone.result.length > 0) {
                return dispatch({
                    type: CUSTOMER_ADD_PHONE_INVALID,
                    message: "Phone is already used"
                })
            }
            // Kiểm tra email đã sử dụng hay chưa
            const checkDublicateEmail = await fetch("http://localhost:8000/api/v1/customers?" + paramsEmail.toString(), requestOptions);
            const dataEmail = await checkDublicateEmail.json()
            if (dataEmail.result.length > 0) {
                return dispatch({
                    type: CUSTOMER_ADD_EMAIL_INVALID,
                    message: "Email is already used"
                })
            }
            // Post Customer
            const responseTotal = await fetch("http://localhost:8000/api/v1/customers", requestOptionsPostCustomer);
            if (responseTotal.status === 201) {
                return dispatch({
                    type: CUSTOMER_CREATE_SUCCESS,
                })
            }

        } catch (error) {
            return dispatch({
                type: CUSTOMER_CREATE_ERROR,
                error: error
            })

        }
    }
}
// Hàm xử lý sự kiện onchange của input Fullname modal Edit
export const changeFullNameEditCustomer = (event) => {
    return {
        type: CUSTOMER_EDIT_MODAL_FULLNAME_CHANGE,
        payload: event.target.value
    }
}
// Hàm xử lý sự kiện onchange của input Email modal Edit
export const changeEmailEditCustomer = (event) => {
    return {
        type: CUSTOMER_EDIT_MODAL_EMAIL_CHANGE,
        payload: event.target.value
    }
}
// Hàm xử lý sự kiện onchange của input Phone modal Edit
export const changePhoneEditCustomer = (event) => {
    return {
        type: CUSTOMER_EDIT_MODAL_PHONE_CHANGE,
        payload: event.target.value
    }
}
// Hàm xử lý sự kiện onchange của input Address modal Edit
export const changeAddressEditCustomer = (event) => {
    return {
        type: CUSTOMER_EDIT_MODAL_ADDRESS_CHANGE,
        payload: event.target.value
    }
}
// Hàm xử lý sự kiện onchange của input City modal Edit
export const changeCityEditCustomer = (event) => {
    return {
        type: CUSTOMER_EDIT_MODAL_CITY_CHANGE,
        payload: event.target.value
    }
}
// Hàm xử lý sự kiện onchange của input Country modal Edit
export const changeCountryEditCustomer = (event) => {
    return {
        type: CUSTOMER_EDIT_MODAL_COUNTRY_CHANGE,
        payload: event.target.value
    }
}
// Hàm xử lý sự kiện onclick của button update customer modal Edit
export const updateCustomer = (stateUpdateCustomer, customerId) => {
    return async (dispatch) => {
        try {
            const vCustomer = {
                fullName: stateUpdateCustomer.fullName,
                phone: stateUpdateCustomer.phone,
                email: stateUpdateCustomer.email,
                address: stateUpdateCustomer.address,
                city: stateUpdateCustomer.city,
                country: stateUpdateCustomer.country
            }
            // validate data sau khi edit
            if (!vCustomer.fullName) {
                alert("fullName is required")
                return false
            }
            if (!vCustomer.phone) {
                alert("phone is required")
                return false
            }
            if (isPhoneValidate(vCustomer.phone) === false) {
                alert("phone is invalid")
                return false
            }
            if (!vCustomer.email) {
                alert("email is required")
                return false
            }
            if (isEmailValidate(vCustomer.email) === false) {
                alert("email is invalid")
                return false
            }
            var requestOptionsPostCustomer = {
                method: 'PUT',
                redirect: 'follow',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(vCustomer)
            };

            await dispatch({
                type: CUSTOMER_UPDATE_PENDING
            })

            const responseTotal = await fetch("http://localhost:8000/api/v1/customers" + "/" + customerId, requestOptionsPostCustomer);
            return dispatch({
                type: CUSTOMER_UPDATE_SUCCESS
            })
        } catch (error) {
            return dispatch({
                type: CUSTOMER_UPDATE_ERROR,
                error: error
            })

        }
    }
}
// Hàm xử lý sự kiện đổi trang
export const pageChangePagination = (page) => {
    return {
        type: CUSTOMER_PAGE_CHANGE,
        page: page
    }
}
// Hàm xử lý sự kiện onclick của button delete customer
export const deleteCustomer = (customerId) => {
    return async (dispatch) => {
        try {

            var requestOptionsDeleteCustomer = {
                method: 'DELETE',
                redirect: 'follow',
            };
            await dispatch({
                type: CUSTOMER_DELETE_PENDING
            })
            const responseTotal = await fetch("http://localhost:8000/api/v1/customers" + "/" + customerId, requestOptionsDeleteCustomer);
            return dispatch({
                type: CUSTOMER_DELETE_SUCCESS
            })
        } catch (error) {
            return dispatch({
                type: CUSTOMER_DELETE_ERROR,
                error: error
            })
        }
    }
}

// Hàm xử lý sự kiện onchange của input  filter
export const inputFilterCustomer = (event) => {
    return {
        type: CUSTOMER_INPUT_FILTER_CHANGE,
        payload: event.target.value,
        key:event.target.dataset.key
    }
}
// Hàm xử lý sự kiện onchange của input  filter
export const selectFilterCustomer = (event) => {
    return {
        type: CUSTOMER_SELECT_FILTER_CHANGE,
        payload: event.target.value,
        key:event.target.dataset.key
    }
}
// Hàm xử lý sự kiện onchange của input  filter
export const filtCustomer = (typeFilter, dataFilter, limit, currentPage) => {
    return async (dispatch) => {
        try {

            var requestOptionsGet = {
                method: 'GET',
                redirect: 'follow',
            };
            const params = new URLSearchParams({
                limit: limit,
                page: currentPage
            });
            typeFilter.map((element,index)=>{
                if (element === "fullName") {
                    params.append("fullName",dataFilter[index]) 
                }
                if (element === "email") {
                    params.append("email",dataFilter[index]) 
                }
                if (element === "phone") {
                    params.append("phone",dataFilter[index]) 
                }
            })
            console.log(params.toString())
            const response = await fetch("http://localhost:8000/api/v1/customers" + "?" + params.toString(), requestOptionsGet);
            const data = await response.json()
            console.log(data)
            return dispatch({
                type: CUSTOMER_FILT_SUCCESS,
                payload: data.result
            })
        } catch (error) {
            return dispatch({
                type: CUSTOMER_FILT_ERROR,
                error: error
            })
        }
    }
}
// Order Table
// Hàm fetch dữ liệu order
export const fetchOrder = (limit, page) => {
    return async (dispatch) => {
        try {
            var requestOptions = {
                method: 'GET',
                redirect: 'follow'
            };
            await dispatch({
                type: ORDER_FETCH_PENDING
            })
            const responseTotal = await fetch("http://localhost:8000/api/v1/orders", requestOptions);
            const dataTotal = await responseTotal.json();

            // searchParam để phân trang
            const params = new URLSearchParams({
                limit: limit,
                page: page
            });
            const response = await fetch("http://localhost:8000/api/v1/orders?" + params.toString(), requestOptions);
            const data = await response.json();
            return dispatch({
                type: ORDER_FETCH_SUCCESS,
                total: dataTotal.result.length,
                data: data,
            })
        } catch (error) {
            return dispatch({
                type: ORDER_FETCH_ERROR,
                error: error
            })
        }
    }
}
// hàm xử lý khi click button +
export const openProductList = () => {
    return {
        type: ORDER_OPEN_ADD_PRODUCTLIST,
    }
}
// hàm xử lý khi click button process to checkout
export const openAdminCart = () => {
    return {
        type: ORDER_OPEN_ADD_CART
    }
}
// hàm xử lý khi click button edit order
export const openAdminCartToEdit = (orderObject) => {
    return async (dispatch) => {
        try {
            var requestOptions = {
                method: 'GET',
                redirect: 'follow'
            };
            await dispatch({
                type: ORDER_CART_FETCH_PENDING
            })
            var adminCart = []
            localStorage.setItem("adminCartItems", JSON.stringify(adminCart))
            orderObject.orderDetails.map(async (element, index) => {
                const response = await fetch("http://localhost:8000/api/v1/products/" + element.product, requestOptions);
                const data = await response.json();
                data.result.quantity = element.quantity
                adminCart.push(data.result)
                if (adminCart.length === orderObject.orderDetails.length) {
                    localStorage.setItem("adminCartItems", JSON.stringify(adminCart))
                    return dispatch({
                        type: ORDER_OPEN_EDIT_CART,
                        payload: orderObject._id,
                        note: orderObject.note,
                        shippedDate: orderObject.shippedDate
                    })
                }
            })
        } catch (error) {
            return dispatch({
                type: ORDER_CART_FETCH_ERROR,
                error: error
            })
        }
    }
}
// hàm xử lý khi click button +
export const shoppingMore = () => {
    return {
        type: ORDER_OPEN_EDIT_PRODUCTLIST
    }
}
// hàm xử lý khi click button +
export const openModalEditOrder = () => {
    return {
        type: ORDER_OPEN_EDIT_MODAL
    }
}
// Ham xu ly date time picker
export const editDatePicked = (event) => {
    return {
        type: ORDER_EDIT_SHIPDATE_CHANGE,
        payload: event
    }
}
// hàm xử lý khi click button +
export const inputNoteUpdateOrderChange = (event) => {
    return {
        type: ORDER_EDIT_MODAL_NOTE_CHANGE,
        payload: event.target.value
    }
}

// Hàm fetch dữ liệu product
export const updateOrder = (editOrderState, orderId, totalPrice, itemArr) => {
    return async (dispatch) => {
        try {
            var vOrder = {
                note: editOrderState.note,
                orderDetails: editOrderState.orderDetails,
                cost: totalPrice
            }
            if (editOrderState.shippedDate !== null) {
                vOrder.shippedDate = editOrderState.shippedDate
            }
            var requestOptionsPutOrder = {
                method: 'PUT',
                redirect: 'follow',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(vOrder)
            };

            const responseTotal = await fetch("http://localhost:8000/api/v1/orders/" + orderId, requestOptionsPutOrder);
            const dataTotal = await responseTotal.json();
            itemArr.map(async (element, index) => {
                var vOrderDetail = {
                    product: element._id,
                    quantity: element.quantity
                }
                var requestOptionsPostOrderDetail = {
                    method: 'POST',
                    redirect: 'follow',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(vOrderDetail)
                }
                var createOrderDetails = await fetch(`http://localhost:8000/api/v1/orderdetails/` + orderId, requestOptionsPostOrderDetail)
                const dataCreateOrderDetails = await createOrderDetails.json()
            })
            return dispatch({
                type: ORDER_UPDATE_SUCCESS
            })
        } catch (error) {
            return dispatch({
                type: ORDER_UPDATE_ERROR,
                error: error
            })
        }
    }
}
export const openDeleteOrderModal = (orderObject) => {
    return {
        type: ORDER_OPEN_DELETE_MODAL,
        payload: orderObject._id
    }
}
// Hàm xử lý sự kiện onchange của input  filter
export const selectFilterOrder = (event) => {
    return {
        type: ORDER_SELECT_FILTER_CHANGE,
        payload: event.target.value,
        key: event.target.dataset.key
    }
}
export const inputFilterOrder = (event) => {
    return {
        type: ORDER_INPUT_FILTER_CHANGE,
        payload: event.target.value,
        key: event.target.dataset.key
    }
}
// Hàm xử lý sự kiện onclick của button delete order
export const deleteOrder = (orderId) => {
    return async (dispatch) => {
        try {

            var requestOptionsDeleteCustomer = {
                method: 'DELETE',
                redirect: 'follow',
            };
            await dispatch({
                type: ORDER_DELETE_PENDING
            })
            const responseTotal = await fetch("http://localhost:8000/api/v1/orders" + "/" + orderId, requestOptionsDeleteCustomer);
            return dispatch({
                type: ORDER_DELETE_SUCCESS
            })
        } catch (error) {
            return dispatch({
                type: ORDER_DELETE_ERROR,
                error: error
            })
        }
    }
}
// Hàm xử lý sự kiện click của button filter
export const filtOrder = (typeFilter, dataFilter, limit, currentPage) => {
    return async (dispatch) => {
        try {

            var requestOptionsGet = {
                method: 'GET',
                redirect: 'follow',
            };

            var params = new URLSearchParams({
                limit: limit,
                page: currentPage
            });

            typeFilter.map((element, index) => {
                if (element == "cost") {
                    params.append("cost",dataFilter[index])
                    
                }
                if (element == "code") {
                    params.append("code",dataFilter[index])
                }
            })

            const response = await fetch("http://localhost:8000/api/v1/orders" + "?" + params.toString(), requestOptionsGet);
            const data = await response.json()
            return dispatch({
                type: ORDER_FILT_SUCCESS,
                payload: data.result
            })
        } catch (error) {
            return dispatch({
                type: ORDER_FILT_ERROR,
                error: error
            })
        }
    }
}
// Product Table
// Hàm fetch dữ liệu product
export const fetchProductType = (limit, page) => {
    return async (dispatch) => {
        try {
            var requestOptions = {
                method: 'GET',
                redirect: 'follow'
            };
            await dispatch({
                type: ADMIN_TYPE_FETCH_PENDING
            })
            const responseTotal = await fetch("http://localhost:8000/api/v1/types", requestOptions);
            const dataTotal = await responseTotal.json();
            return dispatch({
                type: ADMIN_TYPE_FETCH_SUCCESS,
                payload: dataTotal.result
            })
        } catch (error) {
            return dispatch({
                type: ADMIN_TYPE_FETCH_ERROR,
                error: error
            })
        }
    }
}
// Hàm xử lý sự kiện onchange của input  filter
export const selectFilterProduct = (event) => {
    return {
        type: ADMIN_PRODUCT_SELECT_FILTER_CHANGE,
        payload: event.target.value,
        key:event.target.dataset.key
    }
}
export const inputFilterProduct = (event) => {
    return {
        type: ADMIN_PRODUCT_INPUT_FILTER_CHANGE,
        payload: event.target.value,
        key:event.target.dataset.key

    }
}
// Ham` fetch product cho table product
export const fetchProduct = (limit, page) => {
    return async (dispatch) => {
        try {
            var requestOptions = {
                method: 'GET',
                redirect: 'follow'
            };
            await dispatch({
                type: ADMIN_PRODUCT_FETCH_PENDING
            })
            const responseTotal = await fetch("http://localhost:8000/api/v1/products", requestOptions);
            const dataTotal = await responseTotal.json();

            // searchParam để phân trang
            const params = new URLSearchParams({
                limit: limit,
                page: page
            });
            const response = await fetch("http://localhost:8000/api/v1/products?" + params.toString(), requestOptions);
            const data = await response.json();
            return dispatch({
                type: ADMIN_PRODUCT_FETCH_SUCCESS,
                total: dataTotal.result.length,
                data: data.result,
            })
        } catch (error) {
            return dispatch({
                type: ADMIN_PRODUCT_FETCH_ERROR,
                error: error
            })
        }
    }
}
// hàm xử lý khi click button +
export const openAddProductModal = () => {
    return {
        type: ADMIN_PRODUCT_OPEN_ADD_MODAL
    }
}
// Hàm xử lý sự kiện onchange của input modal Add
export const changeNameAddProduct = (event) => {
    return {
        type: ADMIN_PRODUCT_ADD_MODAL_NAME_CHANGE,
        payload: event.target.value
    }
}
export const changeBuyPriceAddProduct = (event) => {
    return {
        type: ADMIN_PRODUCT_ADD_MODAL_BUYPRICE_CHANGE,
        payload: event.target.value
    }
}
export const changePromotionPriceAddProduct = (event) => {
    return {
        type: ADMIN_PRODUCT_ADD_MODAL_PROMOTIONPRICE_CHANGE,
        payload: event.target.value
    }
}
export const changeDescriptionAddProduct = (event) => {
    return {
        type: ADMIN_PRODUCT_ADD_MODAL_DESC_CHANGE,
        payload: event.target.value
    }
}
export const changeAmountAddProduct = (event) => {
    return {
        type: ADMIN_PRODUCT_ADD_MODAL_AMOUNT_CHANGE,
        payload: event.target.value
    }
}
export const changeImageUrlAddProduct = (event) => {
    return {
        type: ADMIN_PRODUCT_ADD_MODAL_URL_CHANGE,
        payload: event.target.value
    }
}
export const changeTypeAddProduct = (event) => {
    return {
        type: ADMIN_PRODUCT_ADD_MODAL_TYPE_CHANGE,
        payload: event.target.value
    }
}
// Hàm xử lý sự kiện onclick của button create customer modal Add
export const createProduct = (stateAddProduct) => {
    return async (dispatch) => {
        try {
            const vProduct = {
                name: stateAddProduct.name,
                buyPrice: stateAddProduct.buyPrice,
                promotionPrice: stateAddProduct.promotionPrice,
                description: stateAddProduct.description,
                amount: stateAddProduct.amount,
                type: stateAddProduct.type,
                imageUrl: stateAddProduct.imageUrl
            }
            if (!vProduct.name) {
                return dispatch({
                    type: ADMIN_PRODUCT_ADD_NAME_INVALID,
                    message: "Name is required"
                })
            }
            if (vProduct.type === "0") {
                return dispatch({
                    type: ADMIN_PRODUCT_ADD_TYPE_INVALID,
                    message: "Type is required"
                })
            }
            if (!vProduct.buyPrice) {
                return dispatch({
                    type: ADMIN_PRODUCT_ADD_BUYPRICE_INVALID,
                    message: "Buy price is required"
                })
            }
            if (!vProduct.promotionPrice) {
                return dispatch({
                    type: ADMIN_PRODUCT_ADD_PROMOTIONPRICE_INVALID,
                    message: "Promotion price is required"
                })
            }
            if (!vProduct.imageUrl) {
                return dispatch({
                    type: ADMIN_PRODUCT_ADD_IMAGE_INVALID,
                    message: "Image url is required"
                })
            }
            var requestOptionsPostProduct = {
                method: 'POST',
                redirect: 'follow',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(vProduct)
            };
            await dispatch({
                type: ADMIN_PRODUCT_CREATE_PENDING
            })
            const paramsName = new URLSearchParams({
                name: vProduct.name,
            });

            var requestOptions = {
                method: 'GET',
                redirect: 'follow'
            };
            // Kiểm tra name đã sử dụng hay chưa
            const checkDublicate = await fetch("http://localhost:8000/api/v1/products?" + paramsName.toString(), requestOptions);
            const data = await checkDublicate.json()
            if (data.result.length > 0) {
                return dispatch({
                    type: ADMIN_PRODUCT_ADD_NAME_INVALID,
                    message: "Name is already used"
                })
            }
            // Post Product
            const responseTotal = await fetch("http://localhost:8000/api/v1/products", requestOptionsPostProduct);
            if (responseTotal.status === 201) {
                return dispatch({
                    type: ADMIN_PRODUCT_CREATE_SUCCESS,
                })
            }

        } catch (error) {
            return dispatch({
                type: ADMIN_PRODUCT_CREATE_ERROR,
                error: error
            })

        }
    }
}
// Ham xu ly su kien click nut edit cua row
export const openEditProductModal = (element) => {
    return {
        type: ADMIN_PRODUCT_OPEN_EDIT_MODAL,
        productId: element._id,
        payload: {
            name: element.name,
            buyPrice: element.buyPrice,
            promotionPrice: element.promotionPrice,
            description: element.description,
            amount: element.amount,
            imageUrl: element.imageUrl,
            type: element.type._id
        }
    }
}
// Hàm xử lý sự kiện onchange của input modal Edit
export const changeNameEditProduct = (event) => {
    return {
        type: ADMIN_PRODUCT_EDIT_MODAL_NAME_CHANGE,
        payload: event.target.value
    }
}
export const changeBuyPriceEditProduct = (event) => {
    return {
        type: ADMIN_PRODUCT_EDIT_MODAL_BUYPRICE_CHANGE,
        payload: event.target.value
    }
}
export const changePromotionPriceEditProduct = (event) => {
    return {
        type: ADMIN_PRODUCT_EDIT_MODAL_PROMOTIONPRICE_CHANGE,
        payload: event.target.value
    }
}
export const changeDescriptionEditProduct = (event) => {
    return {
        type: ADMIN_PRODUCT_EDIT_MODAL_DESC_CHANGE,
        payload: event.target.value
    }
}
export const changeAmountEditProduct = (event) => {
    return {
        type: ADMIN_PRODUCT_EDIT_MODAL_AMOUNT_CHANGE,
        payload: event.target.value
    }
}
export const changeImageUrlEditProduct = (event) => {
    return {
        type: ADMIN_PRODUCT_EDIT_MODAL_URL_CHANGE,
        payload: event.target.value
    }
}
export const changeTypeEditProduct = (event) => {
    return {
        type: ADMIN_PRODUCT_EDIT_MODAL_TYPE_CHANGE,
        payload: event.target.value
    }
}
// Hàm xử lý sự kiện onclick của button create customer modal Add
export const updateProduct = (stateEditProduct, productId) => {
    return async (dispatch) => {
        try {
            const vProduct = {
                name: stateEditProduct.name,
                buyPrice: stateEditProduct.buyPrice,
                promotionPrice: stateEditProduct.promotionPrice,
                description: stateEditProduct.description,
                amount: stateEditProduct.amount,
                type: stateEditProduct.type,
                imageUrl: stateEditProduct.imageUrl
            }
            if (!vProduct.name) {
                return dispatch({
                    type: ADMIN_PRODUCT_EDIT_NAME_INVALID,
                    message: "Name is required"
                })
            }
            if (vProduct.type === "0") {
                return dispatch({
                    type: ADMIN_PRODUCT_EDIT_TYPE_INVALID,
                    message: "Type is required"
                })
            }
            if (!vProduct.buyPrice) {
                return dispatch({
                    type: ADMIN_PRODUCT_EDIT_BUYPRICE_INVALID,
                    message: "Buy price is required"
                })
            }
            if (!vProduct.promotionPrice) {
                return dispatch({
                    type: ADMIN_PRODUCT_EDIT_PROMOTIONPRICE_INVALID,
                    message: "Promotion price is required"
                })
            }
            if (!vProduct.imageUrl) {
                return dispatch({
                    type: ADMIN_PRODUCT_EDIT_IMAGE_INVALID,
                    message: "Image url is required"
                })
            }
            var requestOptionsPutProduct = {
                method: 'PUT',
                redirect: 'follow',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(vProduct)
            };
            await dispatch({
                type: ADMIN_PRODUCT_UPDATE_PENDING
            })
            const paramsName = new URLSearchParams({
                name: vProduct.name,
            });

            var requestOptions = {
                method: 'GET',
                redirect: 'follow'
            };
            // Kiểm tra name đã sử dụng hay chưa
            const checkDublicate = await fetch("http://localhost:8000/api/v1/products?" + paramsName.toString(), requestOptions);
            const data = await checkDublicate.json()
            if (data.result.length > 1) {
                return dispatch({
                    type: ADMIN_PRODUCT_EDIT_NAME_INVALID,
                    message: "Name is already used"
                })
            }
            // Post Product
            const responseTotal = await fetch("http://localhost:8000/api/v1/products/" + productId, requestOptionsPutProduct);
            if (responseTotal.status === 200) {
                return dispatch({
                    type: ADMIN_PRODUCT_UPDATE_SUCCESS,
                })
            }
        } catch (error) {
            return dispatch({
                type: ADMIN_PRODUCT_UPDATE_ERROR,
                error: error
            })
        }
    }
}
// hàm xử lý khi click button delete
export const openDeleteProductModal = (element) => {
    return {
        type: ADMIN_PRODUCT_OPEN_DELETE_MODAL,
        productId: element._id
    }
}
// Hàm xử lý sự kiện onclick của button delete product
export const deleteProduct = (productId) => {
    return async (dispatch) => {
        try {

            var requestOptionsDeleteProduct = {
                method: 'DELETE',
                redirect: 'follow',
            };
            var requestOptionsGet = {
                method: 'GET',
                redirect: 'follow',
            };
            await dispatch({
                type: ADMIN_PRODUCT_DELETE_PENDING
            })
            const paramsId = new URLSearchParams({
                productId: productId,
            });
            const responseDetail = await fetch("http://localhost:8000/api/v1/orderdetails/all" + "?" + paramsId.toString(), requestOptionsGet);
            const details = await responseDetail.json()

            if (details.result.length > 0) {
                return dispatch({
                    type: ADMIN_PRODUCT_DELETE_FAIL,
                    message: "Products that have been ordered cannot be deleted!"
                })
            }
            const responseTotal = await fetch("http://localhost:8000/api/v1/customers" + "/" + productId, requestOptionsDeleteProduct);
            return dispatch({
                type: ADMIN_PRODUCT_DELETE_SUCCESS
            })
        } catch (error) {
            return dispatch({
                type: ADMIN_PRODUCT_DELETE_ERROR,
                error: error
            })
        }
    }
}
// Hàm xử lý sự kiện click của button filter
export const filtProduct = (typeFilter, dataFilter, limit, currentPage) => {
    return async (dispatch) => {
        try {

            var requestOptionsGet = {
                method: 'GET',
                redirect: 'follow',
            };

            const params = new URLSearchParams({
                limit: limit,
                page: currentPage
            });

            typeFilter.map((element,index)=>{
                if(element==="name"){
                    params.append("name",dataFilter[index])
                }
                if(element==="amount"){
                    params.append("amount",dataFilter[index])
                }
                if(element==="buyPrice"){
                    params.append("buyPrice",dataFilter[index])
                }
                if(element==="promotionPrice"){
                    params.append("promotionPrice",dataFilter[index])
                }
            })

            const response = await fetch("http://localhost:8000/api/v1/products" + "?" + params.toString(), requestOptionsGet);
            const data = await response.json()
            return dispatch({
                type: ADMIN_PRODUCT_FILT_SUCCESS,
                payload: data.result
            })
        } catch (error) {
            return dispatch({
                type: ADMIN_PRODUCT_FILT_ERROR,
                error: error
            })
        }
    }
}
// Ham xu ly su kien lay user de check out
export const authorizeAdmin = () => {
    return async (dispatch) => {
        try {
            var authorize
            var userRole = sessionStorage.getItem("userRole")
            if (userRole === "admin") {
                authorize = true
                return dispatch({
                    type: ADMIN_LOGIN_SUCCESS,
                    payload: authorize
                })
            }
            else {
                authorize = false
                return dispatch({
                    type: ADMIN_LOGIN_UNAUTHORIZE,
                    payload: authorize
                })
            }

        } catch (error) {
            return dispatch({
                type: LOG_IN_ERROR,
                error: error.message,
            })
        }
    }
}
export const createMoreFilter = () => {
    return {
        type: ORDER_FILT_MORE,
    }
}
export const createMoreFilterProduct = () => {
    return {
        type: ADMIN_PRODUCT_FILT_MORE,
    }
}
export const createMoreFilterCustomer = () => {
    return {
        type: CUSTOMER_FILT_MORE,
    }
}