import { INFO_QUANTITY_PLUS, INFO_QUANTITY_SUBTRACT, INFO_SIZE_L, INFO_SIZE_M, INFO_SIZE_S, INFO_SIZE_XL, INFO_SIZE_XXL, PRODUCTS_INFO_FETCH_ERROR, PRODUCTS_INFO_FETCH_PENDING, PRODUCTS_INFO_FETCH_SUCCESS } from "../constants/constants";
// Ham fetch data
export const fetchProductInfo = (productId) => {
    return async (dispatch) => {
        try {
            var requestOptions = {
                method: 'GET',
                redirect: 'follow'
            };
            await dispatch({
                type: PRODUCTS_INFO_FETCH_PENDING
            })
            const response = await fetch("http://localhost:8000/api/v1/products/"+productId, requestOptions);
            const data = await response.json();
            const searchParams = new URLSearchParams({
                type6:data.result.type,
            })
            const responseOptions = await fetch("http://localhost:8000/api/v1/products?"+searchParams.toString(), requestOptions);
            const dataOptions = await responseOptions.json();
            const index =  dataOptions.result.filter((item)=>item._id!== productId)
            const otherProductArrayLimit4=[]
            for(var bI = 0;bI<3;bI++){
                otherProductArrayLimit4.push(index[bI])
            }
            return dispatch({
                type: PRODUCTS_INFO_FETCH_SUCCESS,
                data: data,
                otherProduct:otherProductArrayLimit4
            })
        } catch (error) {
            return dispatch({
                type: PRODUCTS_INFO_FETCH_ERROR,
                error: error
            })

        }
    }
}
// Ham xu ly su kien checkbox size 
export const chooseSizeS=()=>{
    return({
        type:INFO_SIZE_S
    })
}
export const chooseSizeM=()=>{
    return({
        type:INFO_SIZE_M
    })
}
export const chooseSizeL=()=>{
    return({
        type:INFO_SIZE_L
    })
}
export const chooseSizeXL=()=>{
    return({
        type:INFO_SIZE_XL
    })
}
export const chooseSizeXXL=()=>{
    return({
        type:INFO_SIZE_XXL
    })
}
export const plusItem=()=>{
    return({
        type:INFO_QUANTITY_PLUS
    })
}
export const subtractItem=()=>{
    return({
        type:INFO_QUANTITY_SUBTRACT
    })
}