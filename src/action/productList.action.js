import { ADD_CART, CLOSE_NOTICE_ADDCART, GET_INFO, GET_USER_DATA, LOG_IN_ERROR, PRODUCTS_FETCH_ERROR, PRODUCTS_FETCH_PENDING, PRODUCTS_FETCH_SUCCESS, PRODUCTS_PAGE_CHANGE, PRODUCTS_SEARCH_ERROR, PRODUCTS_SEARCH_PENDING, PRODUCTS_SEARCH_SUCCESS, SEARCH_NAME, SEARCH_PRICE_MAX, SEARCH_PRICE_MIN, SEARCH_TYPE_GLASSES, SEARCH_TYPE_HANDBAG, SEARCH_TYPE_SHIRT, SEARCH_TYPE_SHOE, SEARCH_TYPE_WATCH } from "../constants/constants";
// Ham xu ly  fetch data de render
export const fetchProduct = (limit, page) => {
    return async (dispatch) => {
        try {
            var requestOptions = {
                method: 'GET',
                redirect: 'follow'
            };
            await dispatch({
                type: PRODUCTS_FETCH_PENDING
            })
            const responseTotal = await fetch("http://localhost:8000/api/v1/products", requestOptions);
            const dataTotal = await responseTotal.json();
            const params = new URLSearchParams({
                limit: limit,
                page: page
            });
            const response = await fetch("http://localhost:8000/api/v1/products?" + params.toString(), requestOptions);
            const data = await response.json();
            const productHomePage = []
            for (var bI = 0; bI < 8; bI++) {
                productHomePage.push(dataTotal.result[bI])
            }
            return dispatch({
                type: PRODUCTS_FETCH_SUCCESS,
                total: dataTotal.result.length,
                data: data,
                dataHomePage: productHomePage,
            })
        } catch (error) {
            return dispatch({
                type: PRODUCTS_FETCH_ERROR,
                error: error
            })

        }
    }
}
// Ham xu ly btn search
export const searchProduct = (slider, checkbox, search) => {
    return async (dispatch) => {
        try {
            var requestOptions = {
                method: 'GET',
                redirect: 'follow'
            };
            await dispatch({
                type: PRODUCTS_SEARCH_PENDING
            })
            var paramSearch = new URLSearchParams({
                min: slider.value[0],
                max: slider.value[1],
                // limit:limit,
                // page:page
            })
            var paramSearchTotal = new URLSearchParams({
                min: slider.value[0],
                max: slider.value[1],
            })
            if (checkbox.shirt === true) {
                paramSearch.append("type1", '6554abcffb28ce75cfb1a2ae')
                paramSearchTotal.append("type1", '6554abcffb28ce75cfb1a2ae')

            }
            if (checkbox.glasses) {
                paramSearch.append("type2", '655482d7b703a749418bc4f7')
                paramSearchTotal.append("type2", '655482d7b703a749418bc4f7')

            }
            if (checkbox.handbag === true) {
                paramSearch.append("type3", '6554ade6fb28ce75cfb1a2bc')
                paramSearchTotal.append("type3", '6554ade6fb28ce75cfb1a2bc')

            }
            if (checkbox.watch === true) {
                paramSearch.append("type4", '655f5bcfb41c9db92fcad9fc')
                paramSearchTotal.append("type4", '655f5bcfb41c9db92fcad9fc')
            }
            if (checkbox.shoe === true) {
                paramSearch.append("type5", '655f5be4b41c9db92fcad9fe')
                paramSearchTotal.append("type5", '655f5be4b41c9db92fcad9fe')

            }
            if (search !== "") {
                paramSearch.append("name", search.toUpperCase())
                paramSearchTotal.append("name", search.toUpperCase())
        }
        const responseTotal = await fetch("http://localhost:8000/api/v1/products?"+paramSearchTotal.toString() , requestOptions);
        const dataTotal = await responseTotal.json();
            const response = await fetch("http://localhost:8000/api/v1/products?" + paramSearch.toString(), requestOptions);
            const data = await response.json();
            return dispatch({
                type: PRODUCTS_SEARCH_SUCCESS,
                total: dataTotal.result.length,
                data: data,
            })
        } catch (error) {
            return dispatch({
                type: PRODUCTS_SEARCH_ERROR,
                error: error
            })

        }
    }
}
// Ham xu ly su kien chuyen trang
export const pageChangePagination = (page) => {
    return {
        type: PRODUCTS_PAGE_CHANGE,
        page: page
    }
}
// Ham xu ly su kien click btn get info
export const getInfo = (productId) => {
    return {
        type: GET_INFO,
        payload: productId.id
    }

}
// Ham xu ly khi change slider Price
export const changePriceMin = (slider, minDistance, newValue) => {
    return {
        type: SEARCH_PRICE_MIN,
        payload: [Math.min(newValue[0], slider.value[1] - minDistance), slider.value[1]]
    }
}
export const changePriceMax = (slider, minDistance, newValue) => {
    return {
        type: SEARCH_PRICE_MAX,
        payload: [slider.value[0], Math.max(newValue[1], slider.value[0] + minDistance)]
    }
}
// Ham xu ly input search
export const changeNameSearch = (event) => {
    return {
        type: SEARCH_NAME,
        payload: event.target.value
    }
}
// Ham xu ly checkbox search
export const changeTypeSearchShirt = (event) => {
    return {
        type: SEARCH_TYPE_SHIRT,
    }
}
export const changeTypeSearchGlasses = (event) => {
    return {
        type: SEARCH_TYPE_GLASSES,
    }
}
export const changeTypeSearchHandbag = (event) => {
    return {
        type: SEARCH_TYPE_HANDBAG,
    }
}
export const changeTypeSearchWatch = (event) => {
    return {
        type: SEARCH_TYPE_WATCH,
    }
}
export const changeTypeSearchShoe = (event) => {
    return {
        type: SEARCH_TYPE_SHOE,
    }
}
// Ham xu ly su kien click add cart
export const addCart = (info,qty) => {
    info.quantity = qty?qty:1
    return {
        type: ADD_CART,
        payload: info
    }
}
// Ham xu ly su kien lay user de check out
export const fetchUser = () => {
    return async (dispatch) => {
        try {
            const userId = sessionStorage.getItem("userId")
            var requestOptionsGET = {
                method: 'GET',
                redirect: 'follow'
            };
            var response = await fetch(`http://localhost:8000/api/v1/auth/users/` + userId, requestOptionsGET)
            var data = await response.json()
            if (response.status === 200) {
                return dispatch({
                    type: GET_USER_DATA,
                    payload: data.result
                })
            }
        } catch (error) {
            return dispatch({
                type: LOG_IN_ERROR,
                error: error.message,
            })
        }
    }
}
// Ham dong notice
  export  const closeNoticeAddCart = ()=>{
        return({
            type:CLOSE_NOTICE_ADDCART,
        })
    }