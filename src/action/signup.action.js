import { SIGNUP_CHANGE_CONFIRMPASSWORD, SIGNUP_CHANGE_EMAIL, SIGNUP_CHANGE_PASSWORD, SIGNUP_CHANGE_PHONE, SIGNUP_CHANGE_USERNAME, SIGNUP_CONFIRM_PASSWORD_INVALID, SIGNUP_EMAIL_INVALID, SIGNUP_PASSWORD_INVALID, SIGNUP_PHONE_INVALID, SIGNUP_USERNAME_INVALID, SIGN_UP_BAD_REQUEST, SIGN_UP_ERROR, SIGN_UP_SUCCESS } from "../constants/constants"
// ham xu ly onchange input
export const changeUsername = (event) => {
    return {
        type: SIGNUP_CHANGE_USERNAME,
        payload: event.target.value
    }
}
// ham xu ly onchange input
export const changePassword = (event) => {
    return {
        type: SIGNUP_CHANGE_PASSWORD,
        payload: event.target.value
    }
}
// ham xu ly onchange input
export const changeConfirmPassword = (event) => {
    return {
        type: SIGNUP_CHANGE_CONFIRMPASSWORD,
        payload: event.target.value
    }
}
// ham xu ly onchange input
export const changeEmail = (event) => {
    return {
        type: SIGNUP_CHANGE_EMAIL,
        payload: event.target.value
    }
}
// ham xu ly onchange input
export const changePhone = (event) => {
    return {
        type: SIGNUP_CHANGE_PHONE,
        payload: event.target.value
    }
}
// ham xu ly su kien sign up
export const signUpUser = (stateSignUp) => {
    return async (dispatch) => {
        try {
            if (stateSignUp.username === "") {
                return dispatch({
                    type: SIGNUP_USERNAME_INVALID,
                    message: "Please fill out your first name in this field"
                })
            }
            if (isEmailValidate(stateSignUp.email) === false) {
                return dispatch({
                    type: SIGNUP_EMAIL_INVALID,
                    message: "Invalid email format. Please try again"
                })
            }
            if (isPhoneValidate(stateSignUp.phone) === false) {
                return dispatch({
                    type: SIGNUP_PHONE_INVALID,
                    message: "Invalid phone format. Please try again"
                })
            }
            if (validatePassword(stateSignUp.password) === false) {
                return dispatch({
                    type: SIGNUP_PASSWORD_INVALID,
                    message: "Password at least 4 characters, 1 number, 1 upper case letter, 1 special character"
                })
            }
            if (stateSignUp.confirmPassword !== stateSignUp.password) {
                return dispatch({
                    type: SIGNUP_CONFIRM_PASSWORD_INVALID,
                    message: "Confirm password and password do not match"
                })
            }

            const vUser = {
                username: stateSignUp.username,
                password: stateSignUp.password,
                phone: stateSignUp.phone,
                email: stateSignUp.email
            }
            var requestOptionsGET = {
                method: 'GET',
                redirect: 'follow',
            };
            var responseUsername = await fetch(`http://localhost:8000/api/v1/auth/users?` + vUser.username, requestOptionsGET)
            var checkDublicateUsername = await responseUsername.json()
            if (checkDublicateUsername.result.length > 0) {
                return dispatch({
                    type: SIGNUP_USERNAME_INVALID,
                    message: "Username existed !"
                })
            }
            var responseEmail = await fetch(`http://localhost:8000/api/v1/auth/users?` + vUser.email, requestOptionsPOST)
            var checkDublicateEmail = await responseEmail.json()
            if (checkDublicateEmail.result.length > 0) {
                return dispatch({
                    type: SIGNUP_EMAIL_INVALID,
                    message: "Email address is already in used !"
                })
            }
            var responsePhone = await fetch(`http://localhost:8000/api/v1/auth/users?` + vUser.phone, requestOptionsPOST)
            var checkDublicatePhone = await responsePhone.json()
            if (checkDublicatePhone.result.length > 0) {
                return dispatch({
                    type: SIGNUP_PHONE_INVALID,
                    message: "Phone is already in used !"
                })
            }
            var requestOptionsPOST = {
                method: 'POST',
                redirect: 'follow',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(vUser)
            };

            var response = await fetch(`http://localhost:8000/api/v1/auth/signup`, requestOptionsPOST)
            var data = await response.json()
            if (response.status === 201) {
                return dispatch({
                    type: SIGN_UP_SUCCESS,
                })
            }
        } catch (error) {
            return dispatch({
                type: SIGN_UP_ERROR,
                error: error.message,
            })
        }
    }


}
//Hàm kiểm tra Password

function validatePassword(paramPassword) {
    if (/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*(\W|_)).{5,}$/.test(paramPassword)) {
        // valid password
        return true
    }
    return false
}
//Hàm kiểm tra email
export function isEmailValidate(paramEmail) {
    var vRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return vRegex.test(paramEmail)
}
//Hàm kiểm tra SĐT
export function isPhoneValidate(paramPhone) {
    var regexPhoneNumber = /(84|0[1|3|7|9])+([0-9]{8})\b/g;
    return paramPhone.match(regexPhoneNumber) ? true : false;
}