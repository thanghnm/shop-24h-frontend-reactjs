import {
    CHECK_OUT, CLOSE_MODAL, CLOSE_NOTICE_PLUS, CREATE_ORDER_ERROR, CREATE_ORDER_PENDING,
    CREATE_ORDER_SUCCESS, DATE_PICKER_CHANGE, FETCH_COUNTRY_ERROR, FETCH_COUNTRY_PENDING, FETCH_COUNTRY_SUCCESS,
    GET_PRODUCT_FROM_STORAGE, GET_USER_DATA, INPUT_CHECKOUT_ADDRESS_CHANGE, SELECT_CHECKOUT_CITY_CHANGE, SELECT_CHECKOUT_COUNTRY_CHANGE,
    INPUT_CHECKOUT_EMAIL_CHANGE, INPUT_CHECKOUT_NAME_CHANGE, INPUT_CHECKOUT_NOTE_CHANGE, INPUT_CHECKOUT_PHONE_CHANGE, LOG_IN_ERROR,
    NOTE_SIGN_IN, ORDER_ADD_ADDRESS_INVALID, ORDER_ADD_EMAIL_INVALID, ORDER_ADD_FULLNAME_INVALID, ORDER_ADD_PHONE_INVALID,
    ORDER_ADD_SHIPDATE_INVALID, QUANTITY_PLUS, QUANTITY_SUBTRACT, REMOVE_ITEM, UPDATE_CART, VALIDATE_PRODUCT_SUCCESS, VALIDATE_PRODUCT_ERROR, PAYMENT_CREDIT, PAYMENT_BANK
} from "../constants/constants"
import { isEmailValidate, isPhoneValidate } from "./signup.action"
// Ham lay data tu localstorage de load ra cart
export const getProductFromStorage = () => {
    return async (dispatch) => {
        if (window.location.pathname === "/admin/orders") {
            var vItem = JSON.parse(localStorage.getItem("adminCartItems"))
        }
        else {
            var vItem = JSON.parse(localStorage.getItem("cartItems"))

        }
        var productObject
        if (vItem) {
            productObject = vItem.map((element, index) => {
                return element
            })
            var vKeyItem = productObject.length
            var priceArray = vItem.map((element, index) => {
                const itemstorage = element
                return itemstorage.promotionPrice
            })
            // var quantityArray = vItem.map((element, index) => {
            //     const itemstorage = element
            //     return itemstorage.quantity
            // })
        }
        return dispatch({
            type: GET_PRODUCT_FROM_STORAGE,
            payload: productObject,
            // qty: quantityArray,
            price: priceArray,
            keyItem: vKeyItem
        })
    }
}
// Ham` xu ly khi click button +
export const plusItem = (event) => {
    return {
        type: QUANTITY_PLUS,
        payload: event.target.dataset.qty
    }
}
// Ham xu ly dong notice
export const closeNoticePlusItem = (event) => {
    return {
        type: CLOSE_NOTICE_PLUS,
        payload: event.target.dataset.qty

    }
}
// Ham` xu ly khi click button -
export const subtractItem = (event) => {
    return {
        type: QUANTITY_SUBTRACT,
        payload: event.target.dataset.qty
    }
}
export const updateCart = (event) => {
    return {
        type: UPDATE_CART,
    }
}
// Ham xu ly khi click btn check out de mo modal check out
export const checkOut = (event) => {
    return {
        type: CHECK_OUT,
        email: event.target.dataset.email,
        phone: event.target.dataset.phone
    }
}
// Ham xu ly dong tat ca cac modal
export const closeModalHandler = () => {
    return {
        type: CLOSE_MODAL
    }
}
// Ham xu ly nhac nho phai dang nhap truoc khi checkout
export const noticeSignIn = (event) => {
    return {
        type: NOTE_SIGN_IN,
    }
}
// Ham xu ly su kien click btn remove item de xoa item trong cart
export const removeItem = (event) => {
    var vDataKey = event.target.dataset.remove
    return {
        type: REMOVE_ITEM,
        payload: vDataKey
    }
}
// Ham xu ly input Name
export const inputNameCheckOutChange = (event) => {
    return {
        type: INPUT_CHECKOUT_NAME_CHANGE,
        payload: event.target.value
    }
}
// Ham xu ly input Email
export const inputEmailCheckOutChange = (event) => {
    return {
        type: INPUT_CHECKOUT_EMAIL_CHANGE,
        payload: event.target.value
    }
}
// Ham xu ly input Phone
export const inputPhoneCheckOutChange = (event) => {
    return {
        type: INPUT_CHECKOUT_PHONE_CHANGE,
        payload: event.target.value
    }
}
// Ham xu ly input Address
export const inputAddressCheckOutChange = (event) => {
    return {
        type: INPUT_CHECKOUT_ADDRESS_CHANGE,
        payload: event.target.value
    }
}
// Ham xu ly select City
export const selectCityCheckOutChange = (event) => {
    return {
        type: SELECT_CHECKOUT_CITY_CHANGE,
        payload: event.target.value
    }
}
// Ham xu ly select Country
export const selectCountryCheckOutChange = (event) => {
    return async (dispatch) => {
        try {
            const countryName = event.target.value
            var data = { country: countryName }
            var requestOptionsPost = {
                method: 'POST',
                redirect: 'follow',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(data)
            };
            var responseCity = await fetch(`https://countriesnow.space/api/v0.1/countries/cities`, requestOptionsPost)
            const cityData = await responseCity.json()
            return dispatch({
                type: SELECT_CHECKOUT_COUNTRY_CHANGE,
                payload: countryName,
                city: cityData.data
            })
        } catch (error) {
            return dispatch({
                type: CREATE_ORDER_ERROR,
                error: error.message,
            })
        }

    }
}
// Ham xu ly input note
export const inputNoteCheckOutChange = (event) => {
    return {
        type: INPUT_CHECKOUT_NOTE_CHANGE,
        payload: event.target.value
    }
}
// Ham xu ly date time picker
export const datePicked = (event) => {
    return {
        type: DATE_PICKER_CHANGE,
        payload: event
    }
}
// Ham xu ly khi click btn create order
export const createOrder = (stateCheckOut, productArr,validateData) => {
    return async (dispatch) => {
        try {
            dispatch({
                type: CREATE_ORDER_PENDING
            })
            if (stateCheckOut.fullName === "") {
                return dispatch({
                    type: ORDER_ADD_FULLNAME_INVALID,
                    message: "Fullname is require"
                })
            }
            if (stateCheckOut.phone === "") {
                return dispatch({
                    type: ORDER_ADD_PHONE_INVALID,
                    message: "Phone is require"
                })
            }
            if (stateCheckOut.email === "") {
                return dispatch({
                    type: ORDER_ADD_EMAIL_INVALID,
                    message: "Email is require"
                })
            }
            if (stateCheckOut.address === "") {
                return dispatch({
                    type: ORDER_ADD_ADDRESS_INVALID,
                    message: "Address is require"
                })
            }

            if (!stateCheckOut.shippedDate) {
                return dispatch({
                    type: ORDER_ADD_SHIPDATE_INVALID,
                    message: "Shipped date is require"
                })
            }
            const vCustomer = {
                fullName: stateCheckOut.fullName,
                phone: stateCheckOut.phone,
                email: stateCheckOut.email,
                address: stateCheckOut.address,
                city: stateCheckOut.city,
                country: stateCheckOut.country
            }
            if (isPhoneValidate(vCustomer.phone) === false) {
                return dispatch({
                    type: ORDER_ADD_PHONE_INVALID,
                    message: "Phone is invalid"
                })
            }
            if (isEmailValidate(vCustomer.email) === false) {
                return dispatch({
                    type: ORDER_ADD_EMAIL_INVALID,
                    message: "Email is invalid"
                })
            }
            const vOrder = {
                shippedDate: stateCheckOut.shippedDate,
                note: stateCheckOut.note,
                cost: stateCheckOut.cost,
            }
            var query = new URLSearchParams({
                phone: vCustomer.phone
            })
            var requestOptionsPostCustomer = {
                method: 'POST',
                redirect: 'follow',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(vCustomer)
            };
            var requestOptionsPostOrder = {
                method: 'POST',
                redirect: 'follow',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(vOrder)
            };
            var requestOptionsGet = {
                method: 'GET',
                redirect: 'follow',
            };
            // Kiem tra nguoi dung co ton tai hay chua
            var findCustomer = await fetch(`http://localhost:8000/api/v1/customers?` + query.toString(), requestOptionsGet)
            var dataCustomer = await findCustomer.json()
            if (dataCustomer.result.length === 0) {
                // Neu chua thi create customer
                var createCustomer = await fetch(`http://localhost:8000/api/v1/customers`, requestOptionsPostCustomer)
                const dataCreateCustomer = await createCustomer.json()
                var createOrder = await fetch(`http://localhost:8000/api/v1/orders/` + dataCreateCustomer.result._id, requestOptionsPostOrder)
                const dataCreateOrder = await createOrder.json()
                productArr.map(async (element, index) => {
                    var vOrderDetail = {
                        product: element._id,
                        quantity: element.quantity
                    }
                    var requestOptionsPostOrderDetail = {
                        method: 'POST',
                        redirect: 'follow',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify(vOrderDetail)
                    }
                    var newAmount = validateData[index].amount-element.quantity
                    var vProduct = {
                        amount: newAmount,
                    }
                    var requestOptionsPutProduct = {
                        method: 'PUT',
                        redirect: 'follow',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify(vProduct)
                    }
                    var createOrderDetails = await fetch(`http://localhost:8000/api/v1/orderdetails/` + dataCreateOrder.result._id, requestOptionsPostOrderDetail)
                    const dataCreateOrderDetails = await createOrderDetails.json()
                    var updateProduct = await fetch(`http://localhost:8000/api/v1/products/`+element._id,requestOptionsPutProduct)
                })
                return dispatch({
                    type: CREATE_ORDER_SUCCESS,
                })
            }
            else {
                // Neu customer da ton tai thi update info customer va create order
                const customerId = dataCustomer.result[0]._id
                var requestOptionsPutCustomer = {
                    method: 'PUT',
                    redirect: 'follow',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(vCustomer)
                }
                var responsePutCustomer = await fetch(`http://localhost:8000/api/v1/customers/` + customerId, requestOptionsPutCustomer)
                const dataPutCustomer = await responsePutCustomer.json()
                var createOrder = await fetch(`http://localhost:8000/api/v1/orders/` + customerId, requestOptionsPostOrder)
                const dataCreateOrder = await createOrder.json()
                productArr.map(async (element, index) => {
                    var vOrderDetail = {
                        product: element._id,
                        quantity: element.quantity
                    }
                    var requestOptionsPostOrderDetail = {
                        method: 'POST',
                        redirect: 'follow',
                        headers: {
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify(vOrderDetail)
                    }
                    var createOrderDetails = await fetch(`http://localhost:8000/api/v1/orderdetails/` + dataCreateOrder.result._id, requestOptionsPostOrderDetail)
                    const dataCreateOrderDetails = await createOrderDetails.json()
                })
                return dispatch({
                    type: CREATE_ORDER_SUCCESS,
                })
            }

        } catch (error) {
            return dispatch({
                type: CREATE_ORDER_ERROR,
                error: error.message,
            })
        }
    }

}

// Ham xu ly khi click btn create order
export const fetchCountry = () => {
    return async (dispatch) => {
        try {
            dispatch({
                type: FETCH_COUNTRY_PENDING
            })
            var requestOptionsGet = {
                method: 'GET',
                redirect: 'follow',
            };
            const responseCountry = await fetch("https://restcountries.com/v3.1/subregion/South-eastern Asia", requestOptionsGet)
            const countryData = await responseCountry.json()
            return dispatch({
                type: FETCH_COUNTRY_SUCCESS,
                payload: countryData
            })
        } catch (error) {
            return dispatch({
                type: FETCH_COUNTRY_ERROR,
                error: error.message,
            })
        }
    }
}
// Ham kiem tra san pham con hang hay het
export const validateProduct = () => {
    return async (dispatch) => {
        try {
            var requestOptionsGet = {
                method: 'GET',
                redirect: 'follow',
            };
            // lay id product tu local storage
            if(window.location.pathname==="/admin/orders"){
                var idProductArr = JSON.parse(localStorage.getItem("adminCartItems")).map((element, index) => {
                    return element._id
                })
            }
            else{
            var idProductArr = JSON.parse(localStorage.getItem("cartItems")).map((element, index) => {
                return element._id
            })}
            const validateData = []
            if (idProductArr.length > 0) {
                for (var bI = 0; bI < idProductArr.length; bI++) {
                    const response = await fetch("http://localhost:8000/api/v1/products/" + idProductArr[bI], requestOptionsGet)
                    const data = await response.json()
                    validateData.push(data.result)
                }
                if (validateData.length === idProductArr.length) {
                    return dispatch({
                        type: VALIDATE_PRODUCT_SUCCESS,
                        payload: validateData
                    })
                }
            }
        } catch (error) {
            return dispatch({
                type: VALIDATE_PRODUCT_ERROR,
                error: error.message,
            })
        }
    }
}
 export const paymentCredit = ()=>{
    return{
        type:PAYMENT_CREDIT
    }
}
export const paymentBank = ()=>{
    return{
        type:PAYMENT_BANK
    }
}