import { jwtDecode } from "jwt-decode"
import { INPUT_PASSWORD_CHANGE, INPUT_USERNAME_CHANGE, LOG_IN, LOG_IN_ERROR, LOG_IN_ERROR_PASSWORD, LOG_IN_PENDING, LOG_IN_SUCCESS, LOG_IN_USERNAME_BLANK, LOG_IN_USER_NOT_FOUND } from "../constants/constants"
// Ham xu ly input
export const inputUsernameChangeHandler = (event) => {
    return {
        type: INPUT_USERNAME_CHANGE,
        payload: event.target.value
    }
}
export const inputPasswordChangeHandler = (event) => {
    return {
        type: INPUT_PASSWORD_CHANGE,
        payload: event.target.value
    }
}
// Ham xu ly khi click btn Log in
export const logInAction = (stateLogIn) => {
    return async (dispatch) => {
        try {
            dispatch({
                type: LOG_IN_PENDING
            })
            const vUser = {
                username: stateLogIn.username,
                password: stateLogIn.password
            }
            if (vUser.username === "") {
                return dispatch({
                    type: LOG_IN_USERNAME_BLANK,
                })
            }
            var requestOptionsGET = {
                method: 'GET',
                redirect: 'follow',
            };
            var requestOptionsPOST = {
                method: 'POST',
                redirect: 'follow',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(vUser)
            };
            var response = await fetch(`http://localhost:8000/api/v1/auth/login`, requestOptionsPOST)
            var data = await response.json()
            const token = data.accessToken
            const user = jwtDecode(token)
            var responseUser = await fetch(`http://localhost:8000/api/v1/auth/users/` + user.id, requestOptionsGET)
            var dataUser = await responseUser.json()
            var userRole = "user"
            const role =  dataUser.result.role
            if (response.status === 404) {
                return dispatch({
                    type: LOG_IN_USER_NOT_FOUND,
                })
            }
            if (response.status === 400) {
                return dispatch({
                    type: LOG_IN_ERROR_PASSWORD,
                })
            }
            else if (response.status === 200) {
                role.map((element, index) => {
                    if (element.name === "admin") {
                        return userRole = "admin"
                    }
                })
                return dispatch({
                    type: LOG_IN_SUCCESS,
                    token: token,
                    refreshToken: data.refreshToken,
                    userId: user.id,
                    userRole:userRole

                })
            }
        } catch (error) {
            return dispatch({
                type: LOG_IN_ERROR,
                error: error.message,
            })
        }
    }

}
