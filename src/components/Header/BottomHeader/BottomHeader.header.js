import { Button } from "@mui/material";
import heart from "../../../assets/images/icon/heart.png"
import search from "../../../assets/images/icon/search.png"
import cart from "../../../assets/images/payment.png"
import AddShoppingCartIcon from '@mui/icons-material/AddShoppingCart';
import { useSelector } from "react-redux";
const BottomHeader = () => {
    const { key } = useSelector((reduxData) => reduxData.cartReducers)
    const { userData } = useSelector((reduxData) => reduxData.productReducers)
    const onBtnLogOutClick =()=>{
        sessionStorage.clear()
        window.location.reload()
    }
    return (
        <>
            <div className="container">
                <div className="row">
                    <div className="col-lg-3 col-md-3 ">
                        <div className="header__logo">
                            <a href="/">
                                <p className="name-shop">LUXURY <span><br /> FASHION</span>
                                </p></a>
                        </div>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-6">
                        <nav className="header__menu mobile-menu">
                            <ul>
                                <li ><a href="/">Home</a></li>
                                <li><a href="/products">Shop</a></li>
                                <li><a href="#">Admin</a>
                                    <ul className="dropdown">
                                        <li><a href="/admin/products">Product</a></li>
                                        <li><a href="/admin/customers">Customer</a></li>
                                        <li><a href="/admin/orders">Order</a></li>
                                    </ul>
                                </li>
                                <li><a href="./blog.html">Blog</a></li>
                                <li><a href="./contact.html">Contacts</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div className="col-lg-3 col-md-3">
                        <div className="header__nav__option">
                            <a href="#" className="search-switch"><img src={search} alt="" /></a>
                            {userData ? <div className="flex flex-col justify-center items-center header__top__hover">
                                <img src={heart} alt="" /><p className="cart-items">{userData.username}</p>
                                <ul style={{width:100}} >
                                            <li>User</li>
                                            <li>Setting</li>
                                            <li onClick={onBtnLogOutClick}>Log out</li>
                                        </ul>
                                </div> : <Button href="/login" className="text-white bg-dark">Sign In</Button>}
                            <a className="cart-icon" href="/cart"><img src={cart} alt="cart" /><p className="cart-items">{key ? key : 0} item</p></a>
                        </div>
                    </div>
                </div>
                <hr className="header-hr" />
            </div>
        </>
    )
}
export default BottomHeader