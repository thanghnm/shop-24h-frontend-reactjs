import { Component } from "react";

class TopHeader extends Component {
    render() {
        return (
            <>
                <div  className="header__top w-full ml-0 mr-0">
                    <div className="container">
                        <div className="row mr-0">
                            <div className="col-lg-6 col-md-7">
                                <div className="header__top__left">
                                    <a style={{color:"white"}} href="/">DEVCAMP</a>
                                </div>
                            </div>
                            <div className="col-lg-6 col-md-5">
                                <div className="header__top__right">
                                    {/* <div className="header__top__links">
                                        <a href="/login">Sign in</a>
                                        <a href="/signup">Sign up</a>
                                    </div> */}
                                    <div className="header__top__hover">
                                        <span>ENG <i className="fa-solid fa-arrow-down"></i></span>
                                        <ul>
                                            <li>ENG</li>
                                            <li>VIE</li>
                                            <li>FRA</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}
export default TopHeader