import { Component } from "react";
import TopHeader from "./TopHeader/TopHeader.header";
import BottomHeader from "./BottomHeader/BottomHeader.header";

class Header extends Component{
    render(){
        return(
            <>
            <TopHeader/>
            <BottomHeader></BottomHeader>
            </>
        )
    }
}
export default Header