import React from "react";

// Chakra imports
import { Flex, useColorModeValue } from "@chakra-ui/react";

// Custom components
import { HorizonLogo } from "../../icons/Icons";
import { HSeparator } from "../../separator/Separator";

export function SidebarBrand() {
  //   Chakra color mode
  let logoColor = useColorModeValue("navy.700", "white");

  return (
    <Flex align='center' direction='column'>
      <h3 style={{fontStyle:"italic"}} className="text-center mt-3 mb-3">LUXURY FASHION</h3>
      <HSeparator mb='20px' />
    </Flex>
  );
}

export default SidebarBrand;
