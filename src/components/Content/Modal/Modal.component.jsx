import { Box, Button, FormControl, Grid, InputLabel, MenuItem, Modal, Select, TextField, Typography } from "@mui/material"
import { useDispatch, useSelector } from "react-redux";
import {
    closeModalHandler, createOrder, inputAddressCheckOutChange, selectCityCheckOutChange, selectCountryCheckOutChange, inputEmailCheckOutChange, inputNameCheckOutChange,
    inputNoteCheckOutChange, inputPhoneCheckOutChange
} from "../../../action/cart.action";
import DatePickerComponent from "./DatePicker.component";
import fail from "../../../assets/images/modal/fail.png";
import tick from "../../../assets/images/modal/tick.jpg";
import { changeAddressAddCustomer, changeAddressEditCustomer, changeAmountAddProduct, changeAmountEditProduct, changeBuyPriceAddProduct, changeBuyPriceEditProduct, changeCityAddCustomer, changeCityEditCustomer, changeCountryAddCustomer, changeCountryEditCustomer, changeDescriptionAddProduct, changeDescriptionEditProduct, changeEmailAddCustomer, changeEmailEditCustomer, changeFullNameAddCustomer, changeFullNameEditCustomer, changeImageUrlAddProduct, changeImageUrlEditProduct, changeNameAddProduct, changeNameEditProduct, changePhoneAddCustomer, changePhoneEditCustomer, changePromotionPriceAddProduct, changePromotionPriceEditProduct, changeTypeAddProduct, changeTypeEditProduct, createCustomer, createProduct, deleteCustomer, deleteOrder, deleteProduct, inputNoteUpdateOrderChange, updateCustomer, updateOrder, updateProduct } from "../../../action/table.action";

const style = {
    position: 'absolute',
    top: '30%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: "30%",
    bgcolor: 'background.paper',
    border: '2px solid #000',
    borderRadius: 5,
    boxShadow: 24,
    p: 4,
};
const styleCheckOut = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: "60%",
    bgcolor: 'background.paper',
    border: '2px solid #000',
    borderRadius: 5,
    boxShadow: 24,
    p: 4,
};
export const ModalSignIn = () => {
    const dispatch = useDispatch()
    const { openModalSignIn } = useSelector((reduxData) => reduxData.cartReducers)

    const handleClose = () => {
        dispatch(closeModalHandler())
    }
    const onBtnSignInClick = () => {
        window.location.href = "/login"
    }
    return (<>
        <Modal
            open={openModalSignIn}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={style} textAlign={"center"}>
                <Typography fontWeight={700} fontSize={30} marginBottom={2} id="modal-modal-description" sx={{ mt: 2 }}>
                    SIGN IN
                </Typography>
                <Typography fontSize={16} id="modal-modal-description" sx={{ mt: 2 }}>
                    You must sign in before checkout !!!
                </Typography>
                <Grid container marginTop={1} justifyContent={"center"}>
                    <Grid sx={{ textAlign: "center", marginTop: 2 }} item xs={12}>
                        <Button onClick={onBtnSignInClick} variant="contained" color="success">Sign In</Button>
                    </Grid>
                </Grid>
            </Box>
        </Modal >
    </>)
}
export const ModalCheckOut = () => {
    const dispatch = useDispatch()
    const { openModalCheckOut, checkOut, productObject, addOrderFullnameAlertDisplay, addOrderFullnameAlertMessage, addOrderEmailAlertDisplay, addOrderEmailAlertMessage,
        addOrderPhoneAlertDisplay, addOrderPhoneAlertMessage, addOrderAddressAlertDisplay, addOrderAddressAlertMessage,
        addOrderShippedDateAlertDisplay, addOrderShippedDateAlertMessage, countryAsia,city,validateData } = useSelector((reduxData) => reduxData.cartReducers)
    const { userData } = useSelector((reduxData) => reduxData.productReducers)
    const handleClose = () => {
        dispatch(closeModalHandler())
    }
    const onTextFieldFullNameCheckOutChange = (event) => {
        dispatch(inputNameCheckOutChange(event))
    }
    const onTextFieldEmailCheckOutChange = (event) => {
        dispatch(inputEmailCheckOutChange(event))
    }
    const onTextFieldPhoneCheckOutChange = (event) => {
        dispatch(inputPhoneCheckOutChange(event))
    }
    const onTextFieldAddressCheckOutChange = (event) => {
        dispatch(inputAddressCheckOutChange(event))
    }
    const onTextFieldNoteCheckOutChange = (event) => {
        dispatch(inputNoteCheckOutChange(event))
    }
    const onSelectCountryCheckOutChange = (event) => {
        dispatch(selectCountryCheckOutChange(event))
    }
    const onSelectCityCheckOutChange = (event) => {
        dispatch(selectCityCheckOutChange(event))
    }
    const onBtnCheckOutClick = (event) => {
        dispatch(createOrder(checkOut, productObject,validateData))
    }
    return (<>
        <Modal
            open={openModalCheckOut}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={styleCheckOut} className="div-modal-checkout" textAlign={"center"}>
                <Typography className="text-white" fontWeight={700} fontSize={30} marginBottom={2} id="modal-modal-description" sx={{ mt: 2 }}>
                    CHECK OUT
                </Typography>
                <Grid container marginTop={2} justifyContent={"center"}>
                    <Grid item xs={6}>
                        <TextField
                            sx={{ width: "90%" }}
                            required
                            label="Fullname"
                            value={checkOut.name}
                            onChange={onTextFieldFullNameCheckOutChange}
                        />
                        <p className="invalid-textfield" style={{ display: addOrderFullnameAlertDisplay }}>{addOrderFullnameAlertMessage}</p>
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            sx={{ width: "90%" }}
                            required
                            label="Email"
                            value={checkOut.email}
                            onChange={onTextFieldEmailCheckOutChange}
                        />
                        <p className="invalid-textfield" style={{ display: addOrderEmailAlertDisplay }}>{addOrderEmailAlertMessage}</p>

                    </Grid>
                </Grid>
                <Grid container marginTop={2} justifyContent={"center"}>
                    <Grid item xs={6}>
                        <TextField
                            sx={{ width: "90%" }}
                            required
                            label="Phone"
                            value={checkOut.phone}
                            onChange={onTextFieldPhoneCheckOutChange}
                        />
                        <p className="invalid-textfield" style={{ display: addOrderPhoneAlertDisplay }}>{addOrderPhoneAlertMessage}</p>

                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            sx={{ width: "90%" }}
                            required
                            label="Address"
                            value={checkOut.address}
                            onChange={onTextFieldAddressCheckOutChange}
                        />
                        <p className="invalid-textfield" style={{ display: addOrderAddressAlertDisplay }}>{addOrderAddressAlertMessage}</p>

                    </Grid>
                </Grid>
                <Grid container marginTop={2} justifyContent={"center"}>
                    <Grid item xs={6}>
                        <FormControl fullWidth >
                            <InputLabel style={{ marginLeft: "10px" }} id="country-label">Country</InputLabel>
                            <Select onChange={onSelectCountryCheckOutChange} value={checkOut.country} labelId="country-label" label="Country" style={{ width: "90%" }} >
                                {countryAsia ? countryAsia.map((element,index) => {
                                    return (
                                        <MenuItem key={index} value={element.name.common}>{element.name.common}</MenuItem>
                                    )
                                }) : ""}
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid item xs={6}>
                    <FormControl fullWidth >
                            <InputLabel style={{ marginLeft: "10px" }} id="city-label">City</InputLabel>
                            <Select disabled={!city?true:false} onChange={onSelectCityCheckOutChange} value={checkOut.city} labelId="city-label" label="City" style={{ width: "90%" }} >
                            
                                {city ? city.map((element,index) => {
                                    return (
                                        <MenuItem key={index} value={element}>{element}</MenuItem>
                                    )
                                }) : <MenuItem  value="" >City</MenuItem>}
                            </Select>
                        </FormControl>
                    </Grid>
                </Grid>
                <Grid container marginTop={2} justifyContent={"center"}>
                    <Grid item xs={6}>
                        <DatePickerComponent />
                        <p className="invalid-textfield" style={{ display: addOrderShippedDateAlertDisplay }}>{addOrderShippedDateAlertMessage}</p>
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            sx={{ width: "90%" }}
                            required
                            label="Note"
                            value={checkOut.note}
                            onChange={onTextFieldNoteCheckOutChange}
                        />
                    </Grid>
                </Grid>
                <Grid container marginTop={2} >
                    <Grid sx={{ textAlign: "end", marginTop: 2 }} item xs={12}>
                        <Button onClick={handleClose} style={{ marginRight: 10 }} variant="contained" color="error">Close</Button>
                        <Button onClick={onBtnCheckOutClick} variant="contained" color="success">Check Out</Button>
                    </Grid>
                </Grid>
            </Box>
        </Modal >
    </>)
}
export const ModalCreateOrderSuccess = () => {
    const dispatch = useDispatch()
    const { openModalCreateOrderSuccess } = useSelector((reduxData) => reduxData.cartReducers)

    const handleClose = () => {
        dispatch(closeModalHandler())
    }
    return (<>
        <Modal
            open={openModalCreateOrderSuccess}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={style} textAlign={"center"}>
                <Typography fontWeight={700} fontSize={30} marginBottom={2} id="modal-modal-description" sx={{ mt: 2 }}>
                    SUCCESS
                </Typography>
                <img style={{ width: 100 }} src={tick} alt="tick" />
                <Typography fontSize={16} id="modal-modal-description" sx={{ mt: 2 }}>
                    Create order success fully !!!
                </Typography>
                <Grid container marginTop={1} justifyContent={"center"}>
                    <Grid sx={{ textAlign: "center", marginTop: 2 }} item xs={12}>
                        <Button onClick={handleClose} variant="contained" color="success">Close</Button>
                    </Grid>
                </Grid>
            </Box>
        </Modal >
    </>)
}
export const ModalAddCustomer = () => {
    const dispatch = useDispatch()
    const { openModalAddCustomer, addCustomer, addCustomerFullnameAlertDisplay, addCustomerFullnameAlertMessage,
        addCustomerPhoneAlertDisplay, addCustomerPhoneAlertMessage, addCustomerEmailAlertDisplay, addCustomerEmailAlertMessage
    } = useSelector((reduxData) => reduxData.tableReducers)

    const handleClose = () => {
        dispatch(closeModalHandler())
    }
    const onTextFieldFullNameChange = (event) => {
        dispatch(changeFullNameAddCustomer(event))
    }
    const onTextFieldEmailChange = (event) => {
        dispatch(changeEmailAddCustomer(event))
    }
    const onTextFieldPhoneChange = (event) => {
        dispatch(changePhoneAddCustomer(event))
    }
    const onTextFieldAddressChange = (event) => {
        dispatch(changeAddressAddCustomer(event))
    }
    const onTextFieldCityChange = (event) => {
        dispatch(changeCityAddCustomer(event))
    }
    const onTextFieldCountryChange = (event) => {
        dispatch(changeCountryAddCustomer(event))
    }
    const onBtnConfirmCreateCustomer = () => {
        dispatch(createCustomer(addCustomer))
    }
    return (<>
        <Modal
            open={openModalAddCustomer}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={styleCheckOut} textAlign={"center"}>
                <Typography fontWeight={700} fontSize={30} marginBottom={4} id="modal-modal-description" sx={{ mt: 2 }}>
                    Add Customer
                </Typography>
                <Grid container marginTop={2} justifyContent={"center"}>
                    <Grid item xs={6}>
                        <TextField
                            sx={{ width: "90%" }}
                            required
                            id="outlined-required"
                            label="Fullname"
                            value={addCustomer.fullName}
                            onChange={onTextFieldFullNameChange}
                        />
                        <p className="invalid-textfield" style={{ display: addCustomerFullnameAlertDisplay }}>{addCustomerFullnameAlertMessage}</p>
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            sx={{ width: "90%" }}
                            required
                            id="outlined-required"
                            label="Email"
                            value={addCustomer.email}
                            onChange={onTextFieldEmailChange}
                        />
                        <p className="invalid-textfield" style={{ display: addCustomerEmailAlertDisplay }}>{addCustomerEmailAlertMessage}</p>

                    </Grid>
                </Grid>
                <Grid container marginTop={2} justifyContent={"center"}>
                    <Grid item xs={6}>
                        <TextField
                            sx={{ width: "90%" }}
                            required
                            id="outlined-required"
                            label="Phone"
                            value={addCustomer.phone}
                            onChange={onTextFieldPhoneChange}
                        />
                        <p className="invalid-textfield" style={{ display: addCustomerPhoneAlertDisplay }}>{addCustomerPhoneAlertMessage}</p>

                    </Grid>

                    <Grid item xs={6}>
                        <TextField
                            sx={{ width: "90%" }}
                            required
                            id="outlined-required"
                            label="Address"
                            value={addCustomer.address}
                            onChange={onTextFieldAddressChange}
                        />
                    </Grid>
                </Grid>
                <Grid container marginTop={2} justifyContent={"center"}>
                    <Grid item xs={6}>
                        <TextField
                            sx={{ width: "90%" }}
                            required
                            id="outlined-required"
                            label="City"
                            value={addCustomer.city}
                            onChange={onTextFieldCityChange}
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            sx={{ width: "90%" }}
                            required
                            id="outlined-required"
                            label="Country"
                            value={addCustomer.country}
                            onChange={onTextFieldCountryChange}
                        />
                    </Grid>

                </Grid>
                <Grid container marginTop={2} >
                    <Grid sx={{ textAlign: "end", marginTop: 2 }} item xs={12}>
                        <Button onClick={handleClose} style={{ marginRight: 10 }} variant="contained" color="error">Close</Button>
                        <Button onClick={onBtnConfirmCreateCustomer} variant="contained" color="success">Create Customer</Button>
                    </Grid>
                </Grid>
            </Box>
        </Modal >
    </>)
}
export const ModalSuccess = (props) => {
    const dispatch = useDispatch()

    const handleClose = () => {
        dispatch(closeModalHandler())
    }
    return (<>
        <Modal
            open={props.openModal}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={style} textAlign={"center"}>
                <Typography fontWeight={700} fontSize={30} marginBottom={2} id="modal-modal-description" sx={{ mt: 2 }}>
                    SUCCESS
                </Typography>
                <img style={{ width: 100 }} src={tick} alt="tick" />
                <Typography fontSize={16} id="modal-modal-description" sx={{ mt: 2 }}>
                    {props.content}
                </Typography>
                <Grid container marginTop={1} justifyContent={"center"}>
                    <Grid sx={{ textAlign: "center", marginTop: 2 }} item xs={12}>
                        <Button onClick={handleClose} variant="contained" color="success">Close</Button>
                    </Grid>
                </Grid>
            </Box>
        </Modal >
    </>)
}
export const ModalFail = (props) => {
    const dispatch = useDispatch()

    const handleClose = () => {
        dispatch(closeModalHandler())
    }
    return (<>
        <Modal
            open={props.openModal}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={style} textAlign={"center"}>
                <Typography fontWeight={700} fontSize={30} marginBottom={2} id="modal-modal-description" sx={{ mt: 2 }}>
                    {props.title?props.title:"FAIL"}
                </Typography>
                <img style={{ width: 100 }} src={fail} alt="fail" />
                <Typography fontSize={16} id="modal-modal-description" sx={{ mt: 2 }}>
                    {props.content}
                </Typography>
                <Grid container marginTop={1} justifyContent={"center"}>
                    <Grid sx={{ textAlign: "center", marginTop: 2 }} item xs={12}>
                        <Button onClick={handleClose} variant="contained" color="success">Close</Button>
                    </Grid>
                </Grid>
            </Box>
        </Modal >
    </>)
}
export const ModalEditCustomer = () => {
    const dispatch = useDispatch()
    const { openModalEditCustomer, editCustomer, customerId } = useSelector((reduxData) => reduxData.tableReducers)

    const handleClose = () => {
        dispatch(closeModalHandler())
    }
    const onTextFieldFullNameChange = (event) => {
        dispatch(changeFullNameEditCustomer(event))
    }
    const onTextFieldEmailChange = (event) => {
        dispatch(changeEmailEditCustomer(event))
    }
    const onTextFieldPhoneChange = (event) => {
        dispatch(changePhoneEditCustomer(event))
    }
    const onTextFieldAddressChange = (event) => {
        dispatch(changeAddressEditCustomer(event))
    }
    const onTextFieldCityChange = (event) => {
        dispatch(changeCityEditCustomer(event))
    }
    const onTextFieldCountryChange = (event) => {
        dispatch(changeCountryEditCustomer(event))
    }
    const onBtnConfirmEditCustomer = () => {
        dispatch(updateCustomer(editCustomer, customerId))
    }
    return (<>
        <Modal
            open={openModalEditCustomer}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={styleCheckOut} textAlign={"center"}>
                <Typography fontWeight={700} fontSize={30} marginBottom={4} id="modal-modal-description" sx={{ mt: 2 }}>
                    Edit Customer
                </Typography>
                <Grid container marginTop={2} justifyContent={"center"}>
                    <Grid item xs={6}>
                        <TextField
                            sx={{ width: "90%" }}
                            required
                            id="outlined-required"
                            label="Fullname"
                            value={editCustomer.fullName}
                            onChange={onTextFieldFullNameChange}
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            sx={{ width: "90%" }}
                            required
                            id="outlined-required"
                            label="Email"
                            value={editCustomer.email}
                            onChange={onTextFieldEmailChange}
                        />
                    </Grid>
                </Grid>
                <Grid container marginTop={2} justifyContent={"center"}>
                    <Grid item xs={6}>
                        <TextField
                            sx={{ width: "90%" }}
                            required
                            label="Phone"
                            value={editCustomer.phone}
                            onChange={onTextFieldPhoneChange}
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            sx={{ width: "90%" }}
                            required
                            label="Address"
                            value={editCustomer.address}
                            onChange={onTextFieldAddressChange}
                        />
                    </Grid>
                </Grid>
                <Grid container marginTop={2} justifyContent={"center"}>
                    <Grid item xs={6}>
                        <TextField
                            sx={{ width: "90%" }}
                            required
                            id="outlined-required"
                            label="City"
                            value={editCustomer.city}
                            onChange={onTextFieldCityChange}
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            sx={{ width: "90%" }}
                            required
                            id="outlined-required"
                            label="Country"
                            value={editCustomer.country}
                            onChange={onTextFieldCountryChange}
                        />
                    </Grid>

                </Grid>
                <Grid container marginTop={2} >
                    <Grid sx={{ textAlign: "end", marginTop: 2 }} item xs={12}>
                        <Button onClick={handleClose} style={{ marginRight: 10 }} variant="contained" color="error">Close</Button>
                        <Button onClick={onBtnConfirmEditCustomer} variant="contained" color="success">Update Customer</Button>
                    </Grid>
                </Grid>
            </Box>
        </Modal >
    </>)
}
export const ModalDeleteCustomer = (props) => {
    const dispatch = useDispatch()
    const { openModalDeleteCustomer, customerId } = useSelector((reduxData) => reduxData.tableReducers)

    const handleClose = () => {
        dispatch(closeModalHandler())
    }
    const onBtnConfirmDeleteCustomerClick = () => {
        dispatch(deleteCustomer(customerId))
    }
    return (<>
        <Modal
            open={openModalDeleteCustomer}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={style} textAlign={"center"}>
                <Typography fontWeight={700} fontSize={24} marginBottom={2} id="modal-modal-description" sx={{ mt: 2 }}>
                    DELETE CUSTOMER
                </Typography>
                <Typography fontSize={16} id="modal-modal-description" sx={{ mt: 2 }}>
                    Do you want to delete this customer!!!
                </Typography>
                <Grid container marginTop={1} justifyContent={"center"}>
                    <Grid sx={{ textAlign: "end", marginTop: 2 }} item xs={12}>
                        <Button onClick={handleClose} variant="contained" color="success">Close</Button>
                        <Button style={{ marginLeft: 10, background: "red" }} onClick={onBtnConfirmDeleteCustomerClick} variant="contained" >Delete</Button>
                    </Grid>
                </Grid>
            </Box>
        </Modal >
    </>)
}
export const ModalAddProduct = () => {
    const dispatch = useDispatch()
    const { openModalAddProduct, types, addProduct, addProductNameAlertDisplay, addProductNameAlertMessage,
        addProductBuyPriceAlertDisplay, addProductBuyPriceAlertMessage, addProductImageUrlAlertDisplay, addProductImageUrlAlertMessage,
        addProductTypeAlertMessage, addProductTypeAlertDisplay, addProductPromotionPriceAlertDisplay, addProductPromotionPriceAlertMessage
    } = useSelector((reduxData) => reduxData.tableReducers)

    const handleClose = () => {
        dispatch(closeModalHandler())
    }
    const onTextFieldNameChange = (event) => {
        dispatch(changeNameAddProduct(event))
    }
    const onTextFieldBuyPriceChange = (event) => {
        dispatch(changeBuyPriceAddProduct(event))
    }
    const onTextFieldpromotionPriceChange = (event) => {
        dispatch(changePromotionPriceAddProduct(event))
    }
    const onTextFieldDescriptionChange = (event) => {
        dispatch(changeDescriptionAddProduct(event))
    }
    const onTextFieldAmountChange = (event) => {
        dispatch(changeAmountAddProduct(event))
    }
    const onTextFieldImageUrlChange = (event) => {
        dispatch(changeImageUrlAddProduct(event))
    }
    const onSelectTypeChange = (event) => {
        dispatch(changeTypeAddProduct(event))
    }
    const onBtnConfirmCreateProduct = () => {
        dispatch(createProduct(addProduct))
    }
    return (<>
        <Modal
            open={openModalAddProduct}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={styleCheckOut} textAlign={"center"}>
                <Typography fontWeight={700} fontSize={30} marginBottom={4} id="modal-modal-description" sx={{ mt: 2 }}>
                    ADD PRODUCT
                </Typography>
                <Grid container marginTop={2} justifyContent={"center"}>
                    <Grid item xs={6}>
                        <TextField
                            sx={{ width: "90%" }}
                            required
                            label="Name"
                            value={addProduct.name}
                            onChange={onTextFieldNameChange}
                        />
                        <p className="invalid-textfield" style={{ display: addProductNameAlertDisplay }}>{addProductNameAlertMessage}</p>
                    </Grid>
                    <Grid item xs={6} display={"flex"} flexDirection={"column"} justifyContent={"center"}>
                        <select className="select-modal form-control text-center" onChange={onSelectTypeChange} value={addProduct.type}
                        >
                            <option value="0">Types</option>
                            {types ? types.map((element, index) => {
                                return (
                                    <option key={element.name} value={element._id}>{element.name}</option>
                                )
                            }) : ""}
                        </select>
                        <p className="invalid-textfield" style={{ display: addProductTypeAlertDisplay }}>{addProductTypeAlertMessage}</p>
                    </Grid>
                </Grid>
                <Grid container marginTop={2} justifyContent={"center"}>
                    <Grid item xs={6}>
                        <TextField
                            type="number"
                            sx={{ width: "90%" }}
                            required
                            label="Buy Price"
                            value={addProduct.buyPrice}
                            onChange={onTextFieldBuyPriceChange}
                        />
                        <p className="invalid-textfield" style={{ display: addProductBuyPriceAlertDisplay }}>{addProductBuyPriceAlertMessage}</p>
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            type="number"
                            sx={{ width: "90%" }}
                            required
                            label="Promotion Price"
                            value={addProduct.promotionPrice}
                            onChange={onTextFieldpromotionPriceChange}
                        />
                        <p className="invalid-textfield" style={{ display: addProductPromotionPriceAlertDisplay }}>{addProductPromotionPriceAlertMessage}</p>
                    </Grid>
                </Grid>
                <Grid container marginTop={2} justifyContent={"center"}>
                    <Grid item xs={6}>
                        <TextField
                            sx={{ width: "90%" }}
                            label="Description"
                            value={addProduct.description}
                            onChange={onTextFieldDescriptionChange}
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            type="number"
                            sx={{ width: "90%" }}
                            required
                            label="Amount"
                            value={addProduct.amount}
                            onChange={onTextFieldAmountChange}
                        />
                    </Grid>
                </Grid>
                <Grid container marginTop={2} justifyContent={"center"}>
                    <Grid item xs={12}>
                        <TextField
                            type="text"
                            sx={{ width: "95%" }}
                            required
                            label="Image Url"
                            value={addProduct.imageUrl}
                            onChange={onTextFieldImageUrlChange}
                        />
                        <p className="invalid-textfield" style={{ display: addProductImageUrlAlertDisplay }}>{addProductImageUrlAlertMessage}</p>
                    </Grid>
                </Grid>
                <Grid container marginTop={2} >
                    <Grid sx={{ textAlign: "end", marginTop: 2 }} item xs={12}>
                        <Button onClick={handleClose} style={{ marginRight: 10 }} variant="contained" color="error">Close</Button>
                        <Button onClick={onBtnConfirmCreateProduct} variant="contained" color="success">Create Product</Button>
                    </Grid>
                </Grid>
            </Box>
        </Modal >
    </>)
}
export const ModalEditProduct = () => {
    const dispatch = useDispatch()
    const { openModalEditProduct, types, editProduct, editProductNameAlertDisplay, editProductNameAlertMessage, editProductId,
        editProductBuyPriceAlertDisplay, editProductBuyPriceAlertMessage, editProductImageUrlAlertDisplay, editProductImageUrlAlertMessage,
        editProductTypeAlertMessage, editProductTypeAlertDisplay, editProductPromotionPriceAlertDisplay, editProductPromotionPriceAlertMessage
    } = useSelector((reduxData) => reduxData.tableReducers)

    const handleClose = () => {
        dispatch(closeModalHandler())
    }
    const onTextFieldNameChange = (event) => {
        dispatch(changeNameEditProduct(event))
    }
    const onTextFieldBuyPriceChange = (event) => {
        dispatch(changeBuyPriceEditProduct(event))
    }
    const onTextFieldpromotionPriceChange = (event) => {
        dispatch(changePromotionPriceEditProduct(event))
    }
    const onTextFieldDescriptionChange = (event) => {
        dispatch(changeDescriptionEditProduct(event))
    }
    const onTextFieldAmountChange = (event) => {
        dispatch(changeAmountEditProduct(event))
    }
    const onTextFieldImageUrlChange = (event) => {
        dispatch(changeImageUrlEditProduct(event))
    }
    const onSelectTypeChange = (event) => {
        dispatch(changeTypeEditProduct(event))
    }
    const onBtnConfirmUpdateProduct = () => {
        dispatch(updateProduct(editProduct, editProductId))
    }
    return (<>
        <Modal
            open={openModalEditProduct}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={styleCheckOut} textAlign={"center"}>
                <Typography fontWeight={700} fontSize={30} marginBottom={4} id="modal-modal-description" sx={{ mt: 2 }}>
                    UPDATE PRODUCT
                </Typography>
                <Grid container marginTop={2} justifyContent={"center"}>
                    <Grid item xs={6}>
                        <TextField
                            sx={{ width: "90%" }}
                            required
                            label="Name"
                            value={editProduct.name}
                            onChange={onTextFieldNameChange}
                        />
                        <p className="invalid-textfield" style={{ display: editProductNameAlertDisplay }}>{editProductNameAlertMessage}</p>
                    </Grid>
                    <Grid item xs={6} display={"flex"} flexDirection={"column"} justifyContent={"center"}>
                        <select className="select-modal form-control text-center" onChange={onSelectTypeChange} value={editProduct.type}
                        >
                            <option value="0">Types</option>
                            {types ? types.map((element, index) => {
                                return (
                                    <option key={element.name} value={element._id}>{element.name}</option>
                                )
                            }) : ""}
                        </select>
                        <p className="invalid-textfield" style={{ display: editProductTypeAlertDisplay }}>{editProductTypeAlertMessage}</p>
                    </Grid>
                </Grid>
                <Grid container marginTop={2} justifyContent={"center"}>
                    <Grid item xs={6}>
                        <TextField
                            type="number"
                            sx={{ width: "90%" }}
                            required
                            label="Buy Price"
                            value={editProduct.buyPrice}
                            onChange={onTextFieldBuyPriceChange}
                        />
                        <p className="invalid-textfield" style={{ display: editProductBuyPriceAlertDisplay }}>{editProductBuyPriceAlertMessage}</p>
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            type="number"
                            sx={{ width: "90%" }}
                            required
                            label="Promotion Price"
                            value={editProduct.promotionPrice}
                            onChange={onTextFieldpromotionPriceChange}
                        />
                        <p className="invalid-textfield" style={{ display: editProductPromotionPriceAlertDisplay }}>{editProductPromotionPriceAlertMessage}</p>
                    </Grid>
                </Grid>
                <Grid container marginTop={2} justifyContent={"center"}>
                    <Grid item xs={6}>
                        <TextField
                            sx={{ width: "90%" }}
                            label="Description"
                            value={editProduct.description}
                            onChange={onTextFieldDescriptionChange}
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            type="number"
                            sx={{ width: "90%" }}
                            required
                            label="Amount"
                            value={editProduct.amount}
                            onChange={onTextFieldAmountChange}
                        />
                    </Grid>
                </Grid>
                <Grid container marginTop={2} justifyContent={"center"}>
                    <Grid item xs={12}>
                        <TextField
                            type="text"
                            sx={{ width: "95%" }}
                            required
                            label="Image Url"
                            value={editProduct.imageUrl}
                            onChange={onTextFieldImageUrlChange}
                        />
                        <p className="invalid-textfield" style={{ display: editProductImageUrlAlertDisplay }}>{editProductImageUrlAlertMessage}</p>
                    </Grid>
                </Grid>
                <Grid container marginTop={2} >
                    <Grid sx={{ textAlign: "end", marginTop: 2 }} item xs={12}>
                        <Button onClick={handleClose} style={{ marginRight: 10 }} variant="contained" color="error">Close</Button>
                        <Button onClick={onBtnConfirmUpdateProduct} variant="contained" color="success">Update Product</Button>
                    </Grid>
                </Grid>
            </Box>
        </Modal >
    </>)
}
export const ModalDeleteProduct = (props) => {
    const dispatch = useDispatch()
    const { openModalDeleteProduct, deleteProductId, failMessage } = useSelector((reduxData) => reduxData.tableReducers)
    const handleClose = () => {
        dispatch(closeModalHandler())
    }
    const onBtnConfirmDeleteProductClick = () => {
        dispatch(deleteProduct(deleteProductId))
    }
    return (<>
        <Modal
            open={openModalDeleteProduct}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={style} textAlign={"center"}>
                <Typography fontWeight={700} fontSize={24} marginBottom={2} id="modal-modal-description" sx={{ mt: 2 }}>
                    DELETE PRODUCT
                </Typography>
                <Typography fontSize={16} id="modal-modal-description" sx={{ mt: 2 }}>
                    Do you want to delete this product!!!
                </Typography>
                <Grid container marginTop={1} justifyContent={"center"}>
                    <Grid sx={{ textAlign: "end", marginTop: 2 }} item xs={12}>
                        <Button onClick={handleClose} variant="contained" color="success">Close</Button>
                        <Button style={{ marginLeft: 10, background: "red" }} onClick={onBtnConfirmDeleteProductClick} variant="contained" >Delete</Button>
                    </Grid>
                </Grid>
                {failMessage ? <p style={{ color: "red" }}>{failMessage}</p> : ""}
            </Box>
        </Modal >
    </>)
}
export const ModalEditOrder = () => {
    const dispatch = useDispatch()
    const { editOrder, orderIdEdit, openModalEditOrder } = useSelector((reduxData) => reduxData.tableReducers)
    const { totalPrice, productObject } = useSelector((reduxData) => reduxData.cartReducers)

    const handleClose = () => {
        dispatch(closeModalHandler())
    }
    const onTextFieldNoteCheckOutChange = (event) => {
        dispatch(inputNoteUpdateOrderChange(event))
    }
    const onBtnConfirmUpdateClick = (event) => {
        dispatch(updateOrder(editOrder, orderIdEdit, totalPrice, productObject))
    }
    return (<>
        <Modal
            open={openModalEditOrder}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={styleCheckOut} textAlign={"center"}>
                <Typography fontWeight={700} fontSize={30} marginBottom={2} id="modal-modal-description" sx={{ mt: 2 }}>
                    EDIT ORDER
                </Typography>
                <Grid container marginTop={2} justifyContent={"center"}>
                    <Grid item xs={6}>
                        <DatePickerComponent />
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            sx={{ width: "90%" }}
                            label="Note"
                            value={editOrder.note}
                            onChange={onTextFieldNoteCheckOutChange}
                        />
                    </Grid>
                </Grid>
                <Grid container marginTop={2} >
                    <Grid sx={{ textAlign: "end", marginTop: 2 }} item xs={12}>
                        <Button onClick={handleClose} style={{ marginRight: 10 }} variant="contained" color="error">Close</Button>
                        <Button onClick={onBtnConfirmUpdateClick} variant="contained" color="success">Confirm update order</Button>
                    </Grid>
                </Grid>
            </Box>
        </Modal >
    </>)
}
export const ModalDeleteOrder = (props) => {
    const dispatch = useDispatch()
    const { openModalDeleteOrder, orderIdDelete } = useSelector((reduxData) => reduxData.tableReducers)
    const handleClose = () => {
        dispatch(closeModalHandler())
    }
    const onBtnConfirmDeleteOrderClick = () => {
        dispatch(deleteOrder(orderIdDelete))
    }
    return (<>
        <Modal
            open={openModalDeleteOrder}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box sx={style} textAlign={"center"}>
                <Typography fontWeight={700} fontSize={24} marginBottom={2} id="modal-modal-description" sx={{ mt: 2 }}>
                    DELETE ORDER
                </Typography>
                <Typography fontSize={16} id="modal-modal-description" sx={{ mt: 2 }}>
                    Do you want to delete this order!!!
                </Typography>
                <Grid container marginTop={1} justifyContent={"center"}>
                    <Grid sx={{ textAlign: "end", marginTop: 2 }} item xs={12}>
                        <Button onClick={handleClose} variant="contained" color="success">Close</Button>
                        <Button style={{ marginLeft: 10, background: "red" }} onClick={onBtnConfirmDeleteOrderClick} variant="contained" >Delete</Button>
                    </Grid>
                </Grid>
            </Box>
        </Modal >
    </>)
}