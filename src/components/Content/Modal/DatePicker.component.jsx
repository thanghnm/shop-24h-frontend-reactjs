import * as React from 'react';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { datePicked } from '../../../action/cart.action';
import { useDispatch, useSelector } from 'react-redux';
import dayjs from 'dayjs';
import { DateTimePicker } from '@mui/x-date-pickers';
import { editDatePicked } from '../../../action/table.action';

export default function DatePickerComponent() {
  const { checkOut } = useSelector((reduxData) => reduxData.cartReducers)
  const { openModalEditOrder } = useSelector((reduxData) => reduxData.tableReducers)

  const dispatch = useDispatch()
  const onDatePickerChange = (event) => {
    if (openModalEditOrder === true) {
      dispatch(editDatePicked(event))
    }
    else {
      dispatch(datePicked(event))
    }
  }
  const now = dayjs().$d
  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <DateTimePicker onChange={onDatePickerChange} label="Shipped Date" />
    </LocalizationProvider>
  );
}