import { Button, Container, Form } from "react-bootstrap"
import { useDispatch, useSelector } from "react-redux"
import { inputPasswordChangeHandler, inputUsernameChangeHandler, logInAction } from "../../../action/login.action"
import loading from "../../../assets/images/loading.gif"
import { ModalFail } from "../Modal/Modal.component"
import { authorizeAdmin } from "../../../action/table.action"

const LogInForm = () => {
    const dispatch = useDispatch()
    const { logIn, userNotFound, errorPassword, pending,usernameBlank,openUnauthorizeModal } = useSelector((reduxData) => reduxData.logInReducers)
    const { authorize } = useSelector((reduxData) => reduxData.tableReducers)

    const onInputUsernameChangeHandler = (event) => {
        dispatch(inputUsernameChangeHandler(event))
    }
    const onInputPasswordChangeHandler = (event) => {
        dispatch(inputPasswordChangeHandler(event))
    }
    const onBtnLogInClick = () => {
        dispatch(logInAction(logIn)
        // setTimeout(()=>{
        //     dispatch(authorizeAdmin())
        // },3000)
        )
    }
    function handleSubmit(e) {
        e.preventDefault();
    }
    return (
        <Container className="login-background w-0 flex "  >
            <h1 style={{ fontStyle: "italic", color: "red", fontWeight: 700, marginTop: 20 }} className="text-center log-in-title">WELCOME</h1>
            <Form onSubmit={handleSubmit} className="log-in-form justify-center">
                <h3 style={{ marginBottom: 20 }} className="text-center">Log in to continue</h3>
                <Form.Group className="mb-3 " controlId="formGroupEmail">
                    <Form.Label>Username</Form.Label>
                    <Form.Control value={logIn.username} onChange={onInputUsernameChangeHandler} type="text" placeholder="Enter username" />
                    <p style={{ display: userNotFound, color: "red" }}>Username not found</p>
                    <p style={{ display: usernameBlank, color: "red" }}>Please input username</p>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formGroupPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control value={logIn.password} onChange={onInputPasswordChangeHandler} type="password" placeholder="Password" />
                    <p style={{ display: errorPassword, color: "red" }}>Error password</p>
                </Form.Group>
                <div className="btn-login">
                    <button onClick={onBtnLogInClick} className="btn btn-success flex"><p style={{ color: "white", margin: 0, marginRight: "5px" }}>LOG IN</p>
                        {pending ? <img style={{ width: 24, height: 24 }} className="pl-2" src={loading} alt="mygif" /> : ""}</button>
                    <a style={{ marginTop: 10, fontStyle: "italic", textDecoration: "underline", color: "grey" }} href="#">Cannot log in ? Help!</a>
                    <a style={{ marginTop: 10, fontStyle: "italic", textDecoration: "underline", color: "blue" }} href="/signup">Sign Up</a>
                </div>
            </Form>
            {window.location.pathname==="/login"?[]:<ModalFail openModal={openUnauthorizeModal==true&&authorize==false} title={"Unauthorize"} content="You dont have authorize to view this page" />}
        </Container>
    )
}
export default LogInForm