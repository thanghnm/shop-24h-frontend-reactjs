import Breadcrumbs from "./Breadcrumbs/Breadcrumbs"
import Filter from "./ProductList/Filter.content"
import Product from "./ProductList/Product.content"

const ListComponent = () => {
    return (
        <div className="p-5">
            <Breadcrumbs thisPage={"Products"} />
            <div className="flex flex-wrap   justify-center ">
                <div className="bg-slate-100 rounded-2xl col-sm-12 col-md-12 col-lg-3 col-xl-3 p-2 justify-start"><Filter/></div>
                <div className=" rounded-2xl col-sm-12 col-md-12 col-lg-9 col-xl-9 p-2"><Product/></div>
            </div>
        </div>
    )
}
export default ListComponent