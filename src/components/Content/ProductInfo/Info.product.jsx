import { useDispatch, useSelector } from "react-redux";
import { getProductFromStorage } from "../../../action/cart.action";
import { addCart } from "../../../action/productList.action";
import RadioButtonCheckedIcon from '@mui/icons-material/RadioButtonChecked';
import RadioButtonUncheckedIcon from '@mui/icons-material/RadioButtonUnchecked';
import { Button, Checkbox } from "@mui/material";
import { chooseSizeL, chooseSizeM, chooseSizeS, chooseSizeXL, chooseSizeXXL, plusItem, subtractItem } from "../../../action/productInfo.action";
const Info = (props) => {
   const { size,infoQty } = useSelector((reduxData) => reduxData.productInfoReducers);
   const dispatch = useDispatch()
   const onBtnAddCartClick = () => {
      dispatch(addCart(props.data,infoQty))
      dispatch(getProductFromStorage())
   }
   const onBtnSize28Check =()=>{
      dispatch(chooseSizeS())
   }
   const onBtnSize29Check =()=>{
      dispatch(chooseSizeM())
   }
   const onBtnSize30Check =()=>{
      dispatch(chooseSizeL())
   }
   const onBtnSize31Check =()=>{
      dispatch(chooseSizeXL())
   }
   const onBtnSize32Check =()=>{
      dispatch(chooseSizeXXL())
   }
   const onBtnPlusClick = (event) => {
      dispatch(plusItem(event))
  }
  const onBtnSubtractClick = (event) => {
      dispatch(subtractItem(event))
  }
   const label = { inputProps: { 'aria-label': 'Checkbox demo' } };
   return (
      <div className="p-3">
         <h4 className="info-name">{props.name.toUpperCase()}</h4>
         <p className="mt-3 info-price">
            {props.price} $ <span className="info-discount">{props.discount} $</span>
         </p>
         <p >All taxes and duties included</p>
         <p>SHORT-SLEEVE SHIRT WITH POINTED COLLAR AND SIDE SPLITS.</p>
         <p className="info-option">Color: Black</p>
         <div className="flex items-center w-full">
            <button className="btn-color-black"></button>
            {/* <button className="btn-color-white"></button>
            <button className="btn-color-red"></button> */}
         </div>
         <p className="info-option">Size</p>
         <div className="flex flex-wrap">
            <div className="flex items-center">
               <p className="size-p">28</p>
               <Checkbox checked={size.small} onChange={onBtnSize28Check} {...label} icon={<RadioButtonUncheckedIcon />} checkedIcon={<RadioButtonCheckedIcon />} />
            </div>
            <div className="flex items-center">
               <p className="size-p">29</p>
               <Checkbox checked={size.medium} onChange={onBtnSize29Check} {...label} icon={<RadioButtonUncheckedIcon />} checkedIcon={<RadioButtonCheckedIcon />} />
            </div>
            <div className="flex items-center">
               <p className="size-p">30</p>
               <Checkbox checked={size.large} onChange={onBtnSize30Check} {...label} icon={<RadioButtonUncheckedIcon />} checkedIcon={<RadioButtonCheckedIcon />} />
            </div>
            <div className="flex items-center">
               <p className="size-p">31</p>
               <Checkbox checked={size.xl} onChange={onBtnSize31Check} {...label} icon={<RadioButtonUncheckedIcon />} checkedIcon={<RadioButtonCheckedIcon />} />
            </div>
            <div className="flex items-center">
               <p className="size-p">32</p>
               <Checkbox checked={size.xxl} onChange={onBtnSize32Check} {...label} icon={<RadioButtonUncheckedIcon />} checkedIcon={<RadioButtonCheckedIcon />} />
            </div>
         </div>
         <p className="info-option">Quantity</p>
         <div className="btn-group flex" role="group" aria-label="Basic example">
                                    <Button variant="outlined" onClick={onBtnSubtractClick} color="warning" style={{color:"black"}}  type="button" className="btn btn-subtract">-</Button>
                                    <Button variant="outlined" style={{color:"black"}} color="warning" className="price-decor btn ">{infoQty}
                                    </Button>
                                    <Button variant="outlined" onClick={onBtnPlusClick} color="warning" style={{ color:"black"}}  type="button" className="btn btn-plus">+</Button>
                                </div>
         {props.amount > 0 ? <button onClick={onBtnAddCartClick} className="btn btn-dark w-full mt-3">ADD TO CART</button> : <button disabled className="btn btn-dark w-full">SOLD OUT</button>}
      </div>
   )
}
export default Info