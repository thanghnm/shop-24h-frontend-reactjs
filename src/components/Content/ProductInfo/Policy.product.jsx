import LocalShippingIcon from '@mui/icons-material/LocalShipping';
import MoneyOffIcon from '@mui/icons-material/MoneyOff';
import ContactPhoneIcon from '@mui/icons-material/ContactPhone';
import RestoreIcon from '@mui/icons-material/Restore';
const Policy = () => {
    return (
        <div className="flex detail-policy flex-wrap mt-3 ">
            <div className="col-6 col-md-3 col-lg-3 col-xl-3 border-solid border-black border-2 p-2 text-center">
                <LocalShippingIcon fontSize="large" /> Fast Delivery
                <p>From 3 - 7 day</p>
            </div>
            <div className="col-6 col-md-3 col-lg-3 col-xl-3 border-solid border-black border-2 p-2 text-center">
                <MoneyOffIcon fontSize="large" /> Free ship
                <p>Free shipping inner city</p>
            </div>
            <div className="col-6 col-md-3 col-lg-3 col-xl-3 border-solid border-black border-2 p-2 text-center">
                <RestoreIcon fontSize="large" /> Free return
                <p>Within 3 day</p>
            </div>
            <div className="col-6 col-md-3 col-lg-3 col-xl-3 border-solid border-black border-2 p-2 text-center">
                <ContactPhoneIcon fontSize="large" /> Hotline
                <p>090000000</p>
            </div>
        </div>
    )
}
export default Policy