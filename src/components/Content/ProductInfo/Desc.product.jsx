const Description = () => {
    return (
        <div className="p-3">
            <h5 className="info-name">DETAILS</h5>
            <p>This long-sleeved silk twill shirt has a concealed button-down front and features .<br />
                - Short sleeves<br />
                - Pointed collar<br />
                - Concealed button placket<br />
                - Buttoned cuffs<br />
                - Outer composition: 100% Silk</p>
            <h5 className="info-name">SIZE & FIT</h5>
            <p>- The model is 1,78 m tall and wears size 38<br />
                - All sizes fit Italian standards</p>
            <h5 className="info-name">CARE</h5>
            <p>- For cleaning and care instructions, we advise you to carefully read the product label present inside the garment.
                 For any questions or concerns, please visit our Care Guide page or entrust its
                  care to a specialist cleaning service.</p>

        </div>
    )
}
export default Description