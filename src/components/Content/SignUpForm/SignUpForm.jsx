import { Container, Form } from "react-bootstrap"
import { useDispatch, useSelector } from "react-redux"
import { changeConfirmPassword, changeEmail, changePassword, changePhone, changeUsername, signUpUser } from "../../../action/signup.action"
import { ModalSuccess } from "../Modal/Modal.component"

const SignUpForm = ()=>{
    const dispatch = useDispatch()
    const {signUp,usernameInvalidMessage,displayUsernameAlert,emailInvalidMessage,displayEmailAlert,
        phoneInvalidMessage,displayPhoneAlert,passwordInvalidMessage,displayPasswordAlert,confirmPasswordInvalidMessage
        ,displayConfirmPasswordAlert,signUpSuccess} = useSelector((reduxData)=>reduxData.signUpReducers)
    const onInputUsernameChangeHandler =(event)=>{
        dispatch(changeUsername(event))
    }
    const onInputPasswordChangeHandler =(event)=>{
        dispatch(changePassword(event))
    }
    const onInputConfirmPasswordChangeHandler =(event)=>{
        dispatch(changeConfirmPassword(event))
    }
    const onInputEmailChangeHandler =(event)=>{
        dispatch(changeEmail(event))
    }
    const onInputPhoneChangeHandler =(event)=>{
        dispatch(changePhone(event))
    }
    const onBtnSignUpClick =()=>{
        dispatch(signUpUser(signUp))
    }
    function handleSubmit(e) {
        e.preventDefault();
    }
    return(
        <Container style={{height:"120vh"}} className="login-background w-0 flex "  >
            <h1 style={{fontStyle:"italic",color:"red",fontWeight:700,marginTop:20}} className="text-center log-in-title">WELCOME</h1>
            <Form onSubmit={handleSubmit} className="log-in-form justify-center">
            <h3 style={{marginBottom:20,fontWeight:"bold"}} className="text-center">SIGN UP</h3>
                <Form.Group className="mb-3 " controlId="formGroupUsername">
                    <Form.Label>Username</Form.Label>
                    <Form.Control onChange={onInputUsernameChangeHandler} value={signUp.username} type="text" placeholder="Enter username" />
                    <p style={{display:displayUsernameAlert}} className="invalid-textfield">{usernameInvalidMessage}</p>
                </Form.Group>
                <Form.Group className="mb-3 " controlId="formGroupEmail">
                    <Form.Label>Email</Form.Label>
                    <Form.Control onChange={onInputEmailChangeHandler} value={signUp.email} type="email" placeholder="Enter email" />
                    <p style={{display:displayEmailAlert}} className="invalid-textfield">{emailInvalidMessage}</p>
                </Form.Group>
                <Form.Group className="mb-3 " controlId="formGroupPhone">
                    <Form.Label>Phone</Form.Label>
                    <Form.Control onChange={onInputPhoneChangeHandler} value={signUp.phone} type="number" placeholder="Enter Phone" />
                    <p style={{display:displayPhoneAlert}} className="invalid-textfield">{phoneInvalidMessage}</p>

                </Form.Group>
                <Form.Group className="mb-3" controlId="formGroupPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control onChange={onInputPasswordChangeHandler} value={signUp.password} type="password" placeholder="Password" />
                    <p style={{display:displayPasswordAlert}} className="invalid-textfield">{passwordInvalidMessage}</p>

                </Form.Group>
                <Form.Group className="mb-3" controlId="formGroupConfirmPassword">
                    <Form.Label>Confirm password</Form.Label>
                    <Form.Control onChange={onInputConfirmPasswordChangeHandler} value={signUp.confirmPassword} type="password"
                     placeholder="Confirm password" />
                    <p style={{display:displayConfirmPasswordAlert}} className="invalid-textfield">{confirmPasswordInvalidMessage}</p>

                </Form.Group>
                <div className="btn-login">
                <button onClick={onBtnSignUpClick} className="btn btn-success">Sign up</button>
                <a style={{marginTop:10,fontStyle:"italic",textDecoration:"underline", color:"grey"}} href="#">Cannot log in ? Help!</a>
                <a style={{marginTop:10,fontStyle:"italic",textDecoration:"underline", color:"red"}} href="/login">Already have account ? Log in !</a>
                </div>
            </Form>
				{signUpSuccess === true ? <ModalSuccess content="Create user successfully !!!" openModal={signUpSuccess} /> : ""}

        </Container>
    )
}
export default SignUpForm