import Breadcrumb from 'react-bootstrap/Breadcrumb';

const Breadcrumbs = (props) => {
    return (
        <>
            <Breadcrumb style={{marginLeft:"5%"}} >
                <Breadcrumb.Item  href="/">Home</Breadcrumb.Item>
                {props.parentPage?<Breadcrumb.Item href = {props.urlParentPage}>
                    {props.parentPage}
                </Breadcrumb.Item>:""}
                <Breadcrumb.Item active>
                    {props.thisPage}
                </Breadcrumb.Item>
            </Breadcrumb>
            
        </>
    )
}
export default Breadcrumbs;