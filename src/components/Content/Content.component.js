import { Component } from "react";
import Carousel from "./Carousel/Carousel.content";
import Product from "./ProductList/Product.content";
import Category from "./Category/Category";

class Content extends Component{
    render(){
        return(
            <>
                <Carousel/>
                <Category/>
                <Product />
            </>
        )
    }
}
export default Content