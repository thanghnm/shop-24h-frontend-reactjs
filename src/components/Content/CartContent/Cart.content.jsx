import { useDispatch, useSelector } from "react-redux"
import Breadcrumbs from "../Breadcrumbs/Breadcrumbs"
import { useEffect } from "react"
import { checkOut, closeNoticePlusItem, fetchCountry, getProductFromStorage, noticeSignIn, paymentBank, paymentCredit, plusItem, removeItem, subtractItem, updateCart, validateProduct } from "../../../action/cart.action"
import { Button, Checkbox, TextField } from "@mui/material"
import { fetchUser } from "../../../action/productList.action"
import RadioButtonCheckedIcon from '@mui/icons-material/RadioButtonChecked';
import RadioButtonUncheckedIcon from '@mui/icons-material/RadioButtonUnchecked';
import { ModalCheckOut, ModalCreateOrderSuccess, ModalSignIn } from "../Modal/Modal.component"
import { openModalEditOrder, shoppingMore } from "../../../action/table.action"
import productNotFound from "../../../assets/images/shopping-cart/product-not-found.png"
import WarningAmberIcon from '@mui/icons-material/WarningAmber';
import CreditCardIcon from '@mui/icons-material/CreditCard';
import AccountBalanceIcon from '@mui/icons-material/AccountBalance';
const CartContent = () => {
    const dispatch = useDispatch()
    const { productObject, totalPrice, key, update, noticeOutOfStore, validateData, outOfStock, payment } = useSelector((reduxData) => reduxData.cartReducers)
    const { editCart, orderIdEdit } = useSelector((reduxData) => reduxData.tableReducers)
    const { userData } = useSelector((reduxData) => reduxData.productReducers)
    useEffect(() => {
        dispatch(getProductFromStorage())
        dispatch(fetchUser())
        dispatch(fetchCountry())
        dispatch(validateProduct())
    }, [update === true])
    const onBtnPlusClick = (event) => {
        dispatch(plusItem(event))
        setTimeout(() => {
            dispatch(closeNoticePlusItem(event))
        }, 5000)
    }
    const onBtnSubtractClick = (event) => {
        dispatch(subtractItem(event))
    }
    const onBtnRemoveClick = (event) => {
        dispatch(removeItem(event))
    }
    const onBtnUpdateClick = () => {
        dispatch(updateCart())
    }
    const onBtnCheckOutClick = (event) => {
        if (userData || window.location.pathname === "/admin/orders") {
            dispatch(checkOut(event))
        }
        else {
            dispatch(noticeSignIn())
        }
    }
    const onBtnShoppingMoreClick = () => {
        dispatch(shoppingMore())
    }
    const onBtnUpdateOrderClick = () => {
        dispatch(openModalEditOrder())
    }
    const onCheckBoxCreditClick = () => {
        dispatch(paymentCredit())
    }
    const onCheckBoxBankClick = () => {
        dispatch(paymentBank())
    }
    const label = { inputProps: { 'aria-label': 'Checkbox demo' } };
    return (
        <div className="p-5">
            {productObject && productObject.length > 0 ?
                <div style={{ borderTop: "3px black solid", borderBottom: "3px solid black" }} className="cart-products p-8">
                    <div className="flex pt-3 pb-3">
                        <div className="col-sm-3 col-md-2 col-lg-2 col-xl-3">
                            <h4>PRODUCT</h4>
                        </div>
                        <div className="col-sm-0 col-md-3 col-lg-3 col-xl-3"></div>
                        <div className="col-sm-3 col-md-2 col-lg-2 col-xl-1 text-center">
                            <h4>PRICE</h4>
                        </div>
                        <div className="col-sm-3 col-md-3 col-lg-3 col-xl-3">
                            <h4 className="text-center">QTY</h4>
                        </div>
                        <div className="col-sm-3 col-md-2 col-lg-2 col-xl-2 text-center">
                            <h4>UNIT PRICE</h4>
                        </div>
                    </div>
                </div> : []}
            {productObject && productObject.length > 0 && validateData && validateData.length === productObject.length ? productObject.map((element, index) => {
                return (
                    <div key={index}>
                        <div style={{ borderBottom: "3px solid black" }} className="flex  item-product items-center">
                            <div className="col-sm-6 col-md-3 col-lg-3 col-xl-3"><img className="cart-img" src={element.imageUrl} /></div>
                            <div className="col-sm-6 col-md-2 col-lg-2 col-xl-3 p-5">
                                <div className="product-info">
                                    <h5 className="text-center">{element.name}</h5>
                                </div>
                            </div>
                            <div className="price-decor col-sm-4 col-md-2 col-lg-2 col-xl-1 text-center">
                                {validateData && validateData[index].amount > 0 ||editCart===true? <p>${element.promotionPrice.toLocaleString()}</p> : []}
                            </div>
                            <div style={{ position: "relative" }} className="col-sm-4 col-md-3 col-lg-3 col-xl-3 justify-center flex flex-row ">
                                {noticeOutOfStore[index] ? <div id="notice-out-of-store">Only {element.quantity} product left</div> : ""}
                                {validateData && validateData[index].amount > 0||editCart===true ?
                                    <div className="btn-group flex" role="group" aria-label="Basic example">
                                        <button onClick={onBtnSubtractClick} style={{ borderRight: "2px solid white" }} data-qty={index} type="button" className="btn bg-dark text-white btn-subtract">-</button>
                                        <button className="price-decor btn bg-dark text-white">{element.quantity}
                                        </button>
                                        <button onClick={onBtnPlusClick} style={{ borderLeft: "2px solid white" }} data-qty={index} type="button" className="btn bg-dark text-white btn-plus">+</button>
                                    </div> : <div className="div-outofstorage" style={{ color: "red" }}><WarningAmberIcon style={{ color: "red" }} /> Sorry this product is out of stock !</div>}
                                <button onClick={onBtnRemoveClick} data-remove={index} style={{ color: "red", top: 70, position: "absolute" }} className="btn btn-link btn-remove absolute">Remove Item</button>
                            </div>
                            {validateData && validateData[index].amount > 0 ||editCart===true? <div className=" price-decor col-sm-4 col-md-2 col-lg-2 col-xl-2 text-center">${(productObject[index].quantity * element.promotionPrice).toLocaleString()}</div> : ""}
                        </div>
                    </div>)
            }) : <div className="flex justify-center items-center">
                <img className="img-cart-notfound" width={"80%"} src={productNotFound} alt="404" />
            </div>}
            <div style={{ justifyContent: "space-between", display: "flex", marginTop: 50 }}>
                {editCart ? "" : <Button href="/products" className="btn bg-secondary text-white">CONTINUE SHOPPING</Button>}
            </div>
            <div className="div-total-cost">
                <div className="flex col-md-12 col-lg-6 div-coupon">
                    <TextField
                        id="filled-search"
                        label="Discount code"
                        variant="outlined"
                        color="success"
                    />
                    <button style={{ height: "55px" }} className="btn btn-dark ml-5">Apply Coupon</button>
                </div>
                <div className="checkout col-sm-12 col-md-12 col-lg-6">
                    <h4 className="cart-pay-p mb-4 text-center text-white">
                        Cart total
                    </h4>
                    <div style={{ justifyContent: "space-between", display: "flex" }} >
                        <p className="cart-pay-p">Subtotal</p>
                        <p>${totalPrice ? totalPrice.toLocaleString() : 0}</p>
                    </div>
                    <div className="mt-3" style={{ justifyContent: "space-between", display: "flex" }} >
                        <p className="cart-pay-p">Total</p>
                        <p>${totalPrice ? totalPrice.toLocaleString() : 0}</p>
                    </div>
                    <h4 className="cart-pay-p text-center mb-2 text-white">
                        Choose payment
                    </h4>
                    <div style={{ display: "flex", justifyContent: "space-evenly" }} >
                        <div className="flex items-center">
                            <p className="payment-p mb-0">Credit card</p><CreditCardIcon />
                            <Checkbox checked={payment.credit} onClick={onCheckBoxCreditClick} {...label} icon={<RadioButtonUncheckedIcon style={{ color: "red" }} />}
                                checkedIcon={<RadioButtonCheckedIcon style={{ color: "white" }} />} />
                        </div>
                        <div className="flex items-center">
                            <p className="payment-p mb-0">Banking</p><AccountBalanceIcon />
                            <Checkbox checked={payment.bank} onClick={onCheckBoxBankClick} {...label} icon={<RadioButtonUncheckedIcon style={{ color: "red" }} />}
                                checkedIcon={<RadioButtonCheckedIcon style={{ color: "white" }} />} />
                        </div>
                    </div>
                    {editCart ? "" : <button disabled={productObject && productObject.length > 0 && outOfStock === false ? false : true} data-email={userData ? userData.email : ""}
                        data-phone={userData ? userData.phone : ""} onClick={onBtnCheckOutClick} className="btn btn-success mt-4">
                        PROCESS TO CHECK OUT</button>}
                </div>
            </div>
            {editCart ? <div style={{ justifyContent: "space-between" }} className="flex pt-3 pb-3 justify-between">
                <Button onClick={onBtnShoppingMoreClick} style={{ background: "green" }} className="bg-green-500 text-white">Shopping more</Button>
                <Button onClick={onBtnUpdateOrderClick} style={{ background: "blue" }} className=" text-white">Update Order</Button>
            </div> : ""}
            <ModalSignIn />
            <ModalCheckOut />
            <ModalCreateOrderSuccess />
        </div>
    )
}
export default CartContent
