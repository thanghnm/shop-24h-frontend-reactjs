import React, { useState } from "react";
import Carousel from 'react-bootstrap/Carousel'
import background1 from "../../../assets/images/carousel/bg-caro-3.jpg"
import background2 from "../../../assets/images/carousel/bg-caro-1.jpg"
import background3 from "../../../assets/images/carousel/bg-caro-2.jpg"
// import imgCarousel1 from "../../../assets/images/carousel/clipart1860539.png"
// import imgCarousel2 from "../../../assets/images/carousel/glass-2.png"
// import imgCarousel3 from "../../../assets/images/carousel/glass-3.png"

function CarouselContent(args) {
    const [index, setIndex] = useState(0);

    const handleSelect = (selectedIndex) => {
        setIndex(selectedIndex);
    };


    return (
            <Carousel activeIndex={index} onSelect={handleSelect}>
                <Carousel.Item>
                    <img className="carousel_background" src={background1} text="First slide" />
                    <Carousel.Caption style={{ alignItems: "normal", height: "50vh" }}>
                        <div className="row container div-inner-carousel" >
                            <div className=" left-carousel">
                                <h2>We picked some <span>cool<br/> things</span> for you!</h2>
                            </div>
                        </div>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img className="carousel_background" src={background2} text="First slide" />
                    <Carousel.Caption style={{ alignItems: "normal", height: "30vh" }}>
                        <div className="row container div-inner-carousel" >
                            <div className="left-carousel text-white ">
                                <h2 className="text-white text-end">We have diverse products</h2>
                                <p className="mt-3 text-end">Some products will make you amazing.</p>
                            </div>

                        </div>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img className="carousel_background" src={background3} text="First slide" />
                    <Carousel.Caption style={{ alignItems: "normal", height: "50vh",width:"100%" }}>
                        <div className="row container div-inner-carousel" >
                            <div className=" left-carousel">
                                <h2 className="text-white">STEP INTO STYLE</h2>
                                <p className="mt-3 text-white">Discover latest our catalog.</p>
                                <button className="btn btn-dark text-white">Go to catalog</button>
                            </div>
                        </div>
                    </Carousel.Caption>
                </Carousel.Item>
                </Carousel>
    )
}
export default CarouselContent