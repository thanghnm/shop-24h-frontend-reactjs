import watch from "../../../assets/images/category/category-1.jpg"
import shoe from "../../../assets/images/category/category-2.jpg"
import clothes from "../../../assets/images/category/category-3.jpg"
import WhatshotIcon from '@mui/icons-material/Whatshot';

function Category() {
    return (
        <div className=" flex-col items-center justify-center mt-5" >
            <div className="flex text-left w-full mt-5 mb-3">
                <WhatshotIcon style={{ color: "red" }} />
                <h3 className="homepage-title">Categories</h3>
            </div>
            <div className="w-9/12 justify-center items-center flex flex-wrap">
                <a href="#" className="col-sm-12 col-md-12 col-lg-4 col-xl-4 relative flex items-center justify-center mb-2 category-div">
                    <img className="category-img" src={watch} alt="watch"/>
                    <p className="category-p">Watch</p>
                </a>
                <a href="#" className="col-sm-12 col-md-12 col-lg-4 col-xl-4 relative flex items-center justify-center mb-2 category-div">
                    <img className="category-img" src={shoe} alt="watch"/>
                    <p className="category-p">Shoe</p>
                </a>
                <a href="#" className="col-sm-12 col-md-12 col-lg-4 col-xl-4 relative flex items-center justify-center mb-2 category-div">
                    <img className="category-img" src={clothes} alt="watch"/>
                    <p className="category-p">Clothing</p>
                </a>
            </div>
        </div>
    )
}
export default Category