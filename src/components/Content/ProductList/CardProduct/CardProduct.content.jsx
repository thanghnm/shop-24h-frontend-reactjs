import { Link } from '@mui/material';
import Card from 'react-bootstrap/Card';
import { useDispatch } from 'react-redux';
import { addCart, closeNoticeAddCart, getInfo } from '../../../../action/productList.action';
import { getProductFromStorage } from '../../../../action/cart.action';
import star from "../../../../assets/images/card/Star 1.png"
const CardProduct = (props) => {

    const dispatch = useDispatch()
    const onBtnGetInfoClick = () => {
        dispatch(getInfo(props.id))
        window.location.href = "/products/" + props.id
    }
    const onBtnAddCartClick = (event) => {
        dispatch(addCart(props.info))
        dispatch(getProductFromStorage())
        setTimeout(()=>{
            dispatch(closeNoticeAddCart())
        },5000)
    }

    return (

        <div className={' card-container col-sm-12 col-md-6 col-lg-4 col-xl-' + props.card}>
            <Card className=' card-div'>
                <Card.Img  className="card-img" variant="top" src={props.image} />
                <Card.Body>
                    <Card.Title className='text-center card-title'>{props.name}</Card.Title>
                    <p className='card-desc'>This long coat is crafted from sumptuous double face cashmere in an understated wrap
                     silhouette with a clean neckline for a contemporary, elegant look.</p>
                    <div className='flex'>
                        <img style={{ marginRight: 5 }} src={star} alt='star' />
                        <img style={{ marginRight: 5 }} src={star} alt='star' />
                        <img style={{ marginRight: 5 }} src={star} alt='star' />
                        <img style={{ marginRight: 5 }} src={star} alt='star' />
                        <img style={{ marginRight: 5 }} src={star} alt='star' />
                    </div>
                    <div className='flex items-center mt-3'>
                        <p className='p-price m-0' >
                            {(props.price).toLocaleString()}$
                        </p>
                        <span className='span-discount pl-5'>{props.discount.toLocaleString()}$</span>
                    </div>
                    <div className='flex justify-around'>
                        <button onClick={onBtnGetInfoClick} className='btn btn-primary ml-[10px]'>Get info</button>
                        {props.quantity > 0 ? <button data-add={props.info} onClick={onBtnAddCartClick} className='btn btn-success'>Add to cart</button> :
                            <button disabled className='btn btn-secondary'>Sold out</button>}
                    </div>
                </Card.Body>
            </Card>
        </div>
    )
}
export default CardProduct