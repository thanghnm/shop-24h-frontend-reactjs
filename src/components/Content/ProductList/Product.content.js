import { useEffect } from "react"
import CardProduct from "./CardProduct/CardProduct.content"
import { closeNoticeAddCart, fetchProduct, fetchUser, pageChangePagination } from "../../../action/productList.action";
import { useDispatch, useSelector } from "react-redux";
import loading from "../../../assets/images/loading.gif"
import { Button, Pagination } from "@mui/material";
import { getProductFromStorage } from "../../../action/cart.action";
import WhatshotIcon from '@mui/icons-material/Whatshot';
import { openAdminCart } from "../../../action/table.action";


const Product = () => {
    const dispatch = useDispatch()
    const { limit, product, pending, noPage, currentPage, userData,noticeAddCart } = useSelector((reduxData) => reduxData.productReducers)
    const { key } = useSelector((reduxData) => reduxData.cartReducers)

    useEffect(() => {
        dispatch(fetchProduct(limit, currentPage))
        dispatch(getProductFromStorage())
        dispatch(fetchUser())
    }, [currentPage, limit])
    const onChangePagination = (event, value) => {
        dispatch(pageChangePagination(value));
    }
    const onBtnProcessToCheckOutClick = () => {
        dispatch(openAdminCart())
    }

    let column;
    if (window.location.pathname === "/products") {
        column = 4
    }
    else {
        column = 3
    }
    return (
        <div className=" flex flex-col justify-around items-center">
            {noticeAddCart?<div id="notice-add-cart">Item added</div>:""}
            {window.location.pathname === "/products" ? "" : <div className="flex text-left w-full mt-5">
                <WhatshotIcon style={{ color: "red" }} />
                <h3  className="homepage-title">Trending</h3>
            </div>}
            <div className=" flex flex-wrap w-full justify-around">
                {pending ? <img src={loading} alt="mygif"></img> : product.map((element, index) => {
                    return (
                        <CardProduct info={element} card={column} key={index} id={element._id} image={element.imageUrl} name={element.name} price={element.buyPrice}
                            discount={element.promotionPrice} quantity={element.amount} />

                    )
                })}
            </div>
            {window.location.pathname === "/products" || window.location.pathname === "/admin/orders" ? <Pagination className="text-center" color="primary"
                count={noPage} page={currentPage} onChange={onChangePagination} variant="outlined" shape="rounded" /> : ""}
            {window.location.pathname === "/" ? <Button style={{ background: "orange", color: "white", marginBottom: 10 }} href="/products">View More</Button> : ""}
            {window.location.pathname === "/admin/orders" ? <Button onClick={onBtnProcessToCheckOutClick} style={{ background: "orange", color: "white", marginBottom: 10 }} className="mt-3">Process to checkout</Button> : []}
        </div>
    )
}
export default Product