import { Checkbox, FormControlLabel, FormGroup, Slider, TextField } from "@mui/material"
import { useDispatch, useSelector } from "react-redux";
import { changeNameSearch, changePriceMax, changePriceMin, changeTypeSearchGlasses, changeTypeSearchHandbag, changeTypeSearchShirt, changeTypeSearchShoe, changeTypeSearchWatch, searchProduct } from "../../../action/productList.action";
const abc = "checked"
const Filter = ({ onChange }) => {
    const dispatch = useDispatch()
    const { slider,checkbox,search,currentPage,limit } = useSelector((reduxData) => reduxData.productReducers);
    function valuetext(value) {
        return `${value}$`;
      }
    const minDistance = 100;
    const handleChange = (
        event,
        newValue,
        activeThumb,
    ) => {
        if (!Array.isArray(newValue)) {
            return;
        }
        if (activeThumb === 0) {
            dispatch(changePriceMin(slider,minDistance,newValue))
        } else {
            dispatch(changePriceMax(slider,minDistance,newValue))
        }
    };
    const onInputSubmitShirtChange =()=>{
        dispatch(changeTypeSearchShirt())
    }
    const onInputSubmitGlassesChange =()=>{
        dispatch(changeTypeSearchGlasses())
    }
    const onInputSubmitHandbagChange =()=>{
        dispatch(changeTypeSearchHandbag())
    }
    const onInputSubmitWatchChange =()=>{
        dispatch(changeTypeSearchWatch())
    }
    const onInputSubmitShoeChange =()=>{
        dispatch(changeTypeSearchShoe())
    }
    const onInputSearchChange = (event)=>{
        dispatch(changeNameSearch(event))
    }
    const onBtnSearchClick =()=>{
        dispatch(searchProduct(slider,checkbox,search))
    }
    return (
        <div className="flex div-filter p-4 ">
            <p className="p-filter" >Price</p>
            <Slider
                getAriaLabel={() => 'Minimum distance'}
                value={slider.value}
                onChange={handleChange}
                getAriaValueText={valuetext}
                min={slider.min}
                max={slider.max}
                disableSwap
                step={100}
                valueLabelDisplay="on"
            />
            <TextField onChange={onInputSearchChange} value={search} className="mt-5" id="outlined-search" label="Nhập tên hàng hóa để tìm kiếm" type="search" />
            <p className="p-filter mt-5" >Category</p>
            <FormGroup>
                <FormControlLabel onChange={onInputSubmitShirtChange} control={<Checkbox checked={checkbox.shirt} />} label="Shirt" />
            </FormGroup>
            <FormGroup>
                <FormControlLabel onChange={onInputSubmitGlassesChange} control={<Checkbox checked={checkbox.glasses} />} label="Glasses" />
            </FormGroup>
            <FormGroup>
                <FormControlLabel onChange={onInputSubmitHandbagChange} control={<Checkbox checked={checkbox.hangbad} />} label="Handbag" />
            </FormGroup>
            <FormGroup>
                <FormControlLabel onChange={onInputSubmitWatchChange} control={<Checkbox checked={checkbox.watch} />} label="Watch" />
            </FormGroup>
            <FormGroup>
                <FormControlLabel onChange={onInputSubmitShoeChange} control={<Checkbox checked={checkbox.shoe} />} label="Shoe" />
            </FormGroup>
            <button onClick={onBtnSearchClick}  className="btn btn-primary mt-5" >Tìm kiếm</button>
        </div >
    )
}
export default Filter