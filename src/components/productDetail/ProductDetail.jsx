import { useDispatch, useSelector } from "react-redux";
import Breadcrumbs from "../Content/Breadcrumbs/Breadcrumbs"
import { useEffect } from "react";
import { fetchProductInfo } from "../../action/productInfo.action";
import { useParams } from "react-router-dom";
import Info from "../Content/ProductInfo/Info.product";
import Description from "../Content/ProductInfo/Desc.product";
import loading from "../../assets/images/loading.gif"
import { getProductFromStorage } from "../../action/cart.action";
import { fetchUser } from "../../action/productList.action";

const ProductDetail = () => {
    const { productId } = useParams()
    const { productData, pending, otherProduct } = useSelector((reduxData) => reduxData.productInfoReducers);
    const { key } = useSelector((reduxData) => reduxData.cartReducers)

    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(getProductFromStorage(),
            dispatch(fetchUser()),
            dispatch(fetchProductInfo(productId)))
    }, [])
    return (
        <div className="w-full p-3">
            <Breadcrumbs thisPage="Detail" parentPage="Products" urlParentPage="/products" />
            {productData ?
                <> <div className="flex justify-center flex-wrap detail-div p-3">
                    <div className="bg-slate-100 col-12 col-sm-12 col-md-5 col-lg-4 col-xl-4 info-img p-2">
                        <img style={{ width: "100%" }} src={productData.imageUrl}></img>
                    </div>
                    <div className="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-4 p-2">
                        <Info name={productData.name} price={productData.promotionPrice.toLocaleString()}
                            amount={productData.amount} data={productData} discount={productData.buyPrice.toLocaleString()} />
                    </div>
                    <div className="col-12 col-sm-6 col-md-3 col-lg-4 col-xl-4 p-2">
                        <Description />
                    </div>

                </div>
                    <div>
                        <h5 className="info-name mt-5">
                            You may also like
                        </h5>
                        <div className="flex justify-around gap-4 flex-wrap ">
                            {otherProduct ?
                                otherProduct.map((element, index) => {
                                    return (
                                        <div key={index} className="flex items-center col-sm-12 col-md-4 col-lg-3 col-xl-3 ">
                                            <div className="col-md-9 col-lg-9 col-xl-9">
                                                <img className="other-img" src={element.imageUrl} alt="other" />
                                            </div>
                                            <div className="col-md-3 col-lg-3 col-xl-3 mr-3 p-2 other-p ">
                                                <a href={`/products/` + element._id} className="info-name other-product-name">{element.name}</a>
                                                <p className="other-price">{element.promotionPrice.toLocaleString()}$</p>
                                            </div>
                                        </div>
                                    )
                                }) : <img src={loading} alt="mygif"></img>}
                        </div>
                    </div>
                </> : ""}

        </div>

    )
}
export default ProductDetail