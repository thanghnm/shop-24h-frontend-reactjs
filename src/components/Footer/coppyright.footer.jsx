const CopyRight = () => {
    return (
        <div className="row"  style={{position:"relative",margin:0,left:0,bottom:"0px",background:"black",width:"100%"}}>
            <div className="col-lg-12 text-center">
                <div className="footer__copyright__text">
                    <p>Copyright ©
                        <script>
                            document.write(new Date().getFullYear());
                        </script>2020
                        All rights reserved | This template is made with <i className="fa fa-heart-o"
                            aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                    </p>
                </div>
            </div>
        </div>
    )
}
export default CopyRight