import {
    ADMIN_LOGIN_SUCCESS,
    ADMIN_LOGIN_UNAUTHORIZE,
    ADMIN_PRODUCT_ADD_BUYPRICE_INVALID,
    ADMIN_PRODUCT_ADD_IMAGE_INVALID,
    ADMIN_PRODUCT_ADD_MODAL_AMOUNT_CHANGE,
    ADMIN_PRODUCT_ADD_MODAL_BUYPRICE_CHANGE,
    ADMIN_PRODUCT_ADD_MODAL_DESC_CHANGE,
    ADMIN_PRODUCT_ADD_MODAL_NAME_CHANGE,
    ADMIN_PRODUCT_ADD_MODAL_PROMOTIONPRICE_CHANGE,
    ADMIN_PRODUCT_ADD_MODAL_TYPE_CHANGE,
    ADMIN_PRODUCT_ADD_MODAL_URL_CHANGE,
    ADMIN_PRODUCT_ADD_NAME_INVALID,
    ADMIN_PRODUCT_ADD_PROMOTIONPRICE_INVALID,
    ADMIN_PRODUCT_ADD_TYPE_INVALID,
    ADMIN_PRODUCT_CREATE_ERROR,
    ADMIN_PRODUCT_CREATE_PENDING,
    ADMIN_PRODUCT_CREATE_SUCCESS,
    ADMIN_PRODUCT_DELETE_FAIL,
    ADMIN_PRODUCT_DELETE_SUCCESS,
    ADMIN_PRODUCT_EDIT_BUYPRICE_INVALID,
    ADMIN_PRODUCT_EDIT_IMAGE_INVALID,
    ADMIN_PRODUCT_EDIT_MODAL_AMOUNT_CHANGE,
    ADMIN_PRODUCT_EDIT_MODAL_BUYPRICE_CHANGE,
    ADMIN_PRODUCT_EDIT_MODAL_DESC_CHANGE,
    ADMIN_PRODUCT_EDIT_MODAL_NAME_CHANGE,
    ADMIN_PRODUCT_EDIT_MODAL_PROMOTIONPRICE_CHANGE,
    ADMIN_PRODUCT_EDIT_MODAL_TYPE_CHANGE,
    ADMIN_PRODUCT_EDIT_MODAL_URL_CHANGE,
    ADMIN_PRODUCT_EDIT_NAME_INVALID,
    ADMIN_PRODUCT_EDIT_PROMOTIONPRICE_INVALID,
    ADMIN_PRODUCT_EDIT_TYPE_INVALID,
    ADMIN_PRODUCT_FETCH_PENDING,
    ADMIN_PRODUCT_FETCH_SUCCESS,
    ADMIN_PRODUCT_FILT_MORE,
    ADMIN_PRODUCT_FILT_SUCCESS,
    ADMIN_PRODUCT_INPUT_FILTER_CHANGE,
    ADMIN_PRODUCT_OPEN_ADD_MODAL,
    ADMIN_PRODUCT_OPEN_DELETE_MODAL,
    ADMIN_PRODUCT_OPEN_EDIT_MODAL,
    ADMIN_PRODUCT_SELECT_FILTER_CHANGE,
    ADMIN_PRODUCT_UPDATE_ERROR,
    ADMIN_PRODUCT_UPDATE_PENDING,
    ADMIN_PRODUCT_UPDATE_SUCCESS,
    ADMIN_TYPE_FETCH_SUCCESS,
    CLOSE_MODAL, CREATE_ORDER_SUCCESS, CUSTOMER_ADD_EMAIL_INVALID, CUSTOMER_ADD_FULLNAME_INVALID, CUSTOMER_ADD_MODAL_ADDRESS_CHANGE,
    CUSTOMER_ADD_MODAL_CITY_CHANGE, CUSTOMER_ADD_MODAL_COUNTRY_CHANGE, CUSTOMER_ADD_MODAL_EMAIL_CHANGE,
    CUSTOMER_ADD_MODAL_FULLNAME_CHANGE, CUSTOMER_ADD_MODAL_PHONE_CHANGE, CUSTOMER_ADD_PHONE_INVALID, CUSTOMER_CREATE_ERROR,
    CUSTOMER_CREATE_PENDING, CUSTOMER_CREATE_SUCCESS, CUSTOMER_DELETE_ERROR, CUSTOMER_DELETE_PENDING, CUSTOMER_DELETE_SUCCESS,
    CUSTOMER_EDIT_MODAL_ADDRESS_CHANGE, CUSTOMER_EDIT_MODAL_CITY_CHANGE, CUSTOMER_EDIT_MODAL_COUNTRY_CHANGE,
    CUSTOMER_EDIT_MODAL_EMAIL_CHANGE, CUSTOMER_EDIT_MODAL_FULLNAME_CHANGE, CUSTOMER_EDIT_MODAL_PHONE_CHANGE, CUSTOMER_FETCH_ERROR,
    CUSTOMER_FETCH_PENDING, CUSTOMER_FETCH_SUCCESS, CUSTOMER_FILT_MORE, CUSTOMER_FILT_SUCCESS, CUSTOMER_INPUT_FILTER_CHANGE, CUSTOMER_OPEN_ADD_MODAL, CUSTOMER_OPEN_DELETE_MODAL, CUSTOMER_OPEN_EDIT_MODAL,
    CUSTOMER_PAGE_CHANGE, CUSTOMER_SELECT_FILTER_CHANGE, CUSTOMER_UPDATE_ERROR, CUSTOMER_UPDATE_PENDING, CUSTOMER_UPDATE_SUCCESS, DATE_PICKER_CHANGE, ORDER_ADD_ADDRESS_INVALID, ORDER_ADD_EMAIL_INVALID, ORDER_ADD_FULLNAME_INVALID, ORDER_ADD_PHONE_INVALID, ORDER_ADD_SHIPDATE_INVALID, ORDER_DELETE_SUCCESS, ORDER_EDIT_MODAL_NOTE_CHANGE, ORDER_EDIT_SHIPDATE_CHANGE, ORDER_FETCH_PENDING, ORDER_FETCH_SUCCESS, ORDER_FILT_MORE, ORDER_FILT_SUCCESS, ORDER_INPUT_FILTER_CHANGE, ORDER_OPEN_ADD_CART, ORDER_OPEN_ADD_PRODUCTLIST, ORDER_OPEN_DELETE_MODAL, ORDER_OPEN_EDIT_CART, ORDER_OPEN_EDIT_MODAL, ORDER_OPEN_EDIT_PRODUCTLIST, ORDER_SELECT_FILTER_CHANGE, ORDER_UPDATE_SUCCESS
} from "../constants/constants";

const initialState = {
    limit: 8,
    currentPage: 1,
    customer: [],
    search: "",
    openModalAddCustomer: false,
    openCreateCustomerSuccessModal: false,
    openModalEditCustomer: false,
    openUpdateCustomerSuccessModal: false,
    openModalDeleteCustomer: false,
    openDeleteCustomerSuccessModal: false,
    openModalAddProduct: false,
    openCreateProductSuccessModal: false,
    openModalEditProduct: false,
    openUpdateProductSuccessModal: false,
    openModalDeleteProduct: false,
    openDeleteProductSuccessModal: false,
    openModalDeleteProductFailModal: false,
    openModalEditOrder: false,
    openUpdateOrderSuccessModal: false,
    openModalDeleteOrder: false,
    openDeleteOrderSuccessModal: false,
    addCustomer: {
        fullName: "",
        phone: "",
        email: "",
        address: "",
        city: "",
        country: ""
    },
    editCustomer: {
        fullName: "",
        phone: "",
        email: "",
        address: "",
        city: "",
        country: ""
    },
    selectFilterOrderValue: ["", "", ""],
    inputFilterOrderValue: ["", "", "",],
    selectFilterCustomerValue: ["", "", ""],
    inputFilterCustomerValue: ["", "", "",],
    selectFilterProductValue: ["", "", "", ""],
    inputFilterProductValue: ["", "", "", ""],
    addProduct: {
        name: "",
        type: "0",
        buyPrice: "",
        promotionPrice: "",
        description: "",
        amount: "",
        imageUrl: ""
    },
    editProduct: {
        name: "",
        type: "0",
        buyPrice: "",
        promotionPrice: "",
        description: "",
        amount: "",
        imageUrl: ""
    },
    editProductId: null,
    editOrder: {
        note: "",
        cost: 0,
        orderDetails: [],
        shippedDate: null
    },
    filterOrderArray: [""],
    filterProductArray: [""],
    filterCustomerArray: [""],
    refProduct:null,
    refCart:null
}
const tableReducer = (state = initialState, action) => {
    switch (action.type) {
        case CUSTOMER_FETCH_PENDING:
            state.pending = true
            state.updateTable = false
            break;
        case CUSTOMER_FETCH_SUCCESS:
            state.customer = action.data.result
            state.noPage = Math.ceil(action.total / state.limit);
            state.pending = false
            break;
        case CUSTOMER_FETCH_ERROR:
            break;
        case CUSTOMER_OPEN_ADD_MODAL:
            state.openModalAddCustomer = true
            break;
        case CLOSE_MODAL:
            state.openModalAddCustomer = false
            state.openCreateCustomerSuccessModal = false
            state.openModalEditCustomer = false
            state.openUpdateCustomerSuccessModal = false
            state.openModalDeleteCustomer = false
            state.openDeleteCustomerSuccessModal = false
            state.openModalAddProduct = false
            state.openCreateProductSuccessModal = false
            state.openModalEditProduct = false
            state.openUpdateProductSuccessModal = false
            state.openModalDeleteProduct = false
            state.openDeleteProductSuccessModal = false
            state.openModalDeleteProductFailModal = false
            state.openModalEditOrder = false
            state.openUpdateOrderSuccessModal = false
            state.openModalDeleteOrder = false
            state.openDeleteOrderSuccessModal = false
            state.openUnauthorizeModal = false
            break;
        case CUSTOMER_ADD_MODAL_FULLNAME_CHANGE:
            state.addCustomer.fullName = action.payload
            state.addCustomerFullnameAlertDisplay = "none"
            break;
        case CUSTOMER_ADD_MODAL_EMAIL_CHANGE:
            state.addCustomer.email = action.payload
            state.addCustomerEmailAlertDisplay = "none"
            break;
        case CUSTOMER_ADD_MODAL_PHONE_CHANGE:
            state.addCustomer.phone = action.payload
            state.addCustomerPhoneAlertDisplay = "none"
            break;
        case CUSTOMER_ADD_MODAL_ADDRESS_CHANGE:
            state.addCustomer.address = action.payload
            break;
        case CUSTOMER_ADD_MODAL_CITY_CHANGE:
            state.addCustomer.city = action.payload
            break;
        case CUSTOMER_ADD_MODAL_COUNTRY_CHANGE:
            state.addCustomer.country = action.payload
            break;
        case CUSTOMER_CREATE_PENDING:
            state.createCustomerPending = true
            break;
        case CUSTOMER_CREATE_SUCCESS:
            state.createCustomerPending = false
            state.openCreateCustomerSuccessModal = true
            state.updateTable = true
            break;
        case CUSTOMER_CREATE_ERROR:
            break;
        case CUSTOMER_OPEN_EDIT_MODAL:
            state.openModalEditCustomer = true
            state.customerId = action.customerId
            state.editCustomer = action.payload
            break;
        case CUSTOMER_EDIT_MODAL_FULLNAME_CHANGE:
            state.editCustomer.fullName = action.payload
            break;
        case CUSTOMER_EDIT_MODAL_EMAIL_CHANGE:
            state.editCustomer.email = action.payload
            break;
        case CUSTOMER_EDIT_MODAL_PHONE_CHANGE:
            state.editCustomer.phone = action.payload
            break;
        case CUSTOMER_EDIT_MODAL_ADDRESS_CHANGE:
            state.editCustomer.address = action.payload
            break;
        case CUSTOMER_EDIT_MODAL_CITY_CHANGE:
            state.editCustomer.city = action.payload
            break;
        case CUSTOMER_EDIT_MODAL_COUNTRY_CHANGE:
            state.editCustomer.country = action.payload
            break;
        case CUSTOMER_UPDATE_PENDING:
            state.updateCustomerPending = true
            break;
        case CUSTOMER_UPDATE_SUCCESS:
            state.updateCustomerPending = false
            state.openUpdateCustomerSuccessModal = true
            state.updateTable = true
            break;
        case CUSTOMER_UPDATE_ERROR:
            break;
        case CUSTOMER_PAGE_CHANGE:
            state.currentPage = action.page
            state.updateTable = true
            break;
        case CUSTOMER_OPEN_DELETE_MODAL:
            state.openModalDeleteCustomer = true
            state.customerId = action.customerId
            break;
        case CUSTOMER_DELETE_PENDING:
            state.deleteCustomerPending = true
            break;
        case CUSTOMER_DELETE_SUCCESS:
            state.deleteCustomerPending = false
            state.openDeleteCustomerSuccessModal = true
            state.updateTable = true
            break;
        case CUSTOMER_DELETE_ERROR:
            break;
        case CUSTOMER_ADD_FULLNAME_INVALID:
            state.addCustomerFullnameAlertDisplay = "block"
            state.addCustomerFullnameAlertMessage = action.message
            break;
        case CUSTOMER_ADD_EMAIL_INVALID:
            state.addCustomerEmailAlertDisplay = "block"
            state.addCustomerEmailAlertMessage = action.message
            break;
        case CUSTOMER_ADD_PHONE_INVALID:
            state.addCustomerPhoneAlertDisplay = "block"
            state.addCustomerPhoneAlertMessage = action.message
            break;
        case CUSTOMER_INPUT_FILTER_CHANGE:
            state.inputFilterCustomerValue[action.key] = action.payload
            break;
        case CUSTOMER_SELECT_FILTER_CHANGE:
            state.selectFilterCustomerValue[action.key] = action.payload
            break;
        case CUSTOMER_FILT_SUCCESS:
            state.customer = action.payload
            state.noPage = Math.ceil(action.payload.length / state.limit);
            break;
        case CUSTOMER_FILT_MORE:
            if (state.filterCustomerArray.length < 3) {
                state.filterCustomerArray.push("")
            }
            break;
        //ORDER REDUCER
        case ORDER_FETCH_PENDING:
            state.updateTable = false
            break;
        case ORDER_FETCH_SUCCESS:
            state.updateTableOrder = false
            state.order = action.data.result
            state.noPage = Math.ceil(action.total / state.limit);
            break;
        case ORDER_OPEN_ADD_PRODUCTLIST:
            state.editCart = false
            state.productList = true
            state.openCart = false
            break;
        case ORDER_OPEN_ADD_CART:
            state.productList = false
            state.openCart = true
            break;
        case CREATE_ORDER_SUCCESS:
            state.updateTable = true
            break;
        case ORDER_OPEN_EDIT_CART:
            state.orderIdEdit = action.payload
            state.editOrder.note = action.note
            state.editOrder.shippedDate = action.shippedDate
            state.editCart = true
            state.productList = false
            state.openCart = true
            break;
        case ORDER_OPEN_EDIT_PRODUCTLIST:
            state.openCart = false
            state.productList = true
            break;
        case ORDER_OPEN_EDIT_MODAL:
            state.openModalEditOrder = true
            break;
        case ORDER_EDIT_MODAL_NOTE_CHANGE:
            state.editOrder.note = action.payload
            break;
        case ORDER_EDIT_SHIPDATE_CHANGE:
            state.editOrder.shippedDate = action.payload
            break;
        case ORDER_UPDATE_SUCCESS:
            state.editCart = false
            state.openCart = false
            state.updateTable = true
            state.updateTableOrder = true
            state.openUpdateOrderSuccessModal = true
            break;
        case ORDER_OPEN_DELETE_MODAL:
            state.orderIdDelete = action.payload
            state.openModalDeleteOrder = true
            break;
        case ORDER_DELETE_SUCCESS:
            state.updateTableOrder = true
            state.openDeleteOrderSuccessModal = true
            break;
        case ORDER_SELECT_FILTER_CHANGE:
            state.selectFilterOrderValue[action.key] = action.payload
            break;
        case ORDER_INPUT_FILTER_CHANGE:
            state.inputFilterOrderValue[action.key] = action.payload
            break;
        case ORDER_FILT_MORE:
            if (state.filterOrderArray.length < 2) {
                state.filterOrderArray.push("")
            }
            case ORDER_FILT_SUCCESS:
                state.order = action.payload
                state.noPage = Math.ceil(action.payload.length / state.limit);
                break;
            break;
        // PRODUCT REDUCER
        case ADMIN_PRODUCT_FETCH_PENDING:
            state.updateTable = false
            break;
        case ADMIN_PRODUCT_FETCH_SUCCESS:
            state.products = action.data
            state.noPage = Math.ceil(action.total / state.limit);
            break;
        case ADMIN_TYPE_FETCH_SUCCESS:
            state.types = action.payload
            break;
        case ADMIN_PRODUCT_ADD_MODAL_NAME_CHANGE:
            state.addProduct.name = action.payload
            state.addProductNameAlertDisplay = "none"
            break;
        case ADMIN_PRODUCT_ADD_MODAL_BUYPRICE_CHANGE:
            state.addProduct.buyPrice = action.payload
            state.addProductBuyPriceAlertDisplay = "none"
            break;
        case ADMIN_PRODUCT_ADD_MODAL_PROMOTIONPRICE_CHANGE:
            state.addProductPromotionPriceAlertDisplay = "none"
            state.addProduct.promotionPrice = action.payload
            break;
        case ADMIN_PRODUCT_ADD_MODAL_DESC_CHANGE:
            state.addProduct.description = action.payload
            break;
        case ADMIN_PRODUCT_ADD_MODAL_AMOUNT_CHANGE:
            state.addProduct.amount = action.payload
            break;
        case ADMIN_PRODUCT_ADD_MODAL_TYPE_CHANGE:
            state.addProduct.type = action.payload
            state.addProductTypeAlertDisplay = "none"
            break;
        case ADMIN_PRODUCT_ADD_MODAL_URL_CHANGE:
            state.addProductImageUrlAlertDisplay = "none"
            state.addProduct.imageUrl = action.payload
            break;
        case ADMIN_PRODUCT_OPEN_ADD_MODAL:
            state.openModalAddProduct = true
            break;
        case ADMIN_PRODUCT_ADD_NAME_INVALID:
            state.addProductNameAlertDisplay = "block"
            state.addProductNameAlertMessage = action.message
            break;
        case ADMIN_PRODUCT_ADD_TYPE_INVALID:
            state.addProductTypeAlertDisplay = "block"
            state.addProductTypeAlertMessage = action.message
            break;
        case ADMIN_PRODUCT_ADD_BUYPRICE_INVALID:
            state.addProductBuyPriceAlertDisplay = "block"
            state.addProductBuyPriceAlertMessage = action.message
            break;
        case ADMIN_PRODUCT_ADD_PROMOTIONPRICE_INVALID:
            state.addProductPromotionPriceAlertDisplay = "block"
            state.addProductPromotionPriceAlertMessage = action.message
            break;
        case ADMIN_PRODUCT_ADD_IMAGE_INVALID:
            state.addProductImageUrlAlertDisplay = "block"
            state.addProductImageUrlAlertMessage = action.message
            break;
        case ADMIN_PRODUCT_CREATE_PENDING:
            state.createProductPending = true
            break;
        case ADMIN_PRODUCT_CREATE_SUCCESS:
            state.createProductPending = false
            state.openCreateProductSuccessModal = true
            state.updateTable = true
            break;
        case ADMIN_PRODUCT_CREATE_ERROR:
            break;
        case ADMIN_PRODUCT_OPEN_EDIT_MODAL:
            state.editProduct = action.payload
            state.editProductId = action.productId
            state.openModalEditProduct = true
            break;
        case ADMIN_PRODUCT_EDIT_MODAL_NAME_CHANGE:
            state.editProduct.name = action.payload
            state.editProductNameAlertDisplay = "none"
            break;
        case ADMIN_PRODUCT_EDIT_MODAL_BUYPRICE_CHANGE:
            state.editProduct.buyPrice = action.payload
            state.editProductBuyPriceAlertDisplay = "none"
            break;
        case ADMIN_PRODUCT_EDIT_MODAL_PROMOTIONPRICE_CHANGE:
            state.editProductPromotionPriceAlertDisplay = "none"
            state.editProduct.promotionPrice = action.payload
            break;
        case ADMIN_PRODUCT_EDIT_MODAL_DESC_CHANGE:
            state.editProduct.description = action.payload
            break;
        case ADMIN_PRODUCT_EDIT_MODAL_AMOUNT_CHANGE:
            state.editProduct.amount = action.payload
            break;
        case ADMIN_PRODUCT_EDIT_MODAL_TYPE_CHANGE:
            state.editProduct.type = action.payload
            state.editProductTypeAlertDisplay = "none"
            break;
        case ADMIN_PRODUCT_EDIT_MODAL_URL_CHANGE:
            state.editProductImageUrlAlertDisplay = "none"
            state.editProduct.imageUrl = action.payload
            break;
        case ADMIN_PRODUCT_EDIT_NAME_INVALID:
            state.editProductNameAlertDisplay = "block"
            state.editProductNameAlertMessage = action.message
            break;
        case ADMIN_PRODUCT_EDIT_TYPE_INVALID:
            state.editProductTypeAlertDisplay = "block"
            state.editProductTypeAlertMessage = action.message
            break;
        case ADMIN_PRODUCT_EDIT_BUYPRICE_INVALID:
            state.editProductBuyPriceAlertDisplay = "block"
            state.editProductBuyPriceAlertMessage = action.message
            break;
        case ADMIN_PRODUCT_EDIT_PROMOTIONPRICE_INVALID:
            state.editProductPromotionPriceAlertDisplay = "block"
            state.editProductPromotionPriceAlertMessage = action.message
            break;
        case ADMIN_PRODUCT_EDIT_IMAGE_INVALID:
            state.editProductImageUrlAlertDisplay = "block"
            state.editProductImageUrlAlertMessage = action.message
            break;
        case ADMIN_PRODUCT_UPDATE_PENDING:
            state.updateProductPending = true
            break;
        case ADMIN_PRODUCT_UPDATE_SUCCESS:
            state.updateProductPending = false
            state.openUpdateProductSuccessModal = true
            state.updateTable = true
            break;
        case ADMIN_PRODUCT_UPDATE_ERROR:
            break;
        case ADMIN_PRODUCT_OPEN_DELETE_MODAL:
            state.deleteProductId = action.productId
            state.openModalDeleteProduct = true
            break;
        case ADMIN_PRODUCT_DELETE_FAIL:
            state.openModalDeleteProductFailModal = true
            state.failMessage = action.message
            break;
        case ADMIN_PRODUCT_DELETE_SUCCESS:
            state.openDeleteProductSuccessModal = true
            state.updateTable = true
            break;
        case ADMIN_PRODUCT_SELECT_FILTER_CHANGE:
            state.selectFilterProductValue[action.key] = action.payload
            break;
        case ADMIN_PRODUCT_INPUT_FILTER_CHANGE:
            state.inputFilterProductValue[action.key] = action.payload
            break;
        case ADMIN_PRODUCT_FILT_MORE:
            if (state.filterProductArray.length < 4) {
                state.filterProductArray.push("")
            }
            break;
        case ADMIN_PRODUCT_FILT_SUCCESS:
            state.products = action.payload
            state.noPage = Math.ceil(action.payload.length / state.limit);
            break;
        case ADMIN_LOGIN_SUCCESS:
            state.authorize = action.payload
            break;
        case ADMIN_LOGIN_UNAUTHORIZE:
            state.authorize = action.payload
            break;
        default:
            break;
    }
    return { ...state }
}
export default tableReducer