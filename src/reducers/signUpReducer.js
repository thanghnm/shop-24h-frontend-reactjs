import { CLOSE_MODAL, SIGNUP_CHANGE_CONFIRMPASSWORD, SIGNUP_CHANGE_EMAIL, SIGNUP_CHANGE_PASSWORD, SIGNUP_CHANGE_PHONE, SIGNUP_CHANGE_USERNAME, SIGNUP_CONFIRM_PASSWORD_INVALID, SIGNUP_EMAIL_INVALID, SIGNUP_PASSWORD_INVALID, SIGNUP_PHONE_INVALID, SIGNUP_USERNAME_INVALID, SIGN_UP_BAD_REQUEST, SIGN_UP_ERROR, SIGN_UP_SUCCESS } from "../constants/constants";

const initialState = {
    signUp: {
        username: "",
        password: "",
        confirmPassword: "",
        email: "",
        phone: ""
    },
    displayUsernameAlert: "none"

}
const signUpReducer = (state = initialState, action) => {
    switch (action.type) {
        case SIGNUP_CHANGE_USERNAME:
            state.signUp.username = action.payload
            state.displayUsernameAlert = "none"
            break;
        case SIGNUP_CHANGE_PASSWORD:
            state.signUp.password = action.payload
            state.displayPasswordAlert = "none"
            break;
        case SIGNUP_CHANGE_CONFIRMPASSWORD:
            state.signUp.confirmPassword = action.payload
            state.displayConfirmPasswordAlert = "none"
            break;
        case SIGNUP_CHANGE_EMAIL:
            state.signUp.email = action.payload
            state.displayEmailAlert = "none"

            break;
        case SIGNUP_CHANGE_PHONE:
            state.signUp.phone = action.payload
            state.displayPhoneAlert = "none"
            break;
        case SIGNUP_USERNAME_INVALID:
            state.usernameInvalidMessage = action.message
            state.displayUsernameAlert = "block"
            break;
        case SIGNUP_EMAIL_INVALID:
            state.emailInvalidMessage = action.message
            state.displayEmailAlert = "block"
            break;
        case SIGNUP_PHONE_INVALID:
            state.phoneInvalidMessage = action.message
            state.displayPhoneAlert = "block"
            break;
        case SIGNUP_PASSWORD_INVALID:
            state.passwordInvalidMessage = action.message
            state.displayPasswordAlert = "block"
            break;
        case SIGNUP_CONFIRM_PASSWORD_INVALID:
            state.confirmPasswordInvalidMessage = action.message
            state.displayConfirmPasswordAlert = "block"
            break;
        case SIGN_UP_SUCCESS:
            state.signUpSuccess = true
            window.location.href = "/login"
            break;
        case SIGN_UP_BAD_REQUEST:
            break;
        case SIGN_UP_ERROR:
        case CLOSE_MODAL:
            state.signUpSuccess = false
            break;
        default:
            break;
    }
    return { ...state }
}
export default signUpReducer