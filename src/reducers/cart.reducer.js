import { useSelector } from "react-redux"
import {
    CHECK_OUT, CLOSE_MODAL, CLOSE_NOTICE_PLUS, CREATE_ORDER_SUCCESS, DATE_PICKER_CHANGE, FETCH_COUNTRY_SUCCESS,
    GET_PRODUCT_FROM_STORAGE, GET_USER_DATA, INPUT_CHECKOUT_ADDRESS_CHANGE, SELECT_CHECKOUT_CITY_CHANGE, SELECT_CHECKOUT_COUNTRY_CHANGE,
    INPUT_CHECKOUT_EMAIL_CHANGE, INPUT_CHECKOUT_NAME_CHANGE, INPUT_CHECKOUT_NOTE_CHANGE, INPUT_CHECKOUT_PHONE_CHANGE, NOTE_SIGN_IN, ORDER_ADD_ADDRESS_INVALID, ORDER_ADD_EMAIL_INVALID, ORDER_ADD_FULLNAME_INVALID, ORDER_ADD_PHONE_INVALID, ORDER_ADD_SHIPDATE_INVALID, ORDER_OPEN_EDIT_CART, QUANTITY_PLUS, QUANTITY_SUBTRACT, REMOVE_ITEM, UPDATE_CART, VALIDATE_PRODUCT_SUCCESS, PAYMENT_CREDIT, PAYMENT_BANK
} from "../constants/constants"

const initialState = {
    quantity: [],
    priceArr: [],
    totalPrice: 0,
    defaultQty: [],
    openModalCheckOut: false,
    openModalSignIn: false,
    openModalCreateOrderSuccess: false,
    outOfStock: false,
    payment: {
        credit: false,
        bank: false
    },
    checkOut: {
        fullName: "",
        email: "",
        phone: "",
        address: "",
        city: "",
        country: "",
        note: "",
        shippedDate: undefined,
        cost: 0
    }
}
const cartReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_PRODUCT_FROM_STORAGE:
            state.priceArr = []
            state.totalPrice = 0
            state.noticeOutOfStore = []
            state.productObject = action.payload
            state.priceDefault = action.price
            const idProductArr = []
            // Neu gio hang` co item thi push id vao arr de validate
            if (state.productObject && state.productObject.length > 0) {
                state.productObject.map((element, index) => {
                    idProductArr.push(element._id)
                })
            }
            state.outOfStock = false
            state.idProductArr = idProductArr
            // Neu gio hang co item thi push price vao arr
            if (state.priceDefault && state.priceDefault.length > 0) {
                state.priceDefault.map((element, index) => {
                    state.priceArr.push(element * state.productObject[index].quantity)
                    //push notice cua tat ca obj false
                    state.noticeOutOfStore.push(false)
                })
            }
            // Neu gio hang co item thi tinh tong tien
            if (state.priceDefault && state.priceDefault.length > 0) {
                state.totalPrice = state.priceArr.reduce((accumulator, currentValue) => accumulator + currentValue)
            }
            state.key = action.keyItem
            state.update = false
            break;
        case QUANTITY_PLUS:
            // so sanh voi validate data 
            // Neu so luong + lon hon validate thi` tra lai validate va hien notice
           
             if (state.productObject[action.payload].quantity >= state.validateData[action.payload].amount) {
                state.productObject[action.payload].quantity = state.productObject[action.payload].quantity
                state.noticeOutOfStore[action.payload] = true
            }
            else {
                //++
                state.productObject[action.payload].quantity += 1
                if (window.location.pathname === "/admin/orders") {
                    localStorage.setItem("adminCartItems", JSON.stringify(state.productObject))
                }
                else {
                    localStorage.setItem("cartItems", JSON.stringify(state.productObject))
                }
                // ++ Tien` vao` price arr voi so luong moi'
                state.priceArr[action.payload] = state.productObject[action.payload].quantity * state.priceDefault[action.payload]
                // Tinh tong tien sau do'
                if (state.priceDefault.length > 0) {
                    state.totalPrice = state.priceArr.reduce((accumulator, currentValue) => accumulator + currentValue)
                }
            }
            break;
        case QUANTITY_SUBTRACT:
            // Neu - be hon 1 thi` tra lai gia tri =1
            if (state.productObject[action.payload].quantity < 2) {
                state.productObject[action.payload].quantity = 1
            }
            else {
                //--
                state.productObject[action.payload].quantity -= 1
                if (window.location.pathname === "/admin/orders") {
                    localStorage.setItem("adminCartItems", JSON.stringify(state.productObject))
                }
                else {
                    localStorage.setItem("cartItems", JSON.stringify(state.productObject))
                }
                // Tinh toan lai priceArr theo so luong moi
                state.priceArr[action.payload] = state.productObject[action.payload].quantity * state.priceDefault[action.payload]
                //Tinh tong tien
                if (state.priceDefault.length > 0) {
                    state.totalPrice = state.priceArr.reduce((accumulator, currentValue) => accumulator + currentValue)
                }
            }
            break;
        case REMOVE_ITEM:
            // Neu o admin order thi xoa item key adminCartItems
            if (window.location.pathname === "/admin/orders") {
                const itemArr = JSON.parse(localStorage.getItem("adminCartItems"))
                itemArr.splice(action.payload, 1)
                localStorage.setItem("adminCartItems", JSON.stringify(itemArr))
            }
            // else key cartItems
            else {
                const itemArr = JSON.parse(localStorage.getItem("cartItems"))
                itemArr.splice(action.payload, 1)
                localStorage.setItem("cartItems", JSON.stringify(itemArr))
            }
            state.update = true
            state.priceArr = []
            // Get lai price Arr sau khi xoa item
            state.priceDefault.map((element, index) => {
                state.priceArr.push(element)
            })
            // Tinh tong tien
            if (state.priceArr.length > 0) {
                state.totalPrice = state.priceArr.reduce((accumulator, currentValue) => accumulator + currentValue)
            }
            state.quantity = []
            state.defaultQty.map((element, index) => {
                state.quantity.push(element)
            })
            break;
        // Sau 5s tu an notice
        case CLOSE_NOTICE_PLUS:
            state.noticeOutOfStore[action.payload] = false
            break;
        case CHECK_OUT:
            state.openModalCheckOut = true
            state.checkOut.email = action.email
            state.checkOut.phone = action.phone
            state.checkOut.cost = state.totalPrice
            break;
        case NOTE_SIGN_IN:
            state.openModalSignIn = true
            break;
        case CLOSE_MODAL:
            state.openModalSignIn = false
            state.openModalCheckOut = false
            if (state.openModalCreateOrderSuccess === true) {
                state.openModalCreateOrderSuccess = false
                window.location.reload()
            }
            break;
        case INPUT_CHECKOUT_NAME_CHANGE:
            state.checkOut.fullName = action.payload
            state.addOrderFullnameAlertDisplay = "none"
            break;
        case INPUT_CHECKOUT_EMAIL_CHANGE:
            state.checkOut.email = action.payload
            state.addOrderEmailAlertDisplay = "none"
            break;
        case INPUT_CHECKOUT_PHONE_CHANGE:
            state.checkOut.phone = action.payload
            state.addOrderPhoneAlertDisplay = "none"
            break;
        case SELECT_CHECKOUT_COUNTRY_CHANGE:
            state.checkOut.country = action.payload
            state.city = action.city
            break;
        case SELECT_CHECKOUT_CITY_CHANGE:
            state.checkOut.city = action.payload
            break;
        case INPUT_CHECKOUT_ADDRESS_CHANGE:
            state.checkOut.address = action.payload
            state.addOrderAddressAlertDisplay = "none"
            break;
        case INPUT_CHECKOUT_NOTE_CHANGE:
            state.checkOut.note = action.payload
            break;
        case DATE_PICKER_CHANGE:
            state.checkOut.shippedDate = action.payload
            state.addOrderShippedDateAlertDisplay = "none"
            break;
        case VALIDATE_PRODUCT_SUCCESS:
            state.validateData = action.payload
            state.validateData.map((element, index) => {
                if (element.amount == 0) {
                    state.outOfStock = true
                }
            })
            break;
        case ORDER_OPEN_EDIT_CART:
            state.update = true
            break;
        case ORDER_ADD_FULLNAME_INVALID:
            state.addOrderFullnameAlertDisplay = "block"
            state.addOrderFullnameAlertMessage = action.message
            break;
        case ORDER_ADD_EMAIL_INVALID:
            state.addOrderEmailAlertDisplay = "block"
            state.addOrderEmailAlertMessage = action.message
            break;
        case ORDER_ADD_PHONE_INVALID:
            state.addOrderPhoneAlertDisplay = "block"
            state.addOrderPhoneAlertMessage = action.message
            break;
        case ORDER_ADD_ADDRESS_INVALID:
            state.addOrderAddressAlertDisplay = "block"
            state.addOrderAddressAlertMessage = action.message
            break;
        case ORDER_ADD_SHIPDATE_INVALID:
            state.addOrderShippedDateAlertDisplay = "block"
            state.addOrderShippedDateAlertMessage = action.message
            break;
        case CREATE_ORDER_SUCCESS:
            state.openModalCreateOrderSuccess = true
            localStorage.removeItem("cartItems")
            localStorage.removeItem("adminCartItems")
            break;
        case FETCH_COUNTRY_SUCCESS:
            state.countryAsia = action.payload
            break;
        case PAYMENT_CREDIT:
            state.payment.credit = true
            state.payment.bank = false
            break;
        case PAYMENT_BANK:
            state.payment.bank = true
            state.payment.credit = false
            break;
        default:
            break;
    }
    return { ...state }
}
export default cartReducer