import { ADD_CART, CLOSE_NOTICE_ADDCART, GET_INFO, GET_PRODUCT_FROM_STORAGE, GET_USER_DATA,
     ORDER_OPEN_ADD_PRODUCTLIST, ORDER_OPEN_EDIT_PRODUCTLIST, PRODUCTS_FETCH_ERROR, PRODUCTS_FETCH_PENDING, PRODUCTS_FETCH_SUCCESS,
      PRODUCTS_INFO_FETCH_SUCCESS, PRODUCTS_PAGE_CHANGE, PRODUCTS_SEARCH_SUCCESS, SEARCH_NAME, SEARCH_PRICE_MAX, SEARCH_PRICE_MIN,
       SEARCH_TYPE_GLASSES, SEARCH_TYPE_HANDBAG, SEARCH_TYPE_SHIRT, SEARCH_TYPE_SHOE, SEARCH_TYPE_WATCH } from "../constants/constants";
const initialState = {
    limit: 8,
    currentPage: 1,
    product: [],
    slider: {
        max: 2000,
        min: 0,
        value: [0, 2000],
    },
    checkbox: {
        shirt: false,
        glasses: false,
        handbag: false,
        watch: false,
        shoe: false
    },
    search: ""

}
const productReducer = (state = initialState, action) => {
    switch (action.type) {
        case PRODUCTS_FETCH_PENDING:
            state.pending = true
            state.updateCart = false
            break;
        case PRODUCTS_FETCH_SUCCESS:
            // Nếu trang đang hoạt động là list Product thì sẽ fetch 9 sản phẩm
            if (window.location.pathname === "/products") {
                state.limit = 9
            }
            // Còn lại (Homepage) fetch 8 sản phẩm
            else {
                state.limit = 8
            }
            state.product = action.data.result
            state.noPage = Math.ceil(action.total / state.limit);
            state.productHomePage = action.dataHomePage
            state.pending = false
            const localCart = JSON.parse(localStorage.getItem("cartItems"))
            // const adminCart = JSON.parse(localStorage.getItem("adminCartItems"))
            // Nếu đã có item trong local thì giữ nguyên
            if (localCart) {
                state.cartItems = localCart
            }
            // nếu chưa thì là mảng rỗng
            else {
                state.cartItems = []
            }
            // if(adminCart){
            //     state.adminCart = adminCart
            //     }
            //     else{
            //         state.adminCart = []
            //     }
            break;
        case PRODUCTS_FETCH_ERROR:
            break;
        case SEARCH_PRICE_MIN:
            state.slider.value = action.payload
            break;
        case SEARCH_PRICE_MAX:
            state.slider.value = action.payload
            break;
        case SEARCH_TYPE_SHIRT:
            state.checkbox.shirt = !state.checkbox.shirt
            break;
        case SEARCH_TYPE_GLASSES:
            state.checkbox.glasses = !state.checkbox.glasses
            break;
        case SEARCH_TYPE_HANDBAG:
            state.checkbox.handbag = !state.checkbox.handbag
            break;
        case SEARCH_TYPE_WATCH:
            state.checkbox.watch = !state.checkbox.watch
            break;
        case SEARCH_TYPE_SHOE:
            state.checkbox.shoe = !state.checkbox.shoe
            break;
        case SEARCH_NAME:
            state.search = action.payload
            break
        case PRODUCTS_SEARCH_SUCCESS:
            state.product = action.data.result
            // state.noPage = Math.ceil(action.total / state.limit);
            break
        case GET_INFO:
            state.productId = action.payload
            break;
        case PRODUCTS_INFO_FETCH_SUCCESS:
            if (JSON.parse(localStorage.getItem("cartItems"))) {
                state.cartItems = JSON.parse(localStorage.getItem("cartItems"))
            }
            else {
                state.cartItems = []
            }
            break;
        case ORDER_OPEN_ADD_PRODUCTLIST:
            localStorage.setItem("adminCartItems", JSON.stringify([]))
        case ORDER_OPEN_EDIT_PRODUCTLIST:
            state.adminCart = JSON.parse(localStorage.getItem("adminCartItems"))
            break;
        case ADD_CART:
            // Nếu trang đang hiện hành là admin order thì push vào localStorage key adminCartItems
            if (window.location.pathname === "/admin/orders") {
                state.adminCart.push(action.payload)
                localStorage.setItem("adminCartItems", JSON.stringify(state.adminCart))
            }
            // Nếu không thì push vào localStorage key cartItems
            else {
                state.updateCart = true
                state.cartItems.push(action.payload)
                localStorage.setItem("cartItems", JSON.stringify(state.cartItems))
            }
            state.noticeAddCart = true
            break
        case PRODUCTS_PAGE_CHANGE:
            state.currentPage = action.page
            break
        case GET_USER_DATA:
            state.userData = action.payload
            break
        case CLOSE_NOTICE_ADDCART:
            state.noticeAddCart = false
            break

        default:
            break;
    }
    return { ...state }
}
export default productReducer