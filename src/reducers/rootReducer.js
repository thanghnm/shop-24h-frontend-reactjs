import { combineReducers } from "redux";
import productReducers from "./productReducer";
import logInReducers from "./loginReducer";
import signUpReducers from "./signUpReducer";
import productInfoReducers from "./productInfoReducer";
import cartReducers from "./cart.reducer";
import tableReducers from "./table.reducer";

const rootReducer =combineReducers({
    productReducers,
    logInReducers,
    signUpReducers,
    productInfoReducers,
    cartReducers,
    tableReducers
})
export default rootReducer