import { CLOSE_MODAL, INPUT_PASSWORD_CHANGE, INPUT_USERNAME_CHANGE, LOG_IN_ERROR_PASSWORD, LOG_IN_PENDING, LOG_IN_SUCCESS, LOG_IN_USERNAME_BLANK, LOG_IN_USER_NOT_FOUND, } from "../constants/constants";

const initialState = {
    logIn: {
        username: "",
        password: ""
    },
    userNotFound: "none",
    errorPassword: "none",
    usernameBlank: "none",
    pending: false,
    openUnauthorizeModal: false

}
const logInReducer = (state = initialState, action) => {
    switch (action.type) {
        case INPUT_USERNAME_CHANGE:
            state.logIn.username = action.payload
            state.userNotFound = "none"
            state.usernameBlank = "none"
            break;
        case INPUT_PASSWORD_CHANGE:
            state.logIn.password = action.payload
            state.errorPassword = "none"
            break;
        case CLOSE_MODAL:
            state.openUnauthorizeModal = false
            break;
        case LOG_IN_PENDING:
            state.pending = true
            break;
        case LOG_IN_SUCCESS:
            state.pending = false
            sessionStorage.setItem("accessToken", action.token)
            sessionStorage.setItem("userId", action.userId)
            sessionStorage.setItem("refreshToken", action.refreshToken)
            sessionStorage.setItem("userRole", action.userRole)
            if (window.location.pathname === "/login") {
                window.location.href = "/"
            }
            if(action.userRole!=="admin"){
                state.openUnauthorizeModal = true
            }
            break;
        case LOG_IN_USER_NOT_FOUND:
            state.userNotFound = "block"
            state.pending = false
            break;
        case LOG_IN_USERNAME_BLANK:
            state.usernameBlank = "block"
            state.pending = false
            break;
        case LOG_IN_ERROR_PASSWORD:
            state.errorPassword = "block"
            state.pending = false
            break;
        default:
            break;
    }
    return { ...state }
}
export default logInReducer