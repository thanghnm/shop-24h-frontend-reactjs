import { INFO_QUANTITY_PLUS, INFO_QUANTITY_SUBTRACT, INFO_SIZE_L, INFO_SIZE_M, INFO_SIZE_S, INFO_SIZE_XL, INFO_SIZE_XXL, PRODUCTS_INFO_FETCH_PENDING, PRODUCTS_INFO_FETCH_SUCCESS } from "../constants/constants";

const initialState = {
    pending: false,
    size: {
        small: false,
        medium: false,
        large: false,
        xl: false,
        xxl: false
    },
    infoQty : 1
}
const productInfoReducer = (state = initialState, action) => {
    switch (action.type) {
        case PRODUCTS_INFO_FETCH_PENDING:
            state.pending = true
            break;
        case PRODUCTS_INFO_FETCH_SUCCESS:
            state.productData = action.data.result
            state.otherProduct = action.otherProduct
            state.pending = false
            break;
        case INFO_SIZE_S:
            state.size.small = true
            state.size.medium = false
            state.size.large = false
            state.size.xl = false
            state.size.xxl = false
            break
        case INFO_SIZE_M:
            state.size.small = false
            state.size.medium = true
            state.size.large = false
            state.size.xl = false
            state.size.xxl = false
            break
        case INFO_SIZE_L:
            state.size.small = false
            state.size.medium = false
            state.size.large = true
            state.size.xl = false
            state.size.xxl = false
            break
        case INFO_SIZE_XL:
            state.size.small = false
            state.size.medium = false
            state.size.large = false
            state.size.xl = true
            state.size.xxl = false
            break
        case INFO_SIZE_XXL:
            state.size.small = false
            state.size.medium = false
            state.size.large = false
            state.size.xl = false
            state.size.xxl = true
            break
        case INFO_QUANTITY_PLUS:
            if(state.infoQty >= 10){
                state.infoQty =10
            }
            else{
                state.infoQty +=1
            }
            break;
        case INFO_QUANTITY_SUBTRACT:
            if(state.infoQty <= 1){
                state.infoQty =1
            }
            else{
                state.infoQty -=1
            }
            break;
        default:
            break;
    }
    return { ...state }
}
export default productInfoReducer