import { Route, Routes } from 'react-router-dom';
import "bootstrap/dist/css/bootstrap.min.css"
import HomePage from './pages/HomePage';
import LogIn from './pages/LogIn';
// import '../node_modules/font-awesome/css/font-awesome.min.css';
import SignUp from './pages/SignUp';
import ProductList from './pages/ProductList';
import ProductInfo from './pages/ProductInfo';
import Cart from './pages/Cart';
import AdminCustomer from './pages/Customer.admin';
import AdminOrder from './pages/Order.admin';
import AdminProduct from './pages/Product.admin';

import './App.css';
import LogInAdmin from './pages/LogIn.admin';
function App() {
  return (
    <div>
      <Routes>
        <Route path='/' element={<HomePage/>}/>
        <Route path='/products' element={<ProductList/>}/>
        <Route path='/login' element={<LogIn/>}/>
        <Route path='/signup' element={<SignUp/>}/>
        <Route path='/products/:productId' element={<ProductInfo/>}/>
        <Route path='/cart' element={<Cart/>}/>
        <Route path='/admin/customers' element={<AdminCustomer/>}/>
        <Route path='/admin/products' element={<AdminProduct/>}/>
        <Route path='/admin/orders' element={<AdminOrder/>}/>
      </Routes>
    </div>
  );
}

export default App;
